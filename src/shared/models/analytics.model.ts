export interface Analytics {
  id: number;
  participantId: number;
  examId: number;
  message: string;
  createdAt: number;
}
