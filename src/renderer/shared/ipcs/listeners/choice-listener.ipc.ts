import {ErrorResponseDto, GetChoiceResponseDto, ListChoicesResponseDto} from "../../../../shared/dtos";
import {choiceAction} from "../../../../shared/redux/actions";
import store from "../../../store";

const choiceListener = () => {
  window.choice.on('list-choices', (args: ListChoicesResponseDto) => {
    store.dispatch(choiceAction.listChoices.fulfilled(args));
  });
  window.choice.on('get-choice', (args: GetChoiceResponseDto) => {
    store.dispatch(choiceAction.getChoice.fulfilled(args));
  });
  window.choice.on('choice-error', (args: ErrorResponseDto) => {
    store.dispatch(choiceAction.setError(args));
  });
}

export default choiceListener;
