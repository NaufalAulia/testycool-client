import {TextFormat} from "./text-format.model";

export interface Answer {
  id: number;
  participantId: number;
  questionId: number;
  choiceId: number | undefined;
  essay: EssayAnswer | undefined;
  isCorrect: boolean;
}

export interface EssayAnswer {
  format: TextFormat;
  content: string;
}
