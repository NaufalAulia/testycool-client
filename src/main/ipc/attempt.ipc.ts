import {ipcMain} from 'electron';
import store from "../store";
import {Metadata} from "@grpc/grpc-js";
import {CreateAttemptRequestDto, GetAttemptRequestDto, UpdateAttemptRequestDto} from "../../shared/dtos";
import Services, {GRPCCredential} from "../adapters/services";
import {AttemptServiceClient} from "../adapters/protobuf/gen/testycool/attempt";
import {attemptAction} from "../../shared/redux/actions";

export const AttemptIpc = () => {
  ipcMain.on('get-attempt', ((event, args: GetAttemptRequestDto) => {
    const address = store.getState().auth.address;
    if (!Services.attempt) {
      Services.attempt = new AttemptServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }

    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.attempt.getAttempt(
      {id: args.id, participantId: args.participantId},
      metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('attempt-error', {code: error.code, details: error.details});
        }
        if (response) {
          store.dispatch(attemptAction.getAttempt.fulfilled({
            attempt: {
              ...response.attempt,
              createdAt: new Date(response.attempt.createdAt).getTime(),
              updatedAt: new Date(response.attempt.updatedAt).getTime(),
            }
          }));
          event.reply('get-attempt', response);
        }
      }));
  }))
  ipcMain.on('create-attempt', ((event, args: CreateAttemptRequestDto) => {
    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    console.log('create request');
    console.log(args);
    Services.attempt.createAttempt(
      {
        ...args,
        createdAt: new Date(args.createdAt),
      },
      metadata,
      ((error, response) => {
        if (error) {
          console.log(error);
          event.sender.send('attempt-error', {code: error.code, details: error.details});
        }
        if (response) {
          const attempt = {
            ...response.attempt,
            createdAt: new Date(response.attempt.createdAt).getTime(),
            updatedAt: new Date(response.attempt.updatedAt).getTime(),
          }
          store.dispatch(attemptAction.createAttempt.fulfilled({attempt}));
          event.reply('create-attempt', {attempt});
        }
      }));
  }))
  ipcMain.on('update-attempt', ((event, args: UpdateAttemptRequestDto) => {
    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.attempt.updateAttempt(
      {
        attempt: {
          ...args.attempt,
          createdAt: new Date(args.attempt.createdAt),
          updatedAt: new Date(args.attempt.updatedAt),
        }
      },
      metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('attempt-error', {code: error.code, details: error.details});
        }
        if (response) {
          const attempt = {
            ...response.attempt,
            createdAt: new Date(response.attempt.createdAt).getTime(),
            updatedAt: new Date(response.attempt.updatedAt).getTime(),
          }
          store.dispatch(attemptAction.updateAttempt.fulfilled({attempt}));
          event.reply('update-attempt', {attempt});
        }
      }));
  }))
}
