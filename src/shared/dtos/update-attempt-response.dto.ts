import {Attempt} from "../models";

export interface UpdateAttemptResponseDto {
  attempt: Attempt | undefined;
}
