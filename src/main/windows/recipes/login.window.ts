import {WindowRecipe} from "./interfaces/window-recipe";
import {browserWindows} from "../window-manager";

declare const LOGIN_WINDOW_WEBPACK_ENTRY: string;
declare const LOGIN_WINDOW_PRELOAD_WEBPACK_ENTRY: string;

const LoginWindow: WindowRecipe = {
  entry: LOGIN_WINDOW_WEBPACK_ENTRY,
  options: {
    title: "login_window",
    height: 550,
    width: 450,
    resizable: false,
    autoHideMenuBar: true,
    webPreferences: {
      preload: LOGIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
      accessibleTitle: "login_window",
      devTools: false,
    }
  },
  extraActions: (window) => {
    window.removeMenu();
    window.on('closed', () => {
      browserWindows.loginWindow = undefined;
    });

    browserWindows.loginWindow = window;
  }
}

export default LoginWindow;