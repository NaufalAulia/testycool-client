export class ErrorResponseDto {
  public code: number;
  public details: string;
}