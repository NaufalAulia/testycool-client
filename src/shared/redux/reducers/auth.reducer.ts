import {createReducer} from '@reduxjs/toolkit';
import {LoadingType} from '../../models';
import {authAction} from '../actions';
import {ErrorResponseDto} from "../../dtos";

export interface AuthState {
  loading: LoadingType
  address?: string;
  credentials?: {
    participantCode?: string;
    participantId?: number;
    examPassword?: string;
  },
  accessToken?: string;
  authenticated?: boolean;
  error?: ErrorResponseDto;
}

const initialState: AuthState = {
  loading: LoadingType.IDLE,
  address: undefined,
  accessToken: undefined,
  authenticated: undefined,
  error: undefined,
};

const AuthReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(authAction.getAccessToken.pending, (state) => ({
      ...state,
      loading: LoadingType.LOADING,
    }))
    .addCase(authAction.getAccessToken.fulfilled, (state, action) => ({
      ...state,
      accessToken: action.payload.accessToken,
      authenticated: true,
      loading: LoadingType.IDLE,
      error: undefined,
    }))
    .addCase(authAction.getAccessToken.rejected, (state, action) => ({
      ...state,
      loading: LoadingType.IDLE,
      error: action.payload,
    }))
    .addCase(authAction.setAddress, (state, action) => ({
      ...state,
      address: action.payload.address,
    }))
    .addCase(authAction.setCredentials, (state, action) => ({
      ...state,
      credentials: action.payload,
    }))
    .addCase(authAction.setAccessToken, (state, action) => ({
      ...state,
      accessToken: action.payload.accessToken,
    }))
    .addCase(authAction.setError, (state, action) => ({
      ...state,
      error: action.payload,
      loading: LoadingType.IDLE,
    }))
    .addCase(authAction.resetState, () => ({
      ...initialState
    }))
);

export default AuthReducer;
