import {createReducer} from '@reduxjs/toolkit';
import {Attempt, Exam, LoadingType} from "../../models";
import {examAction} from "../actions";
import {ErrorResponseDto} from "../../dtos";

export interface ExamState {
  exam?: Exam;
  attempt?: Attempt;
  loading: LoadingType;
  error?: ErrorResponseDto;
}

const initialState: ExamState = {
  exam: undefined,
  attempt: undefined,
  loading: LoadingType.IDLE,
  error: undefined
};

const ExamReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(examAction.getExam.pending, (state) => ({
      ...state,
      loading: LoadingType.GETENTRY,
    }))
    .addCase(examAction.getExam.fulfilled, (state, action) => {
      return {
        ...state,
        exam: action.payload.exam,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(examAction.getExam.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(examAction.setError, (state, action) => ({
      ...state,
      error: action.payload,
      loading: LoadingType.IDLE,
    }))
    .addCase(examAction.resetState, () => {
      return {
        ...initialState
      }
    })
);

export default ExamReducer;
