import {Exam_Status} from "../../main/adapters/protobuf/gen/testycool/exam";
import {ExamStatus} from "../models";

export const examStatusToLocalExamStatus = (es: Exam_Status): ExamStatus => {
  switch (es) {
    case Exam_Status.WAITING:
      return ExamStatus.WAITING;
    case Exam_Status.STARTED:
      return ExamStatus.STARTED;
    case Exam_Status.DONE:
      return ExamStatus.DONE;
    case Exam_Status.UNKNOWN:
      return ExamStatus.UNKNOWN;
  }
}