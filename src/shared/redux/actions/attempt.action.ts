import {createAction} from '@reduxjs/toolkit';
import {CreateAttemptResponseDto, ErrorResponseDto, GetAttemptResponseDto, UpdateAttemptResponseDto} from "../../dtos";
import {FinishStatus} from "../../models";

const attemptAction = {
  getAttempt: {
    pending: createAction('[Attempt] Get Attempt/pending'),
    fulfilled: createAction<GetAttemptResponseDto>('[Attempt] Get Attempt/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Attempt] Get Attempt/rejected'),
  },
  createAttempt: {
    pending: createAction('[Attempt] Create Attempt/pending'),
    fulfilled: createAction<CreateAttemptResponseDto>('[Attempt] Create Attempt/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Attempt] Create Attempt/rejected'),
  },
  updateAttempt: {
    pending: createAction('[Attempt] Update Attempt/pending'),
    fulfilled: createAction<UpdateAttemptResponseDto>('[Attempt] Update Attempt/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Attempt] Update Attempt/rejected'),
  },
  updateFinishStatus: createAction<FinishStatus>("[Attempt] Update Attempt Finish Status"),
  setError: createAction<ErrorResponseDto>(
    '[Attempt] set exam error'
  ),
  resetState: createAction('[Attempt] reset state')
}

export default attemptAction;
