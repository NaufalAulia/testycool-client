import {Attempt} from "../models";

export interface UpdateAttemptRequestDto {
  attempt: Attempt | undefined;
}
