/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "testycool.v1";

export interface GetAdminTokenRequest {
  passcode: string;
}

export interface GetAdminTokenResponse {
  accessToken: string;
}

export interface GetParticipantTokenRequest {
  examPassword: string;
  participantCode: string;
}

export interface GetParticipantTokenResponse {
  accessToken: string;
}

function createBaseGetAdminTokenRequest(): GetAdminTokenRequest {
  return { passcode: "" };
}

export const GetAdminTokenRequest = {
  encode(
    message: GetAdminTokenRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.passcode !== "") {
      writer.uint32(10).string(message.passcode);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetAdminTokenRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAdminTokenRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.passcode = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAdminTokenRequest {
    return {
      passcode: isSet(object.passcode) ? String(object.passcode) : "",
    };
  },

  toJSON(message: GetAdminTokenRequest): unknown {
    const obj: any = {};
    message.passcode !== undefined && (obj.passcode = message.passcode);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAdminTokenRequest>, I>>(
    object: I
  ): GetAdminTokenRequest {
    const message = createBaseGetAdminTokenRequest();
    message.passcode = object.passcode ?? "";
    return message;
  },
};

function createBaseGetAdminTokenResponse(): GetAdminTokenResponse {
  return { accessToken: "" };
}

export const GetAdminTokenResponse = {
  encode(
    message: GetAdminTokenResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.accessToken !== "") {
      writer.uint32(10).string(message.accessToken);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetAdminTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAdminTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.accessToken = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAdminTokenResponse {
    return {
      accessToken: isSet(object.accessToken) ? String(object.accessToken) : "",
    };
  },

  toJSON(message: GetAdminTokenResponse): unknown {
    const obj: any = {};
    message.accessToken !== undefined &&
      (obj.accessToken = message.accessToken);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAdminTokenResponse>, I>>(
    object: I
  ): GetAdminTokenResponse {
    const message = createBaseGetAdminTokenResponse();
    message.accessToken = object.accessToken ?? "";
    return message;
  },
};

function createBaseGetParticipantTokenRequest(): GetParticipantTokenRequest {
  return { examPassword: "", participantCode: "" };
}

export const GetParticipantTokenRequest = {
  encode(
    message: GetParticipantTokenRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examPassword !== "") {
      writer.uint32(10).string(message.examPassword);
    }
    if (message.participantCode !== "") {
      writer.uint32(18).string(message.participantCode);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetParticipantTokenRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetParticipantTokenRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examPassword = reader.string();
          break;
        case 2:
          message.participantCode = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetParticipantTokenRequest {
    return {
      examPassword: isSet(object.examPassword)
        ? String(object.examPassword)
        : "",
      participantCode: isSet(object.participantCode)
        ? String(object.participantCode)
        : "",
    };
  },

  toJSON(message: GetParticipantTokenRequest): unknown {
    const obj: any = {};
    message.examPassword !== undefined &&
      (obj.examPassword = message.examPassword);
    message.participantCode !== undefined &&
      (obj.participantCode = message.participantCode);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetParticipantTokenRequest>, I>>(
    object: I
  ): GetParticipantTokenRequest {
    const message = createBaseGetParticipantTokenRequest();
    message.examPassword = object.examPassword ?? "";
    message.participantCode = object.participantCode ?? "";
    return message;
  },
};

function createBaseGetParticipantTokenResponse(): GetParticipantTokenResponse {
  return { accessToken: "" };
}

export const GetParticipantTokenResponse = {
  encode(
    message: GetParticipantTokenResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.accessToken !== "") {
      writer.uint32(10).string(message.accessToken);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetParticipantTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetParticipantTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.accessToken = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetParticipantTokenResponse {
    return {
      accessToken: isSet(object.accessToken) ? String(object.accessToken) : "",
    };
  },

  toJSON(message: GetParticipantTokenResponse): unknown {
    const obj: any = {};
    message.accessToken !== undefined &&
      (obj.accessToken = message.accessToken);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetParticipantTokenResponse>, I>>(
    object: I
  ): GetParticipantTokenResponse {
    const message = createBaseGetParticipantTokenResponse();
    message.accessToken = object.accessToken ?? "";
    return message;
  },
};

export const AuthServiceService = {
  getAdminToken: {
    path: "/testycool.v1.AuthService/GetAdminToken",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetAdminTokenRequest) =>
      Buffer.from(GetAdminTokenRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetAdminTokenRequest.decode(value),
    responseSerialize: (value: GetAdminTokenResponse) =>
      Buffer.from(GetAdminTokenResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetAdminTokenResponse.decode(value),
  },
  getParticipantToken: {
    path: "/testycool.v1.AuthService/GetParticipantToken",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetParticipantTokenRequest) =>
      Buffer.from(GetParticipantTokenRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      GetParticipantTokenRequest.decode(value),
    responseSerialize: (value: GetParticipantTokenResponse) =>
      Buffer.from(GetParticipantTokenResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      GetParticipantTokenResponse.decode(value),
  },
} as const;

export interface AuthServiceServer extends UntypedServiceImplementation {
  getAdminToken: handleUnaryCall<GetAdminTokenRequest, GetAdminTokenResponse>;
  getParticipantToken: handleUnaryCall<
    GetParticipantTokenRequest,
    GetParticipantTokenResponse
  >;
}

export interface AuthServiceClient extends Client {
  getAdminToken(
    request: GetAdminTokenRequest,
    callback: (
      error: ServiceError | null,
      response: GetAdminTokenResponse
    ) => void
  ): ClientUnaryCall;
  getAdminToken(
    request: GetAdminTokenRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: GetAdminTokenResponse
    ) => void
  ): ClientUnaryCall;
  getAdminToken(
    request: GetAdminTokenRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: GetAdminTokenResponse
    ) => void
  ): ClientUnaryCall;
  getParticipantToken(
    request: GetParticipantTokenRequest,
    callback: (
      error: ServiceError | null,
      response: GetParticipantTokenResponse
    ) => void
  ): ClientUnaryCall;
  getParticipantToken(
    request: GetParticipantTokenRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: GetParticipantTokenResponse
    ) => void
  ): ClientUnaryCall;
  getParticipantToken(
    request: GetParticipantTokenRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: GetParticipantTokenResponse
    ) => void
  ): ClientUnaryCall;
}

export const AuthServiceClient = makeGenericClientConstructor(
  AuthServiceService,
  "testycool.v1.AuthService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): AuthServiceClient;
  service: typeof AuthServiceService;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
