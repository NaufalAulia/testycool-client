import {Answer} from "../models";

export interface GetAnswerResponseDto {
  answer: Answer | undefined;
}
