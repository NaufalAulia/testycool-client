import React from "react";
import {questionAction} from "../../../../shared/redux/actions";
import {LoadingType, Question, QuestionFlag} from "../../../../shared/models";
import {useDispatch} from "react-redux";
import {AppDispatch} from "../../../store";
import {LoadingTypeToString} from "../../../../shared/utils/string-helper";
import ReactTooltip from "react-tooltip";

export default function QuestionList(
  props: {
    questions?: Question[],
    questionFlags?: QuestionFlag[],
    questionIdx: number,
    questionLoading: LoadingType,
    answerLoading: LoadingType,
    answerSaved: boolean,
  }): JSX.Element {
  const {questions, questionFlags, questionIdx, questionLoading, answerLoading, answerSaved} = props;

  const dispatch = useDispatch<AppDispatch>();

  const buttonStyle = (i: number): string => {
    if (questionFlags[i].isActive) {
      return i === questionIdx ? "button-danger" : "button-danger-outline";
    } else {
      return i === questionIdx ? "button-info" : "button-info-outline";
    }
  };

  return (
    (questions && questions.length > 0 && questionLoading !== LoadingType.GETLIST) ? (
        <div
          className="
          flex-grow grid
          2xl:grid-cols-5 xl:grid-cols-4 grid-cols-3
          gap-3 justify-items-start content-start my-10
          "
        >
          {
            questions.map((q, i) => (
              <button
                key={q.id}
                onClick={() => dispatch(questionAction.changeQuestion({index: i}))}
                className={`button button-md w-12 ${buttonStyle(i)} group relative`}
                disabled={!answerSaved || questionLoading !== LoadingType.IDLE || answerLoading !== LoadingType.IDLE}
                data-tip={i} data-for={`tooltip-${i}`}
              >
                {i + 1}
                {(questionFlags[i].isActive && questionFlags[i].note !== "") ? (
                    <ReactTooltip id={`tooltip-${i}`} effect="solid" type="dark" place="top">
                      <span>{questionFlags[i].note}</span>
                    </ReactTooltip>
                  )
                  : (
                    ""
                  )
                }
              </button>
            ))
          }
        </div>
      )
      : (
        <h2 className="flex-grow text-xl font-bold text-center my-10">
          {LoadingTypeToString(questionLoading, 'Question')}
        </h2>
      )
  )
}