import {ipcRenderer} from "electron";

export const windowManagerContextBridge = {
  apiKey: 'windowManager', api: {
    switchToMain: () => {
      ipcRenderer.send('switch-to-main', {});
    },
    switchToLogin: () => {
      ipcRenderer.send('switch-to-login', {});
    }
  }
};
