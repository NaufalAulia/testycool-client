import {createAction} from '@reduxjs/toolkit';
import {
  CreateAttemptResponseDto,
  ErrorResponseDto,
  GetAttemptResponseDto,
  GetExamResponseDto,
  UpdateAttemptResponseDto
} from "../../dtos";

const examAction = {
  getExam: {
    pending: createAction('[Exam] Get Exam/pending'),
    fulfilled: createAction<GetExamResponseDto>('[Exam] Get Exam/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Exam] Get Exam/rejected'),
  },
  getExamAttempt: {
    pending: createAction('[Exam] Get Exam Attempt/pending'),
    fulfilled: createAction<GetAttemptResponseDto>('[Exam] Get Exam Attempt/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Exam] Get Exam Attempt/rejected'),
  },
  createExamAttempt: {
    pending: createAction('[Exam] Create Exam Attempt/pending'),
    fulfilled: createAction<CreateAttemptResponseDto>('[Exam] Create Exam Attempt/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Exam] Create Exam Attempt/rejected'),
  },
  updateExamAttempt: {
    pending: createAction('[Exam] Update Exam Attempt/pending'),
    fulfilled: createAction<UpdateAttemptResponseDto>('[Exam] Update Exam Attempt/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Exam] Update Exam Attempt/rejected'),
  },
  setError: createAction<ErrorResponseDto>(
    '[Exam] set exam error'
  ),
  resetState: createAction('[Exam] reset state')
}

export default examAction;
