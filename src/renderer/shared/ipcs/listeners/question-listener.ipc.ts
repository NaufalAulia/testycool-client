import {ErrorResponseDto, GetQuestionResponseDto, ListQuestionsResponseDto} from "../../../../shared/dtos";
import {questionAction} from "../../../../shared/redux/actions";
import store from "../../../store";
import {QuestionType} from "../../../../shared/models";

const questionListener = () => {
  window.question.on('list-questions', (args: ListQuestionsResponseDto) => {
    store.dispatch(questionAction.listQuestions.fulfilled(args));
  });
  window.question.on('get-question', (args: GetQuestionResponseDto) => {
    if (args.question.type === QuestionType.MULTIPLE_CHOICE) {
      window.choice.listChoices({filter: {questionId: args.question.id}, size: -1, page: 1});
    }
    store.dispatch(questionAction.getQuestion.fulfilled(args));
  });
  window.question.on('question-error', (args: ErrorResponseDto) => {
    store.dispatch(questionAction.setError(args));
  });
}

export default questionListener;
