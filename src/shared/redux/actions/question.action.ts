import {createAction} from '@reduxjs/toolkit';
import {ErrorResponseDto, GetQuestionResponseDto, ListQuestionsResponseDto} from "../../dtos";
import {QuestionFlag} from "../../models";

const questionAction = {
  listQuestions: {
    pending: createAction('[Question] List Questions/pending'),
    fulfilled: createAction<ListQuestionsResponseDto>('[Question] List Questions/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Question] List Questions/rejected'),
  },
  getQuestion: {
    pending: createAction('[Question] Get Question/pending'),
    fulfilled: createAction<GetQuestionResponseDto>('[Question] Get Question/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Question] Get Question/rejected'),
  },
  changeQuestion: createAction<{ index: number }>('[Question] Change current Question'),
  setFlag: createAction<QuestionFlag>('[Question] set question flag'),
  setError: createAction<ErrorResponseDto>('[Question] set question error'),
  resetState: createAction('[Question] reset state')
}

export default questionAction;
