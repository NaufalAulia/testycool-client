import React, {useCallback} from "react";
import {useSelector} from "react-redux";
import {RootState} from "../../store";
import {ExamStatus, FinishStatus, LoadingType} from "../../../shared/models";
import {windowManagerSender} from "../../shared/ipcs";

export default function Result(): JSX.Element {
  const exam = useSelector((state: RootState) => state.exam.exam);
  const questionSize = useSelector((state: RootState) => state.question.size);
  const attempt = useSelector((state: RootState) => state.attempt.attempt);
  const attemptLoading = useSelector((state: RootState) => state.attempt.loading);
  const finishStatus = useSelector((state: RootState) => state.attempt.finishStatus);

  const resultMessage = useCallback(() => {
    if (exam.status === ExamStatus.DONE) {
      return "Exam Ended";
    }

    switch (finishStatus) {
      case FinishStatus.FORFEITED:
        return "You have forfeited your attempt";
      case FinishStatus.FINISHED:
        return "You have finished your attempt";
      default:
        return "";
    }
  }, [exam, finishStatus]);

  return (
    <div className="h-screen grid place-content-center gap-6 text-center bg-gray-100">
      <div className="grid place-content-center gap-6 text-center">
        <h1 className="text-5xl font-bold text-primary">
          {resultMessage()}
        </h1>
        {finishStatus === FinishStatus.FORFEITED ?
          <p className="text-md">
            If you think this was a mistake please contact administrator through:

            E-Mail: naufal7600@gmail.com, Telegram: t.me/nulllptr
          </p>
          :
          ""
        }
        {attempt && attemptLoading === LoadingType.IDLE ? (
          <div className="grid place-content-center gap-6">
            <h3 className="text-xl">You results are</h3>
            <h3 className="text-xl">
              <span>
                {"Out of "}
                {questionSize !== -1 ? questionSize : (attempt.corrects + attempt.wrongs + attempt.unanswered)}
                {" Questions: "}
              </span>
              <span>
                {attempt.corrects} Corrects, {attempt.wrongs} Wrongs, {attempt.unanswered} Unanswered
              </span>
            </h3>
            <button
              onClick={() => windowManagerSender.switchToLogin()}
              className="button button-md button-info justify-self-center w-1/2"
            >
              LOGOUT
            </button>
          </div>
        ) : (
          <h3 className="text-xl">Calculating your results...</h3>
        )}
      </div>
    </div>
  );
}
