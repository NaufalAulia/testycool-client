import {ipcRenderer} from "electron";

export const examChannels = ['get-exam', 'exam-error'];

export const examContextBridge = {
  apiKey: 'exam',
  api: {
    getExam: (getExamRequestDto) => {
      ipcRenderer.send('get-exam', {...getExamRequestDto});
    },
    once: (channel, func) => {
      if (!examChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid exam channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.once(channel, (event, ...args) => func(...args));
    },
    on: (channel, func) => {
      if (!examChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid exam channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    },
  }
}