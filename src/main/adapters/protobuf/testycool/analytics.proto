// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

syntax = "proto3";

package testycool.v1;

import "google/api/annotations.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/empty.proto";

option go_package = "gitlab.com/testycool/server/api/protobuf/gen/go/testycool";


// -----------------------------------------------------------------------------
// Service Definitions
// -----------------------------------------------------------------------------

service AnalyticsService {
  rpc GetAnalytics(GetAnalyticsRequest) returns (GetAnalyticsResponse) {
    option (google.api.http) = {
      get: "/v1/analytics/{id}"
    };
  }

  rpc ListAnalytics(ListAnalyticsRequest) returns (ListAnalyticsResponse) {
    option (google.api.http) = {
      get: "/v1/analytics"
    };
  }

  rpc CreateAnalytics(CreateAnalyticsRequest) returns (CreateAnalyticsResponse) {
    option (google.api.http) = {
      post: "/v1/analytics"
      body: "*"
    };
  }

  rpc UpdateAnalytics(UpdateAnalyticsRequest) returns (UpdateAnalyticsResponse) {
    option (google.api.http) = {
      put: "/v1/analytics"
      body: "*"
    };
  }

  rpc DeleteAnalytics(DeleteAnalyticsRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/analytics/{id}"
    };
  }

}


// -----------------------------------------------------------------------------
// Request and Response Definitions
// -----------------------------------------------------------------------------

message GetAnalyticsRequest {
  int32 id = 1;
}

message GetAnalyticsResponse {
  Analytics analytics = 1;
}

message ListAnalyticsRequest {
  int32 size = 1;
  int32 page = 2;
  message Filter {
    optional int32 exam_id = 2;
  }
  Filter filter = 3;
}

message ListAnalyticsResponse {
  repeated Analytics analytics = 1;
  int32 size = 2;
  int32 page = 3;
  int32 total_size = 4;
}

message CreateAnalyticsRequest {
  int32 participant_id = 1;
  int32 exam_id = 2;
  string message = 3;
  google.protobuf.Timestamp created_at = 4;
}

message CreateAnalyticsResponse {
  Analytics analytics = 1;
}

message UpdateAnalyticsRequest {
  Analytics analytics = 1;
}

message UpdateAnalyticsResponse {
  Analytics analytics = 1;
}

message DeleteAnalyticsRequest {
  int32 id = 1;
}


// -----------------------------------------------------------------------------
// Resource Definitions
// -----------------------------------------------------------------------------

message Analytics {
  int32 id = 1;
  int32 participant_id = 2;
  int32 exam_id = 3;
  string message = 4;
  google.protobuf.Timestamp created_at = 5;
}

