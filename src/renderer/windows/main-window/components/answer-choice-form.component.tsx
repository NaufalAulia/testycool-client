import React, {useEffect, useState} from "react";
import {Answer, Choice, LoadingType, Question} from "../../../../shared/models";
import {answerSender} from "../../../shared/ipcs";
import {Participant} from "../../../../shared/models/participant.model";
import {useDispatch} from "react-redux";
import {AppDispatch} from "../../../store";
import {answerAction} from "../../../../shared/redux/actions";
import {LoadingTypeToString} from "../../../../shared/utils/string-helper";
import ReactQuill from "react-quill";

export default function AnswerChoiceForm(
  props: {
    participant?: Participant,
    currentQuestion: Question,
    choices?: Choice[],
    currentAnswer?: Answer,
    questionLoading: LoadingType,
    choiceLoading: LoadingType,
    answerLoading: LoadingType
  }): JSX.Element {
  const {participant, currentQuestion, choices, currentAnswer, questionLoading, choiceLoading, answerLoading} = props;

  const [answerForm, setAnswerForm] = useState<{ choiceId: number | undefined }>({
      choiceId: currentAnswer?.choiceId || undefined,
    }
  );

  const handleChange = (event?: any) => {
    if (answerForm.choiceId !== +event.currentTarget.value) {
      dispatch(answerAction.setAnswerSaved(currentAnswer?.choiceId === +event.currentTarget.value));
    }
    setAnswerForm({...answerForm, choiceId: +event.currentTarget.value});
  }

  const dispatch = useDispatch<AppDispatch>();
  useEffect(() => {
    const timeout = setTimeout(() => {
      if (
        questionLoading === LoadingType.IDLE &&
        answerLoading === LoadingType.IDLE &&
        participant &&
        answerForm.choiceId
      ) {
        if (!currentAnswer) {
          answerSender.createAnswer({
            questionId: currentQuestion.id,
            participantId: participant.id,
            choiceId: answerForm.choiceId,
            essay: undefined
          });
        } else {
          if (currentAnswer.choiceId !== answerForm.choiceId) {
            answerSender.updateAnswer({
              answer: {
                ...currentAnswer,
                choiceId: answerForm.choiceId,
              }
            });
          }
        }
      }
    }, 1000);
    return () => clearTimeout(timeout);
  }, [answerForm, participant, currentQuestion, choices, currentAnswer, questionLoading, answerLoading]);

  return (
    <form>
      <div>
        {choiceLoading !== LoadingType.IDLE ?
          (
            <h2 className="flex-grow my-8 text-xl font-bold text-center">
              {LoadingTypeToString(choiceLoading, 'Choice')}
            </h2>
          ) :
          (
            choices?.map((c, i) => (
              <div key={i} className="mb-5 flex items-center">
                <input
                  type="radio" name="choice" id={`choice-${i}`} value={c.id}
                  className="w-6 h-6"
                  onChange={handleChange}
                  checked={c.id === answerForm.choiceId}
                  disabled={answerLoading !== LoadingType.IDLE}
                />
                <label htmlFor="a" className="ml-2 float-left">
                  <ReactQuill className="text-md select-none" readOnly value={c.content} theme="bubble"/>
                </label>
              </div>
            ))
          )
        }
      </div>
    </form>
  );
}