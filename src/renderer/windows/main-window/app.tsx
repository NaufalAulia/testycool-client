import React, {useEffect} from "react";
import {MemoryRouter, Route, Routes} from "react-router";
import Attempt from "./attempt";
import Waiting from "./waiting";
import Result from "./result";
import {toast, ToastContainer} from "react-toastify";
import {useSelector} from "react-redux";
import {RootState} from "../../store";
import {TranslateAnalyticsMessage} from "../../../shared/utils/string-helper";

export default function App(): JSX.Element {
  const authError = useSelector((state: RootState) => state.auth.error);
  const examError = useSelector((state: RootState) => state.exam.error);
  const participantError = useSelector((state: RootState) => state.participant.error);
  const attemptError = useSelector((state: RootState) => state.attempt.error);
  const questionError = useSelector((state: RootState) => state.question.error);
  const choiceError = useSelector((state: RootState) => state.choice.error);
  const answerError = useSelector((state: RootState) => state.answer.error);
  const analyticsError = useSelector((state: RootState) => state.analytics.error);
  const newAnalytics = useSelector((state: RootState) => state.analytics.newAnalytics);

  useEffect(() => {
    if (authError) {
      toast.error(`${authError.details}`);
    }
  }, [authError]);
  useEffect(() => {
    if (examError) {
      toast.error(`${examError.details}`);
    }
  }, [examError]);
  useEffect(() => {
    if (participantError) {
      toast.error(`${participantError.details}`);
    }
  }, [participantError]);
  useEffect(() => {
    if (attemptError) {
      toast.error(`${attemptError.details}`);
    }
  }, [attemptError]);
  useEffect(() => {
    if (questionError) {
      toast.error(`${questionError.details}`);
    }
  }, [questionError]);
  useEffect(() => {
    if (choiceError) {
      toast.error(`${choiceError.details}`);
    }
  }, [choiceError]);
  useEffect(() => {
    if (answerError) {
      toast.error(`${answerError.details}`);
    }
  }, [answerError]);
  useEffect(() => {
    if (analyticsError) {
      toast.error(`${analyticsError.details}`);
    }
  }, [analyticsError]);
  useEffect(() => {
    if (newAnalytics) {
      toast.warning(`${TranslateAnalyticsMessage(newAnalytics.message)}`);
    }
  }, [newAnalytics]);

  return (
    <MemoryRouter initialEntries={["/waiting"]}>
      <Routes>
        <Route path="attempt" element={<Attempt/>}/>
        <Route path="waiting" element={<Waiting/>}/>
        <Route path="result" element={<Result/>}/>
      </Routes>
      <ToastContainer
        position="top-right"
        theme="colored"
        hideProgressBar
      />
    </MemoryRouter>
  );
}
