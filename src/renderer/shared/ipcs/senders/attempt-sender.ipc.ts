import {attemptAction} from "../../../../shared/redux/actions";
import {CreateAttemptRequestDto, GetAttemptRequestDto, UpdateAttemptRequestDto} from "../../../../shared/dtos";
import store from "../../../store";

const attemptSender = {
  getAttempt: (dto: GetAttemptRequestDto) => {
    store.dispatch(attemptAction.getAttempt.pending());
    window.attempt.getAttempt(dto);
  },
  createAttempt: (dto: CreateAttemptRequestDto) => {
    store.dispatch(attemptAction.createAttempt.pending());
    window.attempt.createAttempt(dto);
  },
  updateAttempt: (dto: UpdateAttemptRequestDto) => {
    store.dispatch(attemptAction.updateAttempt.pending());
    window.attempt.updateAttempt(dto);
  },
}

export default attemptSender;