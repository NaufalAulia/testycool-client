/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import {
  TextFormat,
  textFormatFromJSON,
  textFormatToJSON,
} from "../testycool/text_format";
import { Empty } from "../google/protobuf/empty";

export const protobufPackage = "testycool.v1";

export interface GetAnswerRequest {
  id: number;
}

export interface GetAnswerResponse {
  answer: Answer | undefined;
}

export interface ListAnswersRequest {
  size: number;
  page: number;
  filter: ListAnswersRequest_Filter | undefined;
}

export interface ListAnswersRequest_Filter {
  participantId?: number | undefined;
  examId?: number | undefined;
}

export interface ListAnswersResponse {
  answers: Answer[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateAnswerRequest {
  participantId: number;
  questionId: number;
  choiceId: number | undefined;
  essay: EssayAnswer | undefined;
}

export interface CreateAnswerResponse {
  answer: Answer | undefined;
}

export interface UpdateAnswerRequest {
  answer: Answer | undefined;
}

export interface UpdateAnswerResponse {
  answer: Answer | undefined;
}

export interface DeleteAnswerRequest {
  id: number;
}

export interface Answer {
  id: number;
  participantId: number;
  questionId: number;
  choiceId: number | undefined;
  essay: EssayAnswer | undefined;
  isCorrect: boolean;
}

export interface EssayAnswer {
  format: TextFormat;
  content: string;
}

function createBaseGetAnswerRequest(): GetAnswerRequest {
  return { id: 0 };
}

export const GetAnswerRequest = {
  encode(
    message: GetAnswerRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAnswerRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAnswerRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAnswerRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: GetAnswerRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAnswerRequest>, I>>(
    object: I
  ): GetAnswerRequest {
    const message = createBaseGetAnswerRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseGetAnswerResponse(): GetAnswerResponse {
  return { answer: undefined };
}

export const GetAnswerResponse = {
  encode(
    message: GetAnswerResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.answer !== undefined) {
      Answer.encode(message.answer, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAnswerResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAnswerResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answer = Answer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAnswerResponse {
    return {
      answer: isSet(object.answer) ? Answer.fromJSON(object.answer) : undefined,
    };
  },

  toJSON(message: GetAnswerResponse): unknown {
    const obj: any = {};
    message.answer !== undefined &&
      (obj.answer = message.answer ? Answer.toJSON(message.answer) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAnswerResponse>, I>>(
    object: I
  ): GetAnswerResponse {
    const message = createBaseGetAnswerResponse();
    message.answer =
      object.answer !== undefined && object.answer !== null
        ? Answer.fromPartial(object.answer)
        : undefined;
    return message;
  },
};

function createBaseListAnswersRequest(): ListAnswersRequest {
  return { size: 0, page: 0, filter: undefined };
}

export const ListAnswersRequest = {
  encode(
    message: ListAnswersRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListAnswersRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListAnswersRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListAnswersRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListAnswersRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnswersRequest {
    return {
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      filter: isSet(object.filter)
        ? ListAnswersRequest_Filter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: ListAnswersRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListAnswersRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListAnswersRequest>, I>>(
    object: I
  ): ListAnswersRequest {
    const message = createBaseListAnswersRequest();
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? ListAnswersRequest_Filter.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

function createBaseListAnswersRequest_Filter(): ListAnswersRequest_Filter {
  return { participantId: undefined, examId: undefined };
}

export const ListAnswersRequest_Filter = {
  encode(
    message: ListAnswersRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participantId !== undefined) {
      writer.uint32(16).int32(message.participantId);
    }
    if (message.examId !== undefined) {
      writer.uint32(24).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAnswersRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListAnswersRequest_Filter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.participantId = reader.int32();
          break;
        case 3:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnswersRequest_Filter {
    return {
      participantId: isSet(object.participantId)
        ? Number(object.participantId)
        : undefined,
      examId: isSet(object.examId) ? Number(object.examId) : undefined,
    };
  },

  toJSON(message: ListAnswersRequest_Filter): unknown {
    const obj: any = {};
    message.participantId !== undefined &&
      (obj.participantId = Math.round(message.participantId));
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListAnswersRequest_Filter>, I>>(
    object: I
  ): ListAnswersRequest_Filter {
    const message = createBaseListAnswersRequest_Filter();
    message.participantId = object.participantId ?? undefined;
    message.examId = object.examId ?? undefined;
    return message;
  },
};

function createBaseListAnswersResponse(): ListAnswersResponse {
  return { answers: [], size: 0, page: 0, totalSize: 0 };
}

export const ListAnswersResponse = {
  encode(
    message: ListAnswersResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.answers) {
      Answer.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListAnswersResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListAnswersResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answers.push(Answer.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnswersResponse {
    return {
      answers: Array.isArray(object?.answers)
        ? object.answers.map((e: any) => Answer.fromJSON(e))
        : [],
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      totalSize: isSet(object.totalSize) ? Number(object.totalSize) : 0,
    };
  },

  toJSON(message: ListAnswersResponse): unknown {
    const obj: any = {};
    if (message.answers) {
      obj.answers = message.answers.map((e) =>
        e ? Answer.toJSON(e) : undefined
      );
    } else {
      obj.answers = [];
    }
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.totalSize !== undefined &&
      (obj.totalSize = Math.round(message.totalSize));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListAnswersResponse>, I>>(
    object: I
  ): ListAnswersResponse {
    const message = createBaseListAnswersResponse();
    message.answers = object.answers?.map((e) => Answer.fromPartial(e)) || [];
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.totalSize = object.totalSize ?? 0;
    return message;
  },
};

function createBaseCreateAnswerRequest(): CreateAnswerRequest {
  return {
    participantId: 0,
    questionId: 0,
    choiceId: undefined,
    essay: undefined,
  };
}

export const CreateAnswerRequest = {
  encode(
    message: CreateAnswerRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participantId !== 0) {
      writer.uint32(8).int32(message.participantId);
    }
    if (message.questionId !== 0) {
      writer.uint32(16).int32(message.questionId);
    }
    if (message.choiceId !== undefined) {
      writer.uint32(24).int32(message.choiceId);
    }
    if (message.essay !== undefined) {
      EssayAnswer.encode(message.essay, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CreateAnswerRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateAnswerRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participantId = reader.int32();
          break;
        case 2:
          message.questionId = reader.int32();
          break;
        case 3:
          message.choiceId = reader.int32();
          break;
        case 4:
          message.essay = EssayAnswer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAnswerRequest {
    return {
      participantId: isSet(object.participantId)
        ? Number(object.participantId)
        : 0,
      questionId: isSet(object.questionId) ? Number(object.questionId) : 0,
      choiceId: isSet(object.choiceId) ? Number(object.choiceId) : undefined,
      essay: isSet(object.essay)
        ? EssayAnswer.fromJSON(object.essay)
        : undefined,
    };
  },

  toJSON(message: CreateAnswerRequest): unknown {
    const obj: any = {};
    message.participantId !== undefined &&
      (obj.participantId = Math.round(message.participantId));
    message.questionId !== undefined &&
      (obj.questionId = Math.round(message.questionId));
    message.choiceId !== undefined &&
      (obj.choiceId = Math.round(message.choiceId));
    message.essay !== undefined &&
      (obj.essay = message.essay
        ? EssayAnswer.toJSON(message.essay)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateAnswerRequest>, I>>(
    object: I
  ): CreateAnswerRequest {
    const message = createBaseCreateAnswerRequest();
    message.participantId = object.participantId ?? 0;
    message.questionId = object.questionId ?? 0;
    message.choiceId = object.choiceId ?? undefined;
    message.essay =
      object.essay !== undefined && object.essay !== null
        ? EssayAnswer.fromPartial(object.essay)
        : undefined;
    return message;
  },
};

function createBaseCreateAnswerResponse(): CreateAnswerResponse {
  return { answer: undefined };
}

export const CreateAnswerResponse = {
  encode(
    message: CreateAnswerResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.answer !== undefined) {
      Answer.encode(message.answer, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAnswerResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateAnswerResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answer = Answer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAnswerResponse {
    return {
      answer: isSet(object.answer) ? Answer.fromJSON(object.answer) : undefined,
    };
  },

  toJSON(message: CreateAnswerResponse): unknown {
    const obj: any = {};
    message.answer !== undefined &&
      (obj.answer = message.answer ? Answer.toJSON(message.answer) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateAnswerResponse>, I>>(
    object: I
  ): CreateAnswerResponse {
    const message = createBaseCreateAnswerResponse();
    message.answer =
      object.answer !== undefined && object.answer !== null
        ? Answer.fromPartial(object.answer)
        : undefined;
    return message;
  },
};

function createBaseUpdateAnswerRequest(): UpdateAnswerRequest {
  return { answer: undefined };
}

export const UpdateAnswerRequest = {
  encode(
    message: UpdateAnswerRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.answer !== undefined) {
      Answer.encode(message.answer, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UpdateAnswerRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateAnswerRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answer = Answer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAnswerRequest {
    return {
      answer: isSet(object.answer) ? Answer.fromJSON(object.answer) : undefined,
    };
  },

  toJSON(message: UpdateAnswerRequest): unknown {
    const obj: any = {};
    message.answer !== undefined &&
      (obj.answer = message.answer ? Answer.toJSON(message.answer) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateAnswerRequest>, I>>(
    object: I
  ): UpdateAnswerRequest {
    const message = createBaseUpdateAnswerRequest();
    message.answer =
      object.answer !== undefined && object.answer !== null
        ? Answer.fromPartial(object.answer)
        : undefined;
    return message;
  },
};

function createBaseUpdateAnswerResponse(): UpdateAnswerResponse {
  return { answer: undefined };
}

export const UpdateAnswerResponse = {
  encode(
    message: UpdateAnswerResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.answer !== undefined) {
      Answer.encode(message.answer, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAnswerResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateAnswerResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answer = Answer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAnswerResponse {
    return {
      answer: isSet(object.answer) ? Answer.fromJSON(object.answer) : undefined,
    };
  },

  toJSON(message: UpdateAnswerResponse): unknown {
    const obj: any = {};
    message.answer !== undefined &&
      (obj.answer = message.answer ? Answer.toJSON(message.answer) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateAnswerResponse>, I>>(
    object: I
  ): UpdateAnswerResponse {
    const message = createBaseUpdateAnswerResponse();
    message.answer =
      object.answer !== undefined && object.answer !== null
        ? Answer.fromPartial(object.answer)
        : undefined;
    return message;
  },
};

function createBaseDeleteAnswerRequest(): DeleteAnswerRequest {
  return { id: 0 };
}

export const DeleteAnswerRequest = {
  encode(
    message: DeleteAnswerRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DeleteAnswerRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDeleteAnswerRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteAnswerRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: DeleteAnswerRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<DeleteAnswerRequest>, I>>(
    object: I
  ): DeleteAnswerRequest {
    const message = createBaseDeleteAnswerRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseAnswer(): Answer {
  return {
    id: 0,
    participantId: 0,
    questionId: 0,
    choiceId: undefined,
    essay: undefined,
    isCorrect: false,
  };
}

export const Answer = {
  encode(
    message: Answer,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.participantId !== 0) {
      writer.uint32(16).int32(message.participantId);
    }
    if (message.questionId !== 0) {
      writer.uint32(24).int32(message.questionId);
    }
    if (message.choiceId !== undefined) {
      writer.uint32(32).int32(message.choiceId);
    }
    if (message.essay !== undefined) {
      EssayAnswer.encode(message.essay, writer.uint32(42).fork()).ldelim();
    }
    if (message.isCorrect === true) {
      writer.uint32(48).bool(message.isCorrect);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Answer {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAnswer();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.participantId = reader.int32();
          break;
        case 3:
          message.questionId = reader.int32();
          break;
        case 4:
          message.choiceId = reader.int32();
          break;
        case 5:
          message.essay = EssayAnswer.decode(reader, reader.uint32());
          break;
        case 6:
          message.isCorrect = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Answer {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      participantId: isSet(object.participantId)
        ? Number(object.participantId)
        : 0,
      questionId: isSet(object.questionId) ? Number(object.questionId) : 0,
      choiceId: isSet(object.choiceId) ? Number(object.choiceId) : undefined,
      essay: isSet(object.essay)
        ? EssayAnswer.fromJSON(object.essay)
        : undefined,
      isCorrect: isSet(object.isCorrect) ? Boolean(object.isCorrect) : false,
    };
  },

  toJSON(message: Answer): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.participantId !== undefined &&
      (obj.participantId = Math.round(message.participantId));
    message.questionId !== undefined &&
      (obj.questionId = Math.round(message.questionId));
    message.choiceId !== undefined &&
      (obj.choiceId = Math.round(message.choiceId));
    message.essay !== undefined &&
      (obj.essay = message.essay
        ? EssayAnswer.toJSON(message.essay)
        : undefined);
    message.isCorrect !== undefined && (obj.isCorrect = message.isCorrect);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Answer>, I>>(object: I): Answer {
    const message = createBaseAnswer();
    message.id = object.id ?? 0;
    message.participantId = object.participantId ?? 0;
    message.questionId = object.questionId ?? 0;
    message.choiceId = object.choiceId ?? undefined;
    message.essay =
      object.essay !== undefined && object.essay !== null
        ? EssayAnswer.fromPartial(object.essay)
        : undefined;
    message.isCorrect = object.isCorrect ?? false;
    return message;
  },
};

function createBaseEssayAnswer(): EssayAnswer {
  return { format: 0, content: "" };
}

export const EssayAnswer = {
  encode(
    message: EssayAnswer,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.format !== 0) {
      writer.uint32(32).int32(message.format);
    }
    if (message.content !== "") {
      writer.uint32(42).string(message.content);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EssayAnswer {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseEssayAnswer();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 4:
          message.format = reader.int32() as any;
          break;
        case 5:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): EssayAnswer {
    return {
      format: isSet(object.format) ? textFormatFromJSON(object.format) : 0,
      content: isSet(object.content) ? String(object.content) : "",
    };
  },

  toJSON(message: EssayAnswer): unknown {
    const obj: any = {};
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<EssayAnswer>, I>>(
    object: I
  ): EssayAnswer {
    const message = createBaseEssayAnswer();
    message.format = object.format ?? 0;
    message.content = object.content ?? "";
    return message;
  },
};

export const AnswerServiceService = {
  getAnswer: {
    path: "/testycool.v1.AnswerService/GetAnswer",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetAnswerRequest) =>
      Buffer.from(GetAnswerRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetAnswerRequest.decode(value),
    responseSerialize: (value: GetAnswerResponse) =>
      Buffer.from(GetAnswerResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetAnswerResponse.decode(value),
  },
  listAnswers: {
    path: "/testycool.v1.AnswerService/ListAnswers",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListAnswersRequest) =>
      Buffer.from(ListAnswersRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListAnswersRequest.decode(value),
    responseSerialize: (value: ListAnswersResponse) =>
      Buffer.from(ListAnswersResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListAnswersResponse.decode(value),
  },
  createAnswer: {
    path: "/testycool.v1.AnswerService/CreateAnswer",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateAnswerRequest) =>
      Buffer.from(CreateAnswerRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateAnswerRequest.decode(value),
    responseSerialize: (value: CreateAnswerResponse) =>
      Buffer.from(CreateAnswerResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => CreateAnswerResponse.decode(value),
  },
  updateAnswer: {
    path: "/testycool.v1.AnswerService/UpdateAnswer",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateAnswerRequest) =>
      Buffer.from(UpdateAnswerRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateAnswerRequest.decode(value),
    responseSerialize: (value: UpdateAnswerResponse) =>
      Buffer.from(UpdateAnswerResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => UpdateAnswerResponse.decode(value),
  },
  deleteAnswer: {
    path: "/testycool.v1.AnswerService/DeleteAnswer",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteAnswerRequest) =>
      Buffer.from(DeleteAnswerRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteAnswerRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface AnswerServiceServer extends UntypedServiceImplementation {
  getAnswer: handleUnaryCall<GetAnswerRequest, GetAnswerResponse>;
  listAnswers: handleUnaryCall<ListAnswersRequest, ListAnswersResponse>;
  createAnswer: handleUnaryCall<CreateAnswerRequest, CreateAnswerResponse>;
  updateAnswer: handleUnaryCall<UpdateAnswerRequest, UpdateAnswerResponse>;
  deleteAnswer: handleUnaryCall<DeleteAnswerRequest, Empty>;
}

export interface AnswerServiceClient extends Client {
  getAnswer(
    request: GetAnswerRequest,
    callback: (error: ServiceError | null, response: GetAnswerResponse) => void
  ): ClientUnaryCall;
  getAnswer(
    request: GetAnswerRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: GetAnswerResponse) => void
  ): ClientUnaryCall;
  getAnswer(
    request: GetAnswerRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: GetAnswerResponse) => void
  ): ClientUnaryCall;
  listAnswers(
    request: ListAnswersRequest,
    callback: (
      error: ServiceError | null,
      response: ListAnswersResponse
    ) => void
  ): ClientUnaryCall;
  listAnswers(
    request: ListAnswersRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListAnswersResponse
    ) => void
  ): ClientUnaryCall;
  listAnswers(
    request: ListAnswersRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListAnswersResponse
    ) => void
  ): ClientUnaryCall;
  createAnswer(
    request: CreateAnswerRequest,
    callback: (
      error: ServiceError | null,
      response: CreateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  createAnswer(
    request: CreateAnswerRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  createAnswer(
    request: CreateAnswerRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  updateAnswer(
    request: UpdateAnswerRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  updateAnswer(
    request: UpdateAnswerRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  updateAnswer(
    request: UpdateAnswerRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  deleteAnswer(
    request: DeleteAnswerRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAnswer(
    request: DeleteAnswerRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAnswer(
    request: DeleteAnswerRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const AnswerServiceClient = makeGenericClientConstructor(
  AnswerServiceService,
  "testycool.v1.AnswerService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): AnswerServiceClient;
  service: typeof AnswerServiceService;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
