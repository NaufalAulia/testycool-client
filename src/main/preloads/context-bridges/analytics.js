import {ipcRenderer} from "electron";

export const analyticsChannels = ['new-analytics', 'create-analytics', 'analytics-error'];

export const analyticsContextBridge = {
  apiKey: 'analytics',
  api: {
    createAnalytics: (createAnalyticsRequestDto) => {
      ipcRenderer.send('create-analytics', {...createAnalyticsRequestDto});
    },
    once: (channel, func) => {
      if (!analyticsChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid analytics channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.once(channel, (event, ...args) => func(...args));
    },
    on: (channel, func) => {
      if (!analyticsChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid analytics channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    },
  }
}