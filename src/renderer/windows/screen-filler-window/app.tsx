import React from "react";

export default function App(): JSX.Element {
  return (
    <div className="h-screen grid place-content-center gap-6 text-center bg-gray-100">
      <div className="grid place-content-center gap-6 text-center">
        <h1 className="text-5xl font-bold text-primary">
          TestyCool
        </h1>
        <h3 className="text-xl">
          You are not allowed to use external screen
        </h3>
      </div>
    </div>
  );
}