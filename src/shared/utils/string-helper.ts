import {LoadingType} from "../models";

export const LoadingTypeToString = (
  type: LoadingType,
  context: string
): string => {
  switch (type) {
    case LoadingType.CREATE:
      return `Creating ${context}`;
    case LoadingType.GETLIST:
      return `Loading ${context} list`;
    case LoadingType.GETENTRY:
      return `Loading ${context}`;
    case LoadingType.UPDATE:
      return `Updating ${context}`;
    case LoadingType.DELETE:
      return `Deleting ${context}`;
    default:
      return `Loading`;
  }
};

export const SecondsToDuration = (s?: number): string => {
  if (!s) {
    return '';
  }
  const hours = Math.floor(s / 3600);
  const minutes = Math.floor(s / 60) % 60;
  const seconds = Math.floor(s % 60);

  return [hours, minutes, seconds].map((v) => (v < 10 ? `0${v}` : v)).join(':');
};

export const TranslateAnalyticsMessage = (m: string): string => {
  switch (m) {
    case "Alt+F4":
    case "Force Close":
      return "Force closing app is not permitted during attempt!";
    case "PrintScreen":
      return "Taking screen capture is not permitted during attempt!";
    case "DevTools":
      return "Opening DevTools is not permitted during attempt!";
    case "Lost Focus":
      return "Switching out from the app is not permitted during attempt!";
    default:
      return "That action is not permitted during attempt!";
  }
}