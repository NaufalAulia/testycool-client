export interface QuestionFlag {
  isActive: boolean;
  questionId: number;
  note: string;
}
