import { BrowserWindow } from "electron";

export interface WindowRecipe {
  entry: string;
  options?: Electron.BrowserWindowConstructorOptions;
  extraActions?: (currentWindow: BrowserWindow) => any;
}