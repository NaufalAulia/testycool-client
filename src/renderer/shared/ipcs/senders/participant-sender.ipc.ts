import {GetParticipantRequestDto} from "../../../../shared/dtos";
import {participantAction} from "../../../../shared/redux/actions";
import store from "../../../store";

const participantSender = {
  getParticipant: (dto: GetParticipantRequestDto) => {
    store.dispatch(participantAction.getParticipant.pending());
    window.participant.getParticipant(dto);
  }
}

export default participantSender;