import {Answer} from "../models";

export interface UpdateAnswerResponseDto {
  answer: Answer | undefined;
}
