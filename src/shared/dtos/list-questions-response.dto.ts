import {Question} from "../models";

export class ListQuestionsResponseDto {
  public questions: Question[];
  public size: number;
  public page: number;
  public totalSize: number;
}