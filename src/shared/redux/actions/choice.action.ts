import {createAction} from '@reduxjs/toolkit';
import {ErrorResponseDto, GetChoiceResponseDto, ListChoicesResponseDto} from "../../dtos";

const choiceAction = {
  listChoices: {
    pending: createAction('[Choice] List Choices/pending'),
    fulfilled: createAction<ListChoicesResponseDto>('[Choice] List Choices/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Choice] List Choices/rejected'),
  },
  getChoice: {
    pending: createAction('[Choice] Get Choice/pending'),
    fulfilled: createAction<GetChoiceResponseDto>('[Choice] Get Choice/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Choice] Get Choice/rejected'),
  },
  setError: createAction<ErrorResponseDto>(
    '[Choice] set choice error'
  ),
  resetState: createAction('[Choice] reset state')
}

export default choiceAction;
