import {Attempt} from "../models";

export interface GetAttemptResponseDto {
  attempt: Attempt | undefined;
}
