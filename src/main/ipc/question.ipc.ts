import {ipcMain} from "electron";
import store from "../store";
import {Metadata} from "@grpc/grpc-js";
import Services, {GRPCCredential} from "../adapters/services";
import {QuestionServiceClient} from "../adapters/protobuf/gen/testycool/question";
import {GetQuestionRequestDto, ListQuestionsRequestDto} from "../../shared/dtos";

export const QuestionIpc = () => {
  ipcMain.on('list-questions', ((event, args: ListQuestionsRequestDto) => {
    const address = store.getState().auth.address;
    if (!Services.question) {
      Services.question = new QuestionServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }

    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.question.listQuestions({
        filter: {examId: args.filter.examId},
        page: 1,
        size: -1,
      }, metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('question-error', {code: error.code, details: error.details});
        }
        if (response) {
          event.reply('list-questions', response);
        }
      }));
  }))
  ipcMain.on('get-question', ((event, args: GetQuestionRequestDto) => {
    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.question.getQuestion(
      {id: args.id},
      metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('question-error', {code: error.code, details: error.details});
        }
        if (response) {
          event.reply('get-question', response);
        }
      }));
  }))
}
