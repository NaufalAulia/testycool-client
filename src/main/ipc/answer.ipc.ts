import {ipcMain} from 'electron';
import store from "../store";
import Services, {GRPCCredential} from "../adapters/services";
import {AnswerServiceClient} from "../adapters/protobuf/gen/testycool/answer";
import {Metadata} from "@grpc/grpc-js";
import {
  CreateAnswerRequestDto,
  GetAnswerRequestDto,
  ListAnswersRequestDto,
  UpdateAnswerRequestDto
} from "../../shared/dtos";

export const AnswerIpc = () => {
  ipcMain.on('list-answers', ((event, args: ListAnswersRequestDto) => {
    const address = store.getState().auth.address;
    if (!Services.answer) {
      Services.answer = new AnswerServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }

    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    const {participantId} = store.getState().auth.credentials;
    Services.answer.listAnswers({
        filter: {
          examId: args.filter.examId,
          participantId: args.filter.participantId || participantId
        },
        page: 1,
        size: -1,
      }, metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('answer-error', {code: error.code, details: error.details});
        }
        if (response) {
          event.reply('list-answers', response);
        }
      }));
  }))
  ipcMain.on('get-answer', ((event, args: GetAnswerRequestDto) => {
    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.answer.getAnswer(
      {id: args.id},
      metadata,
      ((error, response) => {
        console.log(response);
        console.log(error);
        if (error) {
          event.sender.send('answer-error', {code: error.code, details: error.details});
        }
        if (response) {
          event.reply('get-answer', response);
        }
      }));
  }))
  ipcMain.on('create-answer', ((event, args: CreateAnswerRequestDto) => {
    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.answer.createAnswer(
      args,
      metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('answer-error', {code: error.code, details: error.details});
        }
        if (response) {
          event.reply('create-answer', response);
        }
      }));
  }))
  ipcMain.on('update-answer', ((event, args: UpdateAnswerRequestDto) => {
    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.answer.updateAnswer(
      args,
      metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('answer-error', {code: error.code, details: error.details});
        }
        if (response) {
          event.reply('update-answer', response);
        }
      }));
  }))
}
