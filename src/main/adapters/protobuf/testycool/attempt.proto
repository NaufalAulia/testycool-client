// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

syntax = "proto3";

package testycool.v1;

import "google/api/annotations.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";

option go_package = "gitlab.com/testycool/server/api/protobuf/gen/go/testycool";


// -----------------------------------------------------------------------------
// Service Definitions
// -----------------------------------------------------------------------------

service AttemptService {
  rpc GetAttempt(GetAttemptRequest) returns (GetAttemptResponse) {
    option (google.api.http) = {
      get: "/v1/attempts/{id}"
      additional_bindings {
        get: "/v1/attempts/{participant_id}"
      }
    };
  }

  rpc ListAttempts(ListAttemptsRequest) returns (ListAttemptsResponse) {
    option (google.api.http) = {
      get: "/v1/attempts"
    };
  }

  rpc CreateAttempt(CreateAttemptRequest) returns (CreateAttemptResponse) {
    option (google.api.http) = {
      post: "/v1/attempts"
      body: "*"
    };
  }

  rpc UpdateAttempt(UpdateAttemptRequest) returns (UpdateAttemptResponse) {
    option (google.api.http) = {
      put: "/v1/attempts"
      body: "*"
    };
  }

  rpc DeleteAttempt(DeleteAttemptRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/attempts/{id}"
    };
  }
}


// -----------------------------------------------------------------------------
// Request and Response Definitions
// -----------------------------------------------------------------------------

message GetAttemptRequest {
  oneof identifier {
    int32 id = 1;
    int32 participant_id = 2;
  }
}

message GetAttemptResponse {
  Attempt attempt = 1;
}

message ListAttemptsRequest {
  int32 size = 1;
  int32 page = 2;
  message Filter {
    optional int32 exam_id = 2;
  }
  Filter filter = 3;
}

message ListAttemptsResponse {
  repeated Attempt attempts = 1;
  int32 size = 2;
  int32 page = 3;
  int32 total_size = 4;
}

message CreateAttemptRequest {
  int32 participant_id = 1;
  int32 exam_id = 2;
  google.protobuf.Timestamp created_at = 3;
}

message CreateAttemptResponse {
  Attempt attempt = 1;
}

message UpdateAttemptRequest {
  Attempt attempt = 1;
}

message UpdateAttemptResponse {
  Attempt attempt = 1;
}

message DeleteAttemptRequest {
  int32 id = 1;
}


// -----------------------------------------------------------------------------
// Resource Definitions
// -----------------------------------------------------------------------------

message Attempt {
  int32 id = 1;
  int32 participant_id = 2;
  int32 exam_id = 3;
  bool finished = 4;
  int32 corrects = 5;
  int32 wrongs = 6;
  int32 unanswered = 7;
  google.protobuf.Timestamp created_at = 8;
  google.protobuf.Timestamp updated_at = 9;
}

