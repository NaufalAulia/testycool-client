import {ExamStatus} from "./exam-status.model";

export interface Exam {
  id: number;
  password: string;
  title: string;
  timeLimit: number;
  startAt: number | undefined;
  status: ExamStatus;
}
