import {createReducer} from '@reduxjs/toolkit';
import {Answer, LoadingType} from "../../models";
import {answerAction} from "../actions";
import {ErrorResponseDto} from "../../dtos";

export interface AnswerState {
  answers?: Answer[];
  size: number;
  page: number;
  totalSize: number;
  currentAnswer?: Answer;
  loading: LoadingType;
  answerSaved: boolean;
  error?: ErrorResponseDto;
}

const initialState: AnswerState = {
  answers: undefined,
  size: -1,
  page: 1,
  totalSize: 0,
  currentAnswer: undefined,
  loading: LoadingType.IDLE,
  answerSaved: true,
  error: undefined,
};

const AnswerReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(answerAction.listAnswers.pending, (state) => ({
      ...state,
      loading: LoadingType.GETLIST,
    }))
    .addCase(answerAction.listAnswers.fulfilled, (state, action) => {
      return {
        ...state,
        answers: action.payload.answers,
        size: action.payload.size,
        page: action.payload.page,
        totalSize: action.payload.totalSize,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(answerAction.listAnswers.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(answerAction.getAnswer.pending, (state) => ({
      ...state,
      loading: LoadingType.GETENTRY,
    }))
    .addCase(answerAction.getAnswer.fulfilled, (state, action) => {
      return {
        ...state,
        currentAnswer: action.payload.answer,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(answerAction.getAnswer.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(answerAction.createAnswer.pending, (state) => ({
      ...state,
      loading: LoadingType.CREATE,
    }))
    .addCase(answerAction.createAnswer.fulfilled, (state, action) => {
      state.answers.push(action.payload.answer);
      state.currentAnswer = action.payload.answer;
      state.loading = LoadingType.IDLE;
      state.answerSaved = true;
    })
    .addCase(answerAction.createAnswer.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(answerAction.updateAnswer.pending, (state) => ({
      ...state,
      loading: LoadingType.UPDATE,
    }))
    .addCase(answerAction.updateAnswer.fulfilled, (state, action) => {
      const selectedAnswerIdx = state.answers.findIndex(a => a.id === action.payload.answer.id);

      state.answers[selectedAnswerIdx] = action.payload.answer;
      state.currentAnswer = action.payload.answer;
      state.loading = LoadingType.IDLE;
      state.answerSaved = true;

    })
    .addCase(answerAction.updateAnswer.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(answerAction.changeAnswer, (state, action) => {
      state.currentAnswer = state.answers.find(a => a.questionId === action.payload.questionId);
      state.loading = LoadingType.IDLE;
    })
    .addCase(answerAction.setAnswerSaved, (state, action) => {
      return {
        ...state,
        answerSaved: action.payload,
      };
    })
    .addCase(answerAction.setError, (state, action) => ({
      ...state,
      error: action.payload,
      loading: LoadingType.IDLE,
    }))
    .addCase(answerAction.resetState, () => {
      return {
        ...initialState
      }
    })
);

export default AnswerReducer;
