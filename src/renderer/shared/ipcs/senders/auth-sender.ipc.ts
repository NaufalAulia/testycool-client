import {AuthRequestDto} from "../../../../shared/dtos"
import {authAction} from "../../../../shared/redux/actions";
import store from "../../../store";

const authSender = {
  getAccessToken: (dto: AuthRequestDto) => {
    store.dispatch(authAction.getAccessToken.pending());
    window.auth.getAccessToken(dto);
  }
}

export default authSender;
