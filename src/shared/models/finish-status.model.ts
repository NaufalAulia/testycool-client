export enum FinishStatus {
  UNFINISHED = 0,
  REQUESTED = 1,
  FINISHED = 2,
  FORFEITED = 3,
}
