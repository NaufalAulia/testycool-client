import {ipcRenderer} from "electron";

export const answerChannels = ['list-answers', 'get-answer', 'create-answer', 'update-answer', 'answer-error'];

export const answerContextBridge = {
  apiKey: 'answer',
  api: {
    listAnswers: (listAnswersRequestDto) => {
      ipcRenderer.send('list-answers', {...listAnswersRequestDto});
    },
    getAnswer: (getAnswerRequestDto) => {
      ipcRenderer.send('get-answer', {...getAnswerRequestDto});
    },
    createAnswer: (createAnswerRequestDto) => {
      ipcRenderer.send('create-answer', {...createAnswerRequestDto});
    },
    updateAnswer: (updateAnswerRequestDto) => {
      ipcRenderer.send('update-answer', {...updateAnswerRequestDto});
    },
    once: (channel, func) => {
      if (!answerChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid answer channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.once(channel, (event, ...args) => func(...args));
    },
    on: (channel, func) => {
      if (!answerChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid answer channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    },
  }
}