export class AuthRequestDto {
  public address: string;
  public examPassword: string;
  public participantCode: string;
}
