import React, {useEffect, useState} from "react";
import {SecondsToDuration} from "../../../../shared/utils/string-helper";
import {Exam} from "../../../../shared/models";

export default function CountdownTimer(props: { exam?: Exam, onTimerEnd?: () => void }): JSX.Element {
  const {exam, onTimerEnd} = props;

  const remainingTime = (startAt: number, timeLimit: number) => {
    const timeElapsed = Math.abs(Date.now() - startAt) / 1000;
    return timeLimit - timeElapsed;
  };

  const [duration, setDuration] = useState('00:00:00');

  useEffect(() => {
    if (exam) {
      if (remainingTime(exam.startAt, exam.timeLimit) > 0) {
        setTimeout(() => {
          setDuration(SecondsToDuration(
            remainingTime(exam.startAt, exam.timeLimit)
          ));
        }, 1000);
      } else {
        if (onTimerEnd) onTimerEnd();
      }
    }
  }, [duration, exam]);

  return (
    <h1 className="text-center text-4xl font-bold">{duration}</h1>
  );
}