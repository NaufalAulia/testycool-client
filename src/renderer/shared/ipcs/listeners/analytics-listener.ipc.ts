import {CreateAnalyticsRequestDto, CreateAnalyticsResponseDto, ErrorResponseDto} from "../../../../shared/dtos";
import store from "../../../store";
import {analyticsAction} from "../../../../shared/redux/actions";

const analyticsListener = () => {
  window.analytics.on('new-analytics', (args: CreateAnalyticsRequestDto) => {
    store.dispatch(analyticsAction.createAnalytics.pending());
    window.analytics.createAnalytics(args);
  });
  window.analytics.on('create-analytics', (args: CreateAnalyticsResponseDto) => {
    store.dispatch(analyticsAction.createAnalytics.fulfilled(args));
  });
  window.analytics.on('analytics-error', (args: ErrorResponseDto) => {
    store.dispatch(analyticsAction.setError(args));
  });
}

export default analyticsListener;
