import {createAction} from '@reduxjs/toolkit';
import {CreateAnalyticsResponseDto, ErrorResponseDto} from "../../dtos";

const analyticsAction = {
  createAnalytics: {
    pending: createAction('[Analytics] Create Analytics/pending'),
    fulfilled: createAction<CreateAnalyticsResponseDto>('[Analytics] Create Analytics/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Analytics] Create Analytics/rejected'),
  },
  setError: createAction<ErrorResponseDto>(
    '[Analytics] set exam error'
  ),
  resetState: createAction('[Analytics] reset state')
}

export default analyticsAction;
