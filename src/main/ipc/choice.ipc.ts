import {ipcMain} from "electron";
import store from "../store";
import {Metadata} from "@grpc/grpc-js";
import Services, {GRPCCredential} from "../adapters/services";
import {ChoiceServiceClient} from "../adapters/protobuf/gen/testycool/choice";
import {GetChoiceRequestDto, ListChoicesRequestDto} from "../../shared/dtos";

export const ChoiceIpc = () => {
  ipcMain.on('list-choices', ((event, args: ListChoicesRequestDto) => {
    const address = store.getState().auth.address;
    if (!Services.choice) {
      Services.choice = new ChoiceServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }

    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.choice.listChoices({
        filter: {questionId: args.filter.questionId},
        page: 1,
        size: -1,
      }, metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('choice-error', {code: error.code, details: error.details});
        }
        if (response) {
          event.reply('list-choices', response);
        }
      }));
  }))
  ipcMain.on('get-choice', ((event, args: GetChoiceRequestDto) => {
    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.choice.getChoice(
      {id: args.id},
      metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('choice-error', {code: error.code, details: error.details});
        }
        if (response) {
          event.reply('get-choice', response);
        }
      }));
  }))
}
