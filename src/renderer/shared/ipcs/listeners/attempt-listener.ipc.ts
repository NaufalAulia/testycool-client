import {
  CreateAttemptResponseDto,
  ErrorResponseDto,
  GetAttemptResponseDto,
  UpdateAttemptResponseDto
} from "../../../../shared/dtos";
import {attemptAction} from "../../../../shared/redux/actions";
import store from "../../../store";
import {attemptSender} from "../index";
import {FinishStatus} from "../../../../shared/models";

const attemptListener = () => {
  window.attempt.on('get-attempt', (args: (GetAttemptResponseDto & { participantId: number; examId: number })) => {
    store.dispatch(attemptAction.getAttempt.fulfilled(args));
  });
  window.attempt.on('create-attempt', (args: CreateAttemptResponseDto) => {
    store.dispatch(attemptAction.createAttempt.fulfilled(args));
  });
  window.attempt.on('update-attempt', (args: UpdateAttemptResponseDto) => {
    store.dispatch(attemptAction.updateAttempt.fulfilled(args));
  });
  window.attempt.on('finish-attempt', () => {
    store.dispatch(attemptAction.updateFinishStatus(FinishStatus.REQUESTED));
  });
  window.attempt.on('forfeit-attempt', () => {
    const attemptState = store.getState().attempt.attempt;
    attemptSender.updateAttempt({
      attempt: {
        ...attemptState, finished: true,
        updatedAt: Date.now(),
      }
    });
    store.dispatch(attemptAction.updateFinishStatus(FinishStatus.FORFEITED));
  });
  window.attempt.on('attempt-error', (error: ErrorResponseDto) => {
    console.log(error);
    store.dispatch(attemptAction.setError(error));
  });
}

export default attemptListener;
