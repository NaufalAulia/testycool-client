import store from "../../../store";
import {ErrorResponseDto, GetParticipantResponseDto} from "../../../../shared/dtos";
import {participantAction} from "../../../../shared/redux/actions";

const participantListener = () => {
  window.participant.on('get-participant', (args: GetParticipantResponseDto) => {
    store.dispatch(participantAction.getParticipant.fulfilled(args));
  });
  window.participant.on('participant-error', (args: ErrorResponseDto) => {
    store.dispatch(participantAction.setError(args));
  });
}

export default participantListener;
