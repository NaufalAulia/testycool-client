import {createReducer} from '@reduxjs/toolkit';
import {LoadingType} from "../../models";
import participantAction from "../actions/participant.action";
import {Participant} from "../../models/participant.model";

export interface ExamState {
  participant?: Participant;
  loading: LoadingType;
  error?: any;
}

const initialState: ExamState = {
  participant: undefined,
  loading: LoadingType.IDLE,
};

const ParticipantReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(participantAction.getParticipant.pending, (state) => ({
      ...state,
      loading: LoadingType.GETENTRY,
    }))
    .addCase(participantAction.getParticipant.fulfilled, (state, action) => {
      return {
        ...state,
        participant: action.payload.participant,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(participantAction.getParticipant.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(participantAction.setError, (state, action) => ({
      ...state,
      error: action.payload,
      loading: LoadingType.IDLE,
    }))
    .addCase(participantAction.resetState, () => {
      return {
        ...initialState
      }
    })
);

export default ParticipantReducer;
