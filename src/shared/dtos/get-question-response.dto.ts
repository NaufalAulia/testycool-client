import {Question} from "../models";

export interface GetQuestionResponseDto {
  question: Question | undefined;
}
