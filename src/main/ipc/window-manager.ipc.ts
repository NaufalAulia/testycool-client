import {ipcMain} from "electron"
import LoginWindow from "../windows/recipes/login.window";
import {browserWindows, createNewWindow} from "../windows/window-manager";
import {MainWindow} from "../windows/recipes";
import store from "../store";
import {attemptAction, authAction, examAction, participantAction} from "../../shared/redux/actions";

export const WindowManagerIpc = () => {
  ipcMain.on('switch-to-login', () => {
    browserWindows.mainWindow.close();
    store.dispatch(authAction.resetState());
    store.dispatch(examAction.resetState());
    store.dispatch(participantAction.resetState());
    store.dispatch(attemptAction.resetState());

    createNewWindow(LoginWindow);
  })
  ipcMain.on('switch-to-main', () => {
    browserWindows.loginWindow.close();

    createNewWindow(MainWindow);
  })
}
