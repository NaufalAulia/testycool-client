import {Participant} from "../models/participant.model";

export class GetParticipantResponseDto {
  participant: Participant
}