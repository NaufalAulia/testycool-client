import React, {useEffect} from "react";
import {useNavigate} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../store";
import {attemptSender, examSender, participantSender} from "../../shared/ipcs";
import {ExamStatus, FinishStatus, LoadingType} from "../../../shared/models";
import {LoadingTypeToString} from "../../../shared/utils/string-helper";
import {attemptAction} from "../../../shared/redux/actions";

export default function Waiting(): JSX.Element {
  const exam = useSelector((state: RootState) => state.exam.exam);
  const participant = useSelector((state: RootState) => state.participant.participant);
  const attempt = useSelector((state: RootState) => state.attempt.attempt);
  const examLoading = useSelector((state: RootState) => state.exam.loading);
  const participantLoading = useSelector((state: RootState) => state.participant.loading);
  const attemptLoading = useSelector((state: RootState) => state.attempt.loading);
  const attemptError = useSelector((state: RootState) => state.attempt.error);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  useEffect(() => {
      if (exam && participant && attempt) {
        if (exam.status === ExamStatus.DONE) {
          if (attempt.finished) {
            dispatch(attemptAction.updateFinishStatus(FinishStatus.FINISHED));
            return navigate('/result');
          } else {
            attemptSender.updateAttempt({
              attempt: {
                ...attempt,
                finished: true,
                updatedAt: Date.now(),
              }
            });
            return;
          }
        } else if (exam.status === ExamStatus.STARTED) {
          if (attempt.finished) {
            dispatch(attemptAction.updateFinishStatus(FinishStatus.FINISHED));
            return navigate('/result');
          } else {
            return navigate('/attempt');
          }
        }
      }
      const timeout = setTimeout(() => {
        if (examLoading === LoadingType.IDLE && (!exam || exam.status === ExamStatus.WAITING)) {
          examSender.getExam({});
        }
        if (participantLoading === LoadingType.IDLE && !participant) {
          participantSender.getParticipant({});
        }
        if (attemptLoading === LoadingType.IDLE && exam && participant && !attempt) {
          if (!attemptError) {
            attemptSender.getAttempt({id: undefined, participantId: participant.id, examId: exam.id});
          } else if (attemptError.code === 5) {
            attemptSender.createAttempt({
              participantId: participant.id,
              examId: exam.id,
              createdAt: Date.now(),
            })
          }
        }
      }, 1000);
      return () => clearTimeout(timeout);
    },
    [exam, participant, attempt, examLoading, participantLoading, attemptLoading, attemptError, navigate, dispatch]
  );

  return (
    <div className="h-screen grid place-content-center gap-6 text-center bg-gray-100">
      <div className="grid place-content-center gap-6 text-center">
        <h1 className="text-5xl font-bold text-primary">
          {!exam ? `[${LoadingTypeToString(examLoading, 'Exam')}]` : "Waiting for the host to start The Exam"}
        </h1>
        <h3 className="text-xl">
          {`You are registered 
          as ${participant?.name || `[${LoadingTypeToString(participantLoading, 'Participant')}]`} 
          to ${exam?.title || `[${LoadingTypeToString(examLoading, 'Exam')}]`}`
          }
        </h3>
        <h3 className="text-xl">
          {`The Exam is scheduled to start at 
          ${new Date(exam?.startAt).toLocaleString() || `[${LoadingTypeToString(examLoading, 'Exam')}]`}`
          }
        </h3>
      </div>
    </div>
  );
}
