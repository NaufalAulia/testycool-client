import {TextFormat} from "./text-format.model";

export interface Choice {
  id: number;
  questionId: number;
  isCorrect: boolean;
  format: TextFormat;
  content: string;
}
