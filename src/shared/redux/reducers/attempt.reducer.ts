import {createReducer} from '@reduxjs/toolkit';
import {Attempt, FinishStatus, LoadingType} from "../../models";
import {attemptAction} from "../actions";
import {ErrorResponseDto} from "../../dtos";

export interface AttemptState {
  attempt?: Attempt;
  loading: LoadingType;
  finishStatus: FinishStatus;
  error?: ErrorResponseDto;
}

const initialState: AttemptState = {
  attempt: undefined,
  loading: LoadingType.IDLE,
  finishStatus: FinishStatus.UNFINISHED,
  error: undefined
};

const AttemptReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(attemptAction.getAttempt.pending, (state) => {
      return {
        ...state,
        loading: LoadingType.GETENTRY,
      }
    })
    .addCase(attemptAction.getAttempt.fulfilled, (state, action) => {
      return {
        ...state,
        attempt: action.payload.attempt,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(attemptAction.getAttempt.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(attemptAction.createAttempt.pending, (state) => {
      return {
        ...state,
        loading: LoadingType.CREATE,
      }
    })
    .addCase(attemptAction.createAttempt.fulfilled, (state, action) => {
      return {
        ...state,
        attempt: action.payload.attempt,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(attemptAction.createAttempt.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(attemptAction.updateAttempt.pending, (state) => {
      return {
        ...state,
        loading: LoadingType.UPDATE,
      }
    })
    .addCase(attemptAction.updateAttempt.fulfilled, (state, action) => {
      return {
        ...state,
        attempt: action.payload.attempt,
        finishStatus: state.finishStatus === FinishStatus.FORFEITED ? state.finishStatus : FinishStatus.FINISHED,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(attemptAction.updateAttempt.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(attemptAction.updateFinishStatus, (state, action) => {
      return {
        ...state,
        finishStatus: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(attemptAction.setError, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      }
    })
    .addCase(attemptAction.resetState, () => {
      return {
        ...initialState
      }
    })
);

export default AttemptReducer;
