import {Choice} from "../models";

export class ListChoicesResponseDto {
  public choices: Choice[];
  public size: number;
  public page: number;
  public totalSize: number;
}