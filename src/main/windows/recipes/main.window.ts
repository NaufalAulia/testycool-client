import {Notification, screen} from "electron";
import {WindowRecipe} from "./interfaces/window-recipe";
import {browserWindows, setupExtraWindows} from "../window-manager";
import store from "../../store";
import {ExamStatus} from "../../../shared/models";

declare const MAIN_WINDOW_WEBPACK_ENTRY: string;
declare const MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY: string;

const MainWindow: WindowRecipe = {
  entry: MAIN_WINDOW_WEBPACK_ENTRY,
  options: {
    title: "main_window",
    kiosk: true,
    alwaysOnTop: true,
    autoHideMenuBar: true,
    fullscreen: true,
    webPreferences: {
      preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
      accessibleTitle: "main_window",
      // devTools: false,
    }
  },
  extraActions: (window) => {
    // Debug
    //window.webContents.openDevTools();

    window.setVisibleOnAllWorkspaces(true, {
      visibleOnFullScreen: true
    });

    window.restore();
    window.focus();

    // Set up hotkeys capture
    window.webContents.on('before-input-event', (event, input) => {
      const exam = store.getState().exam.exam;
      const attempt = store.getState().attempt.attempt;
      if (attempt && exam && !attempt.finished && exam.status === ExamStatus.STARTED) {
        if (
          input.alt && input.key === "F4" ||
          ((process.platform === "darwin") && (
            input.meta && input.key === "Q"
          ))
        ) {
          const participantState = store.getState().participant.participant;
          window.webContents.send('new-analytics', {
            participantId: participantState.id,
            examId: participantState.examId,
            message: "Alt+F4",
            createdAt: new Date(Date.now()),
          });
          window.webContents.send("finish-attempt", {});
          event.preventDefault();
        }

        if (
          input.key === "PrintScreen" ||
          ((process.platform === "darwin") && (input.shift && input.meta && input.key === "3"))
        ) {
          const participantState = store.getState().participant.participant;
          window.webContents.send('new-analytics', {
            participantId: participantState.id,
            examId: participantState.examId,
            message: "PrintScreen",
            createdAt: new Date(Date.now()),
          });
          event.preventDefault();
        }

        if ((
            input.control && input.shift && (input.key === "I" || input.key === "C" || input.key === "J")) ||
          ((process.platform === "darwin") &&
            (input.meta && input.alt && (input.key === "I" || input.key === "C" || input.key === "J"))
          ) ||
          input.key === "F12"
        ) {
          const participantState = store.getState().participant.participant;
          window.webContents.send('new-analytics', {
            participantId: participantState.id,
            examId: participantState.examId,
            message: "DevTools",
            createdAt: new Date(Date.now()),
          });
          event.preventDefault();
        }
      }
    });

    // Create extra window to cover external displays
    setupExtraWindows(window);

    screen.on('display-added', () => {
      console.log('display added');
      setupExtraWindows();
    });


    screen.on('display-removed', () => {
      console.log('display removed');
      setupExtraWindows();
    });

    window.removeMenu();
    window.on('closed', () => {
      browserWindows.mainWindow = undefined;
    });

    let forfeitTimeout: NodeJS.Timeout;
    let forfeitNotification: Notification;

    window.on('focus', () => {
      const attempt = store.getState().attempt.attempt;
      const exam = store.getState().exam.exam;
      if (attempt && exam && !attempt.finished && exam.status === ExamStatus.STARTED) {
        // Cancel forfeit timeout when user return focus
        clearTimeout(forfeitTimeout);
        if (forfeitNotification) {
          forfeitNotification.close();
        }
      }

    });

    window.on('blur', () => {
      const attempt = store.getState().attempt.attempt;
      const exam = store.getState().exam.exam;
      if (attempt && exam && !attempt.finished && exam.status === ExamStatus.STARTED) {
        const participantState = store.getState().participant.participant;
        window.webContents.send('new-analytics', {
          participantId: participantState.id,
          examId: participantState.examId,
          message: "Lost Focus",
          createdAt: new Date(Date.now()),
        });
        // Show warning notification
        forfeitNotification = new Notification({
          title: "WARNING",
          body: "Please return immediately to the exam, by clicking on the exam window before this notification closes",
        });

        forfeitNotification.show();
        // Set timeout for 3s, after which user will forfeit from the exam
        forfeitTimeout = setTimeout(() => {
          window.webContents.send('forfeit-attempt', {});
          forfeitNotification.close();
        }, 3000);
      }
    });

    window.on('close', (event) => {
      const attempt = store.getState().attempt.attempt;
      const exam = store.getState().exam.exam;
      if (attempt && exam && !attempt.finished && exam.status === ExamStatus.STARTED) {
        const participantState = store.getState().participant.participant;
        window.webContents.send('new-analytics', {
          participantId: participantState.id,
          examId: participantState.examId,
          message: "Force Close",
          createdAt: new Date(Date.now()),
        });
        window.webContents.send("finish-attempt", {});
        event.preventDefault();
      }
    })

    browserWindows.mainWindow = window;
  }
};

export default MainWindow;