import {QuestionType} from "./question-type.model";
import {TextFormat} from "./text-format.model";

export interface Question {
  id: number;
  examId: number;
  type: QuestionType;
  format: TextFormat;
  content: string;
}
