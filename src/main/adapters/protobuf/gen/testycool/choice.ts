/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import {
  TextFormat,
  textFormatFromJSON,
  textFormatToJSON,
} from "../testycool/text_format";
import { Empty } from "../google/protobuf/empty";

export const protobufPackage = "testycool.v1";

export interface GetChoiceRequest {
  id: number;
}

export interface GetChoiceResponse {
  choice: Choice | undefined;
}

export interface ListChoicesRequest {
  size: number;
  page: number;
  filter: ListChoicesRequest_Filter | undefined;
}

export interface ListChoicesRequest_Filter {
  questionId?: number | undefined;
}

export interface ListChoicesResponse {
  choices: Choice[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateChoiceRequest {
  questionId: number;
  isCorrect: boolean;
  format: TextFormat;
  content: string;
}

export interface CreateChoiceResponse {
  choice: Choice | undefined;
}

export interface UpdateChoiceRequest {
  choice: Choice | undefined;
}

export interface UpdateChoiceResponse {
  choice: Choice | undefined;
}

export interface DeleteChoiceRequest {
  id: number;
}

export interface Choice {
  id: number;
  questionId: number;
  isCorrect: boolean;
  format: TextFormat;
  content: string;
}

function createBaseGetChoiceRequest(): GetChoiceRequest {
  return { id: 0 };
}

export const GetChoiceRequest = {
  encode(
    message: GetChoiceRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetChoiceRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetChoiceRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetChoiceRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: GetChoiceRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetChoiceRequest>, I>>(
    object: I
  ): GetChoiceRequest {
    const message = createBaseGetChoiceRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseGetChoiceResponse(): GetChoiceResponse {
  return { choice: undefined };
}

export const GetChoiceResponse = {
  encode(
    message: GetChoiceResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.choice !== undefined) {
      Choice.encode(message.choice, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetChoiceResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetChoiceResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choice = Choice.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetChoiceResponse {
    return {
      choice: isSet(object.choice) ? Choice.fromJSON(object.choice) : undefined,
    };
  },

  toJSON(message: GetChoiceResponse): unknown {
    const obj: any = {};
    message.choice !== undefined &&
      (obj.choice = message.choice ? Choice.toJSON(message.choice) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetChoiceResponse>, I>>(
    object: I
  ): GetChoiceResponse {
    const message = createBaseGetChoiceResponse();
    message.choice =
      object.choice !== undefined && object.choice !== null
        ? Choice.fromPartial(object.choice)
        : undefined;
    return message;
  },
};

function createBaseListChoicesRequest(): ListChoicesRequest {
  return { size: 0, page: 0, filter: undefined };
}

export const ListChoicesRequest = {
  encode(
    message: ListChoicesRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListChoicesRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListChoicesRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListChoicesRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListChoicesRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListChoicesRequest {
    return {
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      filter: isSet(object.filter)
        ? ListChoicesRequest_Filter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: ListChoicesRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListChoicesRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListChoicesRequest>, I>>(
    object: I
  ): ListChoicesRequest {
    const message = createBaseListChoicesRequest();
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? ListChoicesRequest_Filter.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

function createBaseListChoicesRequest_Filter(): ListChoicesRequest_Filter {
  return { questionId: undefined };
}

export const ListChoicesRequest_Filter = {
  encode(
    message: ListChoicesRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.questionId !== undefined) {
      writer.uint32(8).int32(message.questionId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListChoicesRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListChoicesRequest_Filter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.questionId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListChoicesRequest_Filter {
    return {
      questionId: isSet(object.questionId)
        ? Number(object.questionId)
        : undefined,
    };
  },

  toJSON(message: ListChoicesRequest_Filter): unknown {
    const obj: any = {};
    message.questionId !== undefined &&
      (obj.questionId = Math.round(message.questionId));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListChoicesRequest_Filter>, I>>(
    object: I
  ): ListChoicesRequest_Filter {
    const message = createBaseListChoicesRequest_Filter();
    message.questionId = object.questionId ?? undefined;
    return message;
  },
};

function createBaseListChoicesResponse(): ListChoicesResponse {
  return { choices: [], size: 0, page: 0, totalSize: 0 };
}

export const ListChoicesResponse = {
  encode(
    message: ListChoicesResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.choices) {
      Choice.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListChoicesResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListChoicesResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choices.push(Choice.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListChoicesResponse {
    return {
      choices: Array.isArray(object?.choices)
        ? object.choices.map((e: any) => Choice.fromJSON(e))
        : [],
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      totalSize: isSet(object.totalSize) ? Number(object.totalSize) : 0,
    };
  },

  toJSON(message: ListChoicesResponse): unknown {
    const obj: any = {};
    if (message.choices) {
      obj.choices = message.choices.map((e) =>
        e ? Choice.toJSON(e) : undefined
      );
    } else {
      obj.choices = [];
    }
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.totalSize !== undefined &&
      (obj.totalSize = Math.round(message.totalSize));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListChoicesResponse>, I>>(
    object: I
  ): ListChoicesResponse {
    const message = createBaseListChoicesResponse();
    message.choices = object.choices?.map((e) => Choice.fromPartial(e)) || [];
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.totalSize = object.totalSize ?? 0;
    return message;
  },
};

function createBaseCreateChoiceRequest(): CreateChoiceRequest {
  return { questionId: 0, isCorrect: false, format: 0, content: "" };
}

export const CreateChoiceRequest = {
  encode(
    message: CreateChoiceRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.questionId !== 0) {
      writer.uint32(8).int32(message.questionId);
    }
    if (message.isCorrect === true) {
      writer.uint32(16).bool(message.isCorrect);
    }
    if (message.format !== 0) {
      writer.uint32(24).int32(message.format);
    }
    if (message.content !== "") {
      writer.uint32(34).string(message.content);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CreateChoiceRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateChoiceRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.questionId = reader.int32();
          break;
        case 2:
          message.isCorrect = reader.bool();
          break;
        case 3:
          message.format = reader.int32() as any;
          break;
        case 4:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateChoiceRequest {
    return {
      questionId: isSet(object.questionId) ? Number(object.questionId) : 0,
      isCorrect: isSet(object.isCorrect) ? Boolean(object.isCorrect) : false,
      format: isSet(object.format) ? textFormatFromJSON(object.format) : 0,
      content: isSet(object.content) ? String(object.content) : "",
    };
  },

  toJSON(message: CreateChoiceRequest): unknown {
    const obj: any = {};
    message.questionId !== undefined &&
      (obj.questionId = Math.round(message.questionId));
    message.isCorrect !== undefined && (obj.isCorrect = message.isCorrect);
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateChoiceRequest>, I>>(
    object: I
  ): CreateChoiceRequest {
    const message = createBaseCreateChoiceRequest();
    message.questionId = object.questionId ?? 0;
    message.isCorrect = object.isCorrect ?? false;
    message.format = object.format ?? 0;
    message.content = object.content ?? "";
    return message;
  },
};

function createBaseCreateChoiceResponse(): CreateChoiceResponse {
  return { choice: undefined };
}

export const CreateChoiceResponse = {
  encode(
    message: CreateChoiceResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.choice !== undefined) {
      Choice.encode(message.choice, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateChoiceResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateChoiceResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choice = Choice.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateChoiceResponse {
    return {
      choice: isSet(object.choice) ? Choice.fromJSON(object.choice) : undefined,
    };
  },

  toJSON(message: CreateChoiceResponse): unknown {
    const obj: any = {};
    message.choice !== undefined &&
      (obj.choice = message.choice ? Choice.toJSON(message.choice) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateChoiceResponse>, I>>(
    object: I
  ): CreateChoiceResponse {
    const message = createBaseCreateChoiceResponse();
    message.choice =
      object.choice !== undefined && object.choice !== null
        ? Choice.fromPartial(object.choice)
        : undefined;
    return message;
  },
};

function createBaseUpdateChoiceRequest(): UpdateChoiceRequest {
  return { choice: undefined };
}

export const UpdateChoiceRequest = {
  encode(
    message: UpdateChoiceRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.choice !== undefined) {
      Choice.encode(message.choice, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UpdateChoiceRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateChoiceRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choice = Choice.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateChoiceRequest {
    return {
      choice: isSet(object.choice) ? Choice.fromJSON(object.choice) : undefined,
    };
  },

  toJSON(message: UpdateChoiceRequest): unknown {
    const obj: any = {};
    message.choice !== undefined &&
      (obj.choice = message.choice ? Choice.toJSON(message.choice) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateChoiceRequest>, I>>(
    object: I
  ): UpdateChoiceRequest {
    const message = createBaseUpdateChoiceRequest();
    message.choice =
      object.choice !== undefined && object.choice !== null
        ? Choice.fromPartial(object.choice)
        : undefined;
    return message;
  },
};

function createBaseUpdateChoiceResponse(): UpdateChoiceResponse {
  return { choice: undefined };
}

export const UpdateChoiceResponse = {
  encode(
    message: UpdateChoiceResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.choice !== undefined) {
      Choice.encode(message.choice, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateChoiceResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateChoiceResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choice = Choice.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateChoiceResponse {
    return {
      choice: isSet(object.choice) ? Choice.fromJSON(object.choice) : undefined,
    };
  },

  toJSON(message: UpdateChoiceResponse): unknown {
    const obj: any = {};
    message.choice !== undefined &&
      (obj.choice = message.choice ? Choice.toJSON(message.choice) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateChoiceResponse>, I>>(
    object: I
  ): UpdateChoiceResponse {
    const message = createBaseUpdateChoiceResponse();
    message.choice =
      object.choice !== undefined && object.choice !== null
        ? Choice.fromPartial(object.choice)
        : undefined;
    return message;
  },
};

function createBaseDeleteChoiceRequest(): DeleteChoiceRequest {
  return { id: 0 };
}

export const DeleteChoiceRequest = {
  encode(
    message: DeleteChoiceRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DeleteChoiceRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDeleteChoiceRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteChoiceRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: DeleteChoiceRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<DeleteChoiceRequest>, I>>(
    object: I
  ): DeleteChoiceRequest {
    const message = createBaseDeleteChoiceRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseChoice(): Choice {
  return { id: 0, questionId: 0, isCorrect: false, format: 0, content: "" };
}

export const Choice = {
  encode(
    message: Choice,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.questionId !== 0) {
      writer.uint32(16).int32(message.questionId);
    }
    if (message.isCorrect === true) {
      writer.uint32(24).bool(message.isCorrect);
    }
    if (message.format !== 0) {
      writer.uint32(32).int32(message.format);
    }
    if (message.content !== "") {
      writer.uint32(42).string(message.content);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Choice {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseChoice();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.questionId = reader.int32();
          break;
        case 3:
          message.isCorrect = reader.bool();
          break;
        case 4:
          message.format = reader.int32() as any;
          break;
        case 5:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Choice {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      questionId: isSet(object.questionId) ? Number(object.questionId) : 0,
      isCorrect: isSet(object.isCorrect) ? Boolean(object.isCorrect) : false,
      format: isSet(object.format) ? textFormatFromJSON(object.format) : 0,
      content: isSet(object.content) ? String(object.content) : "",
    };
  },

  toJSON(message: Choice): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.questionId !== undefined &&
      (obj.questionId = Math.round(message.questionId));
    message.isCorrect !== undefined && (obj.isCorrect = message.isCorrect);
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Choice>, I>>(object: I): Choice {
    const message = createBaseChoice();
    message.id = object.id ?? 0;
    message.questionId = object.questionId ?? 0;
    message.isCorrect = object.isCorrect ?? false;
    message.format = object.format ?? 0;
    message.content = object.content ?? "";
    return message;
  },
};

export const ChoiceServiceService = {
  getChoice: {
    path: "/testycool.v1.ChoiceService/GetChoice",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetChoiceRequest) =>
      Buffer.from(GetChoiceRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetChoiceRequest.decode(value),
    responseSerialize: (value: GetChoiceResponse) =>
      Buffer.from(GetChoiceResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetChoiceResponse.decode(value),
  },
  listChoices: {
    path: "/testycool.v1.ChoiceService/ListChoices",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListChoicesRequest) =>
      Buffer.from(ListChoicesRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListChoicesRequest.decode(value),
    responseSerialize: (value: ListChoicesResponse) =>
      Buffer.from(ListChoicesResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListChoicesResponse.decode(value),
  },
  createChoice: {
    path: "/testycool.v1.ChoiceService/CreateChoice",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateChoiceRequest) =>
      Buffer.from(CreateChoiceRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateChoiceRequest.decode(value),
    responseSerialize: (value: CreateChoiceResponse) =>
      Buffer.from(CreateChoiceResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => CreateChoiceResponse.decode(value),
  },
  updateChoice: {
    path: "/testycool.v1.ChoiceService/UpdateChoice",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateChoiceRequest) =>
      Buffer.from(UpdateChoiceRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateChoiceRequest.decode(value),
    responseSerialize: (value: UpdateChoiceResponse) =>
      Buffer.from(UpdateChoiceResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => UpdateChoiceResponse.decode(value),
  },
  deleteChoice: {
    path: "/testycool.v1.ChoiceService/DeleteChoice",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteChoiceRequest) =>
      Buffer.from(DeleteChoiceRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteChoiceRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface ChoiceServiceServer extends UntypedServiceImplementation {
  getChoice: handleUnaryCall<GetChoiceRequest, GetChoiceResponse>;
  listChoices: handleUnaryCall<ListChoicesRequest, ListChoicesResponse>;
  createChoice: handleUnaryCall<CreateChoiceRequest, CreateChoiceResponse>;
  updateChoice: handleUnaryCall<UpdateChoiceRequest, UpdateChoiceResponse>;
  deleteChoice: handleUnaryCall<DeleteChoiceRequest, Empty>;
}

export interface ChoiceServiceClient extends Client {
  getChoice(
    request: GetChoiceRequest,
    callback: (error: ServiceError | null, response: GetChoiceResponse) => void
  ): ClientUnaryCall;
  getChoice(
    request: GetChoiceRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: GetChoiceResponse) => void
  ): ClientUnaryCall;
  getChoice(
    request: GetChoiceRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: GetChoiceResponse) => void
  ): ClientUnaryCall;
  listChoices(
    request: ListChoicesRequest,
    callback: (
      error: ServiceError | null,
      response: ListChoicesResponse
    ) => void
  ): ClientUnaryCall;
  listChoices(
    request: ListChoicesRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListChoicesResponse
    ) => void
  ): ClientUnaryCall;
  listChoices(
    request: ListChoicesRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListChoicesResponse
    ) => void
  ): ClientUnaryCall;
  createChoice(
    request: CreateChoiceRequest,
    callback: (
      error: ServiceError | null,
      response: CreateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  createChoice(
    request: CreateChoiceRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  createChoice(
    request: CreateChoiceRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  updateChoice(
    request: UpdateChoiceRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  updateChoice(
    request: UpdateChoiceRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  updateChoice(
    request: UpdateChoiceRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  deleteChoice(
    request: DeleteChoiceRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteChoice(
    request: DeleteChoiceRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteChoice(
    request: DeleteChoiceRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const ChoiceServiceClient = makeGenericClientConstructor(
  ChoiceServiceService,
  "testycool.v1.ChoiceService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): ChoiceServiceClient;
  service: typeof ChoiceServiceService;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
