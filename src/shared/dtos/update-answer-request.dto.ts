import {Answer} from "../models";

export interface UpdateAnswerRequestDto {
  answer: Answer | undefined;
}
