import {AuthResponseDto, ErrorResponseDto} from "../../../../shared/dtos";
import {authAction} from "../../../../shared/redux/actions";
import store from "../../../store";
import {windowManagerSender} from "../index";

const authListener = () => {
  window.auth.on('get-access-token', (
    args: (AuthResponseDto & { participantCode: string; participantId: number; examPassword: string })
  ) => {
    store.dispatch(authAction.getAccessToken.fulfilled(args));
    store.dispatch(authAction.setCredentials(args));
    windowManagerSender.switchToMain();
  });
  window.auth.on('auth-error', (args: ErrorResponseDto) => {
      store.dispatch(authAction.setError(args));
    }
  );
}

export default authListener;
