/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import { Timestamp } from "../google/protobuf/timestamp";
import { Empty } from "../google/protobuf/empty";

export const protobufPackage = "testycool.v1";

export interface GetAnalyticsRequest {
  id: number;
}

export interface GetAnalyticsResponse {
  analytics: Analytics | undefined;
}

export interface ListAnalyticsRequest {
  size: number;
  page: number;
  filter: ListAnalyticsRequest_Filter | undefined;
}

export interface ListAnalyticsRequest_Filter {
  examId?: number | undefined;
}

export interface ListAnalyticsResponse {
  analytics: Analytics[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateAnalyticsRequest {
  participantId: number;
  examId: number;
  message: string;
  createdAt: Date | undefined;
}

export interface CreateAnalyticsResponse {
  analytics: Analytics | undefined;
}

export interface UpdateAnalyticsRequest {
  analytics: Analytics | undefined;
}

export interface UpdateAnalyticsResponse {
  analytics: Analytics | undefined;
}

export interface DeleteAnalyticsRequest {
  id: number;
}

export interface Analytics {
  id: number;
  participantId: number;
  examId: number;
  message: string;
  createdAt: Date | undefined;
}

function createBaseGetAnalyticsRequest(): GetAnalyticsRequest {
  return { id: 0 };
}

export const GetAnalyticsRequest = {
  encode(
    message: GetAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAnalyticsRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAnalyticsRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: GetAnalyticsRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAnalyticsRequest>, I>>(
    object: I
  ): GetAnalyticsRequest {
    const message = createBaseGetAnalyticsRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseGetAnalyticsResponse(): GetAnalyticsResponse {
  return { analytics: undefined };
}

export const GetAnalyticsResponse = {
  encode(
    message: GetAnalyticsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.analytics !== undefined) {
      Analytics.encode(message.analytics, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetAnalyticsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAnalyticsResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics = Analytics.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAnalyticsResponse {
    return {
      analytics: isSet(object.analytics)
        ? Analytics.fromJSON(object.analytics)
        : undefined,
    };
  },

  toJSON(message: GetAnalyticsResponse): unknown {
    const obj: any = {};
    message.analytics !== undefined &&
      (obj.analytics = message.analytics
        ? Analytics.toJSON(message.analytics)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAnalyticsResponse>, I>>(
    object: I
  ): GetAnalyticsResponse {
    const message = createBaseGetAnalyticsResponse();
    message.analytics =
      object.analytics !== undefined && object.analytics !== null
        ? Analytics.fromPartial(object.analytics)
        : undefined;
    return message;
  },
};

function createBaseListAnalyticsRequest(): ListAnalyticsRequest {
  return { size: 0, page: 0, filter: undefined };
}

export const ListAnalyticsRequest = {
  encode(
    message: ListAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListAnalyticsRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListAnalyticsRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListAnalyticsRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnalyticsRequest {
    return {
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      filter: isSet(object.filter)
        ? ListAnalyticsRequest_Filter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: ListAnalyticsRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListAnalyticsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListAnalyticsRequest>, I>>(
    object: I
  ): ListAnalyticsRequest {
    const message = createBaseListAnalyticsRequest();
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? ListAnalyticsRequest_Filter.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

function createBaseListAnalyticsRequest_Filter(): ListAnalyticsRequest_Filter {
  return { examId: undefined };
}

export const ListAnalyticsRequest_Filter = {
  encode(
    message: ListAnalyticsRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== undefined) {
      writer.uint32(16).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAnalyticsRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListAnalyticsRequest_Filter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnalyticsRequest_Filter {
    return {
      examId: isSet(object.examId) ? Number(object.examId) : undefined,
    };
  },

  toJSON(message: ListAnalyticsRequest_Filter): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListAnalyticsRequest_Filter>, I>>(
    object: I
  ): ListAnalyticsRequest_Filter {
    const message = createBaseListAnalyticsRequest_Filter();
    message.examId = object.examId ?? undefined;
    return message;
  },
};

function createBaseListAnalyticsResponse(): ListAnalyticsResponse {
  return { analytics: [], size: 0, page: 0, totalSize: 0 };
}

export const ListAnalyticsResponse = {
  encode(
    message: ListAnalyticsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.analytics) {
      Analytics.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAnalyticsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListAnalyticsResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics.push(Analytics.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnalyticsResponse {
    return {
      analytics: Array.isArray(object?.analytics)
        ? object.analytics.map((e: any) => Analytics.fromJSON(e))
        : [],
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      totalSize: isSet(object.totalSize) ? Number(object.totalSize) : 0,
    };
  },

  toJSON(message: ListAnalyticsResponse): unknown {
    const obj: any = {};
    if (message.analytics) {
      obj.analytics = message.analytics.map((e) =>
        e ? Analytics.toJSON(e) : undefined
      );
    } else {
      obj.analytics = [];
    }
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.totalSize !== undefined &&
      (obj.totalSize = Math.round(message.totalSize));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListAnalyticsResponse>, I>>(
    object: I
  ): ListAnalyticsResponse {
    const message = createBaseListAnalyticsResponse();
    message.analytics =
      object.analytics?.map((e) => Analytics.fromPartial(e)) || [];
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.totalSize = object.totalSize ?? 0;
    return message;
  },
};

function createBaseCreateAnalyticsRequest(): CreateAnalyticsRequest {
  return { participantId: 0, examId: 0, message: "", createdAt: undefined };
}

export const CreateAnalyticsRequest = {
  encode(
    message: CreateAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participantId !== 0) {
      writer.uint32(8).int32(message.participantId);
    }
    if (message.examId !== 0) {
      writer.uint32(16).int32(message.examId);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    if (message.createdAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.createdAt),
        writer.uint32(34).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateAnalyticsRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participantId = reader.int32();
          break;
        case 2:
          message.examId = reader.int32();
          break;
        case 3:
          message.message = reader.string();
          break;
        case 4:
          message.createdAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAnalyticsRequest {
    return {
      participantId: isSet(object.participantId)
        ? Number(object.participantId)
        : 0,
      examId: isSet(object.examId) ? Number(object.examId) : 0,
      message: isSet(object.message) ? String(object.message) : "",
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
    };
  },

  toJSON(message: CreateAnalyticsRequest): unknown {
    const obj: any = {};
    message.participantId !== undefined &&
      (obj.participantId = Math.round(message.participantId));
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    message.message !== undefined && (obj.message = message.message);
    message.createdAt !== undefined &&
      (obj.createdAt = message.createdAt.toISOString());
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateAnalyticsRequest>, I>>(
    object: I
  ): CreateAnalyticsRequest {
    const message = createBaseCreateAnalyticsRequest();
    message.participantId = object.participantId ?? 0;
    message.examId = object.examId ?? 0;
    message.message = object.message ?? "";
    message.createdAt = object.createdAt ?? undefined;
    return message;
  },
};

function createBaseCreateAnalyticsResponse(): CreateAnalyticsResponse {
  return { analytics: undefined };
}

export const CreateAnalyticsResponse = {
  encode(
    message: CreateAnalyticsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.analytics !== undefined) {
      Analytics.encode(message.analytics, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAnalyticsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateAnalyticsResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics = Analytics.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAnalyticsResponse {
    return {
      analytics: isSet(object.analytics)
        ? Analytics.fromJSON(object.analytics)
        : undefined,
    };
  },

  toJSON(message: CreateAnalyticsResponse): unknown {
    const obj: any = {};
    message.analytics !== undefined &&
      (obj.analytics = message.analytics
        ? Analytics.toJSON(message.analytics)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateAnalyticsResponse>, I>>(
    object: I
  ): CreateAnalyticsResponse {
    const message = createBaseCreateAnalyticsResponse();
    message.analytics =
      object.analytics !== undefined && object.analytics !== null
        ? Analytics.fromPartial(object.analytics)
        : undefined;
    return message;
  },
};

function createBaseUpdateAnalyticsRequest(): UpdateAnalyticsRequest {
  return { analytics: undefined };
}

export const UpdateAnalyticsRequest = {
  encode(
    message: UpdateAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.analytics !== undefined) {
      Analytics.encode(message.analytics, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateAnalyticsRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics = Analytics.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAnalyticsRequest {
    return {
      analytics: isSet(object.analytics)
        ? Analytics.fromJSON(object.analytics)
        : undefined,
    };
  },

  toJSON(message: UpdateAnalyticsRequest): unknown {
    const obj: any = {};
    message.analytics !== undefined &&
      (obj.analytics = message.analytics
        ? Analytics.toJSON(message.analytics)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateAnalyticsRequest>, I>>(
    object: I
  ): UpdateAnalyticsRequest {
    const message = createBaseUpdateAnalyticsRequest();
    message.analytics =
      object.analytics !== undefined && object.analytics !== null
        ? Analytics.fromPartial(object.analytics)
        : undefined;
    return message;
  },
};

function createBaseUpdateAnalyticsResponse(): UpdateAnalyticsResponse {
  return { analytics: undefined };
}

export const UpdateAnalyticsResponse = {
  encode(
    message: UpdateAnalyticsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.analytics !== undefined) {
      Analytics.encode(message.analytics, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAnalyticsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateAnalyticsResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics = Analytics.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAnalyticsResponse {
    return {
      analytics: isSet(object.analytics)
        ? Analytics.fromJSON(object.analytics)
        : undefined,
    };
  },

  toJSON(message: UpdateAnalyticsResponse): unknown {
    const obj: any = {};
    message.analytics !== undefined &&
      (obj.analytics = message.analytics
        ? Analytics.toJSON(message.analytics)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateAnalyticsResponse>, I>>(
    object: I
  ): UpdateAnalyticsResponse {
    const message = createBaseUpdateAnalyticsResponse();
    message.analytics =
      object.analytics !== undefined && object.analytics !== null
        ? Analytics.fromPartial(object.analytics)
        : undefined;
    return message;
  },
};

function createBaseDeleteAnalyticsRequest(): DeleteAnalyticsRequest {
  return { id: 0 };
}

export const DeleteAnalyticsRequest = {
  encode(
    message: DeleteAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): DeleteAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDeleteAnalyticsRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteAnalyticsRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: DeleteAnalyticsRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<DeleteAnalyticsRequest>, I>>(
    object: I
  ): DeleteAnalyticsRequest {
    const message = createBaseDeleteAnalyticsRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseAnalytics(): Analytics {
  return {
    id: 0,
    participantId: 0,
    examId: 0,
    message: "",
    createdAt: undefined,
  };
}

export const Analytics = {
  encode(
    message: Analytics,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.participantId !== 0) {
      writer.uint32(16).int32(message.participantId);
    }
    if (message.examId !== 0) {
      writer.uint32(24).int32(message.examId);
    }
    if (message.message !== "") {
      writer.uint32(34).string(message.message);
    }
    if (message.createdAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.createdAt),
        writer.uint32(42).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Analytics {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAnalytics();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.participantId = reader.int32();
          break;
        case 3:
          message.examId = reader.int32();
          break;
        case 4:
          message.message = reader.string();
          break;
        case 5:
          message.createdAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Analytics {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      participantId: isSet(object.participantId)
        ? Number(object.participantId)
        : 0,
      examId: isSet(object.examId) ? Number(object.examId) : 0,
      message: isSet(object.message) ? String(object.message) : "",
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
    };
  },

  toJSON(message: Analytics): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.participantId !== undefined &&
      (obj.participantId = Math.round(message.participantId));
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    message.message !== undefined && (obj.message = message.message);
    message.createdAt !== undefined &&
      (obj.createdAt = message.createdAt.toISOString());
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Analytics>, I>>(
    object: I
  ): Analytics {
    const message = createBaseAnalytics();
    message.id = object.id ?? 0;
    message.participantId = object.participantId ?? 0;
    message.examId = object.examId ?? 0;
    message.message = object.message ?? "";
    message.createdAt = object.createdAt ?? undefined;
    return message;
  },
};

export const AnalyticsServiceService = {
  getAnalytics: {
    path: "/testycool.v1.AnalyticsService/GetAnalytics",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetAnalyticsRequest) =>
      Buffer.from(GetAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetAnalyticsRequest.decode(value),
    responseSerialize: (value: GetAnalyticsResponse) =>
      Buffer.from(GetAnalyticsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetAnalyticsResponse.decode(value),
  },
  listAnalytics: {
    path: "/testycool.v1.AnalyticsService/ListAnalytics",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListAnalyticsRequest) =>
      Buffer.from(ListAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListAnalyticsRequest.decode(value),
    responseSerialize: (value: ListAnalyticsResponse) =>
      Buffer.from(ListAnalyticsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListAnalyticsResponse.decode(value),
  },
  createAnalytics: {
    path: "/testycool.v1.AnalyticsService/CreateAnalytics",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateAnalyticsRequest) =>
      Buffer.from(CreateAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateAnalyticsRequest.decode(value),
    responseSerialize: (value: CreateAnalyticsResponse) =>
      Buffer.from(CreateAnalyticsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      CreateAnalyticsResponse.decode(value),
  },
  updateAnalytics: {
    path: "/testycool.v1.AnalyticsService/UpdateAnalytics",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateAnalyticsRequest) =>
      Buffer.from(UpdateAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateAnalyticsRequest.decode(value),
    responseSerialize: (value: UpdateAnalyticsResponse) =>
      Buffer.from(UpdateAnalyticsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      UpdateAnalyticsResponse.decode(value),
  },
  deleteAnalytics: {
    path: "/testycool.v1.AnalyticsService/DeleteAnalytics",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteAnalyticsRequest) =>
      Buffer.from(DeleteAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteAnalyticsRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface AnalyticsServiceServer extends UntypedServiceImplementation {
  getAnalytics: handleUnaryCall<GetAnalyticsRequest, GetAnalyticsResponse>;
  listAnalytics: handleUnaryCall<ListAnalyticsRequest, ListAnalyticsResponse>;
  createAnalytics: handleUnaryCall<
    CreateAnalyticsRequest,
    CreateAnalyticsResponse
  >;
  updateAnalytics: handleUnaryCall<
    UpdateAnalyticsRequest,
    UpdateAnalyticsResponse
  >;
  deleteAnalytics: handleUnaryCall<DeleteAnalyticsRequest, Empty>;
}

export interface AnalyticsServiceClient extends Client {
  getAnalytics(
    request: GetAnalyticsRequest,
    callback: (
      error: ServiceError | null,
      response: GetAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  getAnalytics(
    request: GetAnalyticsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: GetAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  getAnalytics(
    request: GetAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: GetAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  listAnalytics(
    request: ListAnalyticsRequest,
    callback: (
      error: ServiceError | null,
      response: ListAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  listAnalytics(
    request: ListAnalyticsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  listAnalytics(
    request: ListAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  createAnalytics(
    request: CreateAnalyticsRequest,
    callback: (
      error: ServiceError | null,
      response: CreateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  createAnalytics(
    request: CreateAnalyticsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  createAnalytics(
    request: CreateAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  updateAnalytics(
    request: UpdateAnalyticsRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  updateAnalytics(
    request: UpdateAnalyticsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  updateAnalytics(
    request: UpdateAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  deleteAnalytics(
    request: DeleteAnalyticsRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAnalytics(
    request: DeleteAnalyticsRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAnalytics(
    request: DeleteAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const AnalyticsServiceClient = makeGenericClientConstructor(
  AnalyticsServiceService,
  "testycool.v1.AnalyticsService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): AnalyticsServiceClient;
  service: typeof AnalyticsServiceService;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = t.seconds * 1_000;
  millis += t.nanos / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Date {
  if (o instanceof Date) {
    return o;
  } else if (typeof o === "string") {
    return new Date(o);
  } else {
    return fromTimestamp(Timestamp.fromJSON(o));
  }
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
