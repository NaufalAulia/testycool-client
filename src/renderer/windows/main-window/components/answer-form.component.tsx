import React from "react";
import {Answer, Choice, LoadingType, Question, QuestionType} from "../../../../shared/models";
import {Participant} from "../../../../shared/models/participant.model";
import AnswerChoiceForm from "./answer-choice-form.component";

export default function AnswerEssayForm(
  props: {
    participant?: Participant,
    currentQuestion?: Question,
    choices?: Choice[],
    currentAnswer?: Answer,
    questionLoading: LoadingType,
    choiceLoading: LoadingType,
    answerLoading: LoadingType
  }): JSX.Element {
  const {participant, currentQuestion, choices, currentAnswer, questionLoading, choiceLoading, answerLoading} = props;

  if (currentQuestion) {
    switch (currentQuestion.type) {
      case QuestionType.MULTIPLE_CHOICE:
        return (
          <AnswerChoiceForm
            participant={participant}
            currentQuestion={currentQuestion}
            choices={choices}
            currentAnswer={currentAnswer}
            questionLoading={questionLoading}
            choiceLoading={choiceLoading}
            answerLoading={answerLoading}
          />
        );
      default:
        return <div/>;
    }
  } else {
    return <div/>;
  }
}