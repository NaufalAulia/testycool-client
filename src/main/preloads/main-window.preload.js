import {contextBridge} from "electron";
import {
  analyticsContextBridge,
  answerContextBridge,
  attemptContextBridge,
  authContextBridge,
  choiceContextBridge,
  examContextBridge,
  participantContextBridge,
  questionContextBridge,
  windowManagerContextBridge
} from "./context-bridges";

contextBridge.exposeInMainWorld(windowManagerContextBridge.apiKey, windowManagerContextBridge.api);
contextBridge.exposeInMainWorld(authContextBridge.apiKey, authContextBridge.api);
contextBridge.exposeInMainWorld(examContextBridge.apiKey, examContextBridge.api);
contextBridge.exposeInMainWorld(participantContextBridge.apiKey, participantContextBridge.api);
contextBridge.exposeInMainWorld(questionContextBridge.apiKey, questionContextBridge.api);
contextBridge.exposeInMainWorld(choiceContextBridge.apiKey, choiceContextBridge.api);
contextBridge.exposeInMainWorld(answerContextBridge.apiKey, answerContextBridge.api);
contextBridge.exposeInMainWorld(attemptContextBridge.apiKey, attemptContextBridge.api);
contextBridge.exposeInMainWorld(analyticsContextBridge.apiKey, analyticsContextBridge.api);
