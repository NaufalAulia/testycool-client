/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import {
  TextFormat,
  textFormatFromJSON,
  textFormatToJSON,
} from "../testycool/text_format";
import { Empty } from "../google/protobuf/empty";

export const protobufPackage = "testycool.v1";

export interface GetQuestionRequest {
  id: number;
}

export interface GetQuestionResponse {
  question: Question | undefined;
}

export interface ListQuestionsRequest {
  size: number;
  page: number;
  filter: ListQuestionsRequest_Filter | undefined;
}

export interface ListQuestionsRequest_Filter {
  examId?: number | undefined;
}

export interface ListQuestionsResponse {
  questions: Question[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateQuestionRequest {
  examId: number;
  type: Question_Type;
  format: TextFormat;
  content: string;
}

export interface CreateQuestionResponse {
  question: Question | undefined;
}

export interface UpdateQuestionRequest {
  question: Question | undefined;
}

export interface UpdateQuestionResponse {
  question: Question | undefined;
}

export interface DeleteQuestionRequest {
  id: number;
}

export interface Question {
  id: number;
  examId: number;
  type: Question_Type;
  format: TextFormat;
  content: string;
}

export enum Question_Type {
  UNKNOWN = 0,
  MULTIPLE_CHOICE = 1,
  ESSAY = 2,
}

export function question_TypeFromJSON(object: any): Question_Type {
  switch (object) {
    case 0:
    case "UNKNOWN":
      return Question_Type.UNKNOWN;
    case 1:
    case "MULTIPLE_CHOICE":
      return Question_Type.MULTIPLE_CHOICE;
    case 2:
    case "ESSAY":
      return Question_Type.ESSAY;
    default:
      throw new globalThis.Error(
        "Unrecognized enum value " + object + " for enum Question_Type"
      );
  }
}

export function question_TypeToJSON(object: Question_Type): string {
  switch (object) {
    case Question_Type.UNKNOWN:
      return "UNKNOWN";
    case Question_Type.MULTIPLE_CHOICE:
      return "MULTIPLE_CHOICE";
    case Question_Type.ESSAY:
      return "ESSAY";
    default:
      return "UNKNOWN";
  }
}

function createBaseGetQuestionRequest(): GetQuestionRequest {
  return { id: 0 };
}

export const GetQuestionRequest = {
  encode(
    message: GetQuestionRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetQuestionRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetQuestionRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetQuestionRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: GetQuestionRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetQuestionRequest>, I>>(
    object: I
  ): GetQuestionRequest {
    const message = createBaseGetQuestionRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseGetQuestionResponse(): GetQuestionResponse {
  return { question: undefined };
}

export const GetQuestionResponse = {
  encode(
    message: GetQuestionResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetQuestionResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetQuestionResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetQuestionResponse {
    return {
      question: isSet(object.question)
        ? Question.fromJSON(object.question)
        : undefined,
    };
  },

  toJSON(message: GetQuestionResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetQuestionResponse>, I>>(
    object: I
  ): GetQuestionResponse {
    const message = createBaseGetQuestionResponse();
    message.question =
      object.question !== undefined && object.question !== null
        ? Question.fromPartial(object.question)
        : undefined;
    return message;
  },
};

function createBaseListQuestionsRequest(): ListQuestionsRequest {
  return { size: 0, page: 0, filter: undefined };
}

export const ListQuestionsRequest = {
  encode(
    message: ListQuestionsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListQuestionsRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListQuestionsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListQuestionsRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListQuestionsRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListQuestionsRequest {
    return {
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      filter: isSet(object.filter)
        ? ListQuestionsRequest_Filter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: ListQuestionsRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListQuestionsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListQuestionsRequest>, I>>(
    object: I
  ): ListQuestionsRequest {
    const message = createBaseListQuestionsRequest();
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? ListQuestionsRequest_Filter.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

function createBaseListQuestionsRequest_Filter(): ListQuestionsRequest_Filter {
  return { examId: undefined };
}

export const ListQuestionsRequest_Filter = {
  encode(
    message: ListQuestionsRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== undefined) {
      writer.uint32(8).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListQuestionsRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListQuestionsRequest_Filter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListQuestionsRequest_Filter {
    return {
      examId: isSet(object.examId) ? Number(object.examId) : undefined,
    };
  },

  toJSON(message: ListQuestionsRequest_Filter): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListQuestionsRequest_Filter>, I>>(
    object: I
  ): ListQuestionsRequest_Filter {
    const message = createBaseListQuestionsRequest_Filter();
    message.examId = object.examId ?? undefined;
    return message;
  },
};

function createBaseListQuestionsResponse(): ListQuestionsResponse {
  return { questions: [], size: 0, page: 0, totalSize: 0 };
}

export const ListQuestionsResponse = {
  encode(
    message: ListQuestionsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.questions) {
      Question.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListQuestionsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListQuestionsResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.questions.push(Question.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListQuestionsResponse {
    return {
      questions: Array.isArray(object?.questions)
        ? object.questions.map((e: any) => Question.fromJSON(e))
        : [],
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      totalSize: isSet(object.totalSize) ? Number(object.totalSize) : 0,
    };
  },

  toJSON(message: ListQuestionsResponse): unknown {
    const obj: any = {};
    if (message.questions) {
      obj.questions = message.questions.map((e) =>
        e ? Question.toJSON(e) : undefined
      );
    } else {
      obj.questions = [];
    }
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.totalSize !== undefined &&
      (obj.totalSize = Math.round(message.totalSize));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListQuestionsResponse>, I>>(
    object: I
  ): ListQuestionsResponse {
    const message = createBaseListQuestionsResponse();
    message.questions =
      object.questions?.map((e) => Question.fromPartial(e)) || [];
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.totalSize = object.totalSize ?? 0;
    return message;
  },
};

function createBaseCreateQuestionRequest(): CreateQuestionRequest {
  return { examId: 0, type: 0, format: 0, content: "" };
}

export const CreateQuestionRequest = {
  encode(
    message: CreateQuestionRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== 0) {
      writer.uint32(8).int32(message.examId);
    }
    if (message.type !== 0) {
      writer.uint32(16).int32(message.type);
    }
    if (message.format !== 0) {
      writer.uint32(24).int32(message.format);
    }
    if (message.content !== "") {
      writer.uint32(34).string(message.content);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateQuestionRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateQuestionRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examId = reader.int32();
          break;
        case 2:
          message.type = reader.int32() as any;
          break;
        case 3:
          message.format = reader.int32() as any;
          break;
        case 4:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateQuestionRequest {
    return {
      examId: isSet(object.examId) ? Number(object.examId) : 0,
      type: isSet(object.type) ? question_TypeFromJSON(object.type) : 0,
      format: isSet(object.format) ? textFormatFromJSON(object.format) : 0,
      content: isSet(object.content) ? String(object.content) : "",
    };
  },

  toJSON(message: CreateQuestionRequest): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    message.type !== undefined &&
      (obj.type = question_TypeToJSON(message.type));
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateQuestionRequest>, I>>(
    object: I
  ): CreateQuestionRequest {
    const message = createBaseCreateQuestionRequest();
    message.examId = object.examId ?? 0;
    message.type = object.type ?? 0;
    message.format = object.format ?? 0;
    message.content = object.content ?? "";
    return message;
  },
};

function createBaseCreateQuestionResponse(): CreateQuestionResponse {
  return { question: undefined };
}

export const CreateQuestionResponse = {
  encode(
    message: CreateQuestionResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateQuestionResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateQuestionResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateQuestionResponse {
    return {
      question: isSet(object.question)
        ? Question.fromJSON(object.question)
        : undefined,
    };
  },

  toJSON(message: CreateQuestionResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateQuestionResponse>, I>>(
    object: I
  ): CreateQuestionResponse {
    const message = createBaseCreateQuestionResponse();
    message.question =
      object.question !== undefined && object.question !== null
        ? Question.fromPartial(object.question)
        : undefined;
    return message;
  },
};

function createBaseUpdateQuestionRequest(): UpdateQuestionRequest {
  return { question: undefined };
}

export const UpdateQuestionRequest = {
  encode(
    message: UpdateQuestionRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateQuestionRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateQuestionRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateQuestionRequest {
    return {
      question: isSet(object.question)
        ? Question.fromJSON(object.question)
        : undefined,
    };
  },

  toJSON(message: UpdateQuestionRequest): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateQuestionRequest>, I>>(
    object: I
  ): UpdateQuestionRequest {
    const message = createBaseUpdateQuestionRequest();
    message.question =
      object.question !== undefined && object.question !== null
        ? Question.fromPartial(object.question)
        : undefined;
    return message;
  },
};

function createBaseUpdateQuestionResponse(): UpdateQuestionResponse {
  return { question: undefined };
}

export const UpdateQuestionResponse = {
  encode(
    message: UpdateQuestionResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateQuestionResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateQuestionResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateQuestionResponse {
    return {
      question: isSet(object.question)
        ? Question.fromJSON(object.question)
        : undefined,
    };
  },

  toJSON(message: UpdateQuestionResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateQuestionResponse>, I>>(
    object: I
  ): UpdateQuestionResponse {
    const message = createBaseUpdateQuestionResponse();
    message.question =
      object.question !== undefined && object.question !== null
        ? Question.fromPartial(object.question)
        : undefined;
    return message;
  },
};

function createBaseDeleteQuestionRequest(): DeleteQuestionRequest {
  return { id: 0 };
}

export const DeleteQuestionRequest = {
  encode(
    message: DeleteQuestionRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): DeleteQuestionRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDeleteQuestionRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteQuestionRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: DeleteQuestionRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<DeleteQuestionRequest>, I>>(
    object: I
  ): DeleteQuestionRequest {
    const message = createBaseDeleteQuestionRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseQuestion(): Question {
  return { id: 0, examId: 0, type: 0, format: 0, content: "" };
}

export const Question = {
  encode(
    message: Question,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.examId !== 0) {
      writer.uint32(16).int32(message.examId);
    }
    if (message.type !== 0) {
      writer.uint32(24).int32(message.type);
    }
    if (message.format !== 0) {
      writer.uint32(32).int32(message.format);
    }
    if (message.content !== "") {
      writer.uint32(42).string(message.content);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Question {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQuestion();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.examId = reader.int32();
          break;
        case 3:
          message.type = reader.int32() as any;
          break;
        case 4:
          message.format = reader.int32() as any;
          break;
        case 5:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Question {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      examId: isSet(object.examId) ? Number(object.examId) : 0,
      type: isSet(object.type) ? question_TypeFromJSON(object.type) : 0,
      format: isSet(object.format) ? textFormatFromJSON(object.format) : 0,
      content: isSet(object.content) ? String(object.content) : "",
    };
  },

  toJSON(message: Question): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    message.type !== undefined &&
      (obj.type = question_TypeToJSON(message.type));
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Question>, I>>(object: I): Question {
    const message = createBaseQuestion();
    message.id = object.id ?? 0;
    message.examId = object.examId ?? 0;
    message.type = object.type ?? 0;
    message.format = object.format ?? 0;
    message.content = object.content ?? "";
    return message;
  },
};

export const QuestionServiceService = {
  getQuestion: {
    path: "/testycool.v1.QuestionService/GetQuestion",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetQuestionRequest) =>
      Buffer.from(GetQuestionRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetQuestionRequest.decode(value),
    responseSerialize: (value: GetQuestionResponse) =>
      Buffer.from(GetQuestionResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetQuestionResponse.decode(value),
  },
  listQuestions: {
    path: "/testycool.v1.QuestionService/ListQuestions",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListQuestionsRequest) =>
      Buffer.from(ListQuestionsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListQuestionsRequest.decode(value),
    responseSerialize: (value: ListQuestionsResponse) =>
      Buffer.from(ListQuestionsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListQuestionsResponse.decode(value),
  },
  createQuestion: {
    path: "/testycool.v1.QuestionService/CreateQuestion",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateQuestionRequest) =>
      Buffer.from(CreateQuestionRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateQuestionRequest.decode(value),
    responseSerialize: (value: CreateQuestionResponse) =>
      Buffer.from(CreateQuestionResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      CreateQuestionResponse.decode(value),
  },
  updateQuestion: {
    path: "/testycool.v1.QuestionService/UpdateQuestion",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateQuestionRequest) =>
      Buffer.from(UpdateQuestionRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateQuestionRequest.decode(value),
    responseSerialize: (value: UpdateQuestionResponse) =>
      Buffer.from(UpdateQuestionResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      UpdateQuestionResponse.decode(value),
  },
  deleteQuestion: {
    path: "/testycool.v1.QuestionService/DeleteQuestion",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteQuestionRequest) =>
      Buffer.from(DeleteQuestionRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteQuestionRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface QuestionServiceServer extends UntypedServiceImplementation {
  getQuestion: handleUnaryCall<GetQuestionRequest, GetQuestionResponse>;
  listQuestions: handleUnaryCall<ListQuestionsRequest, ListQuestionsResponse>;
  createQuestion: handleUnaryCall<
    CreateQuestionRequest,
    CreateQuestionResponse
  >;
  updateQuestion: handleUnaryCall<
    UpdateQuestionRequest,
    UpdateQuestionResponse
  >;
  deleteQuestion: handleUnaryCall<DeleteQuestionRequest, Empty>;
}

export interface QuestionServiceClient extends Client {
  getQuestion(
    request: GetQuestionRequest,
    callback: (
      error: ServiceError | null,
      response: GetQuestionResponse
    ) => void
  ): ClientUnaryCall;
  getQuestion(
    request: GetQuestionRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: GetQuestionResponse
    ) => void
  ): ClientUnaryCall;
  getQuestion(
    request: GetQuestionRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: GetQuestionResponse
    ) => void
  ): ClientUnaryCall;
  listQuestions(
    request: ListQuestionsRequest,
    callback: (
      error: ServiceError | null,
      response: ListQuestionsResponse
    ) => void
  ): ClientUnaryCall;
  listQuestions(
    request: ListQuestionsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListQuestionsResponse
    ) => void
  ): ClientUnaryCall;
  listQuestions(
    request: ListQuestionsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListQuestionsResponse
    ) => void
  ): ClientUnaryCall;
  createQuestion(
    request: CreateQuestionRequest,
    callback: (
      error: ServiceError | null,
      response: CreateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  createQuestion(
    request: CreateQuestionRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  createQuestion(
    request: CreateQuestionRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  updateQuestion(
    request: UpdateQuestionRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  updateQuestion(
    request: UpdateQuestionRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  updateQuestion(
    request: UpdateQuestionRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  deleteQuestion(
    request: DeleteQuestionRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteQuestion(
    request: DeleteQuestionRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteQuestion(
    request: DeleteQuestionRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const QuestionServiceClient = makeGenericClientConstructor(
  QuestionServiceService,
  "testycool.v1.QuestionService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): QuestionServiceClient;
  service: typeof QuestionServiceService;
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
