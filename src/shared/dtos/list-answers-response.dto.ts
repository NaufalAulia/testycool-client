import {Answer} from "../models";

export class ListAnswersResponseDto {
  public answers: Answer[];
  public size: number;
  public page: number;
  public totalSize: number;
}