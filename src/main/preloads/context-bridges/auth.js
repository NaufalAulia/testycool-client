import {ipcRenderer} from "electron";

export const authChannels = ['get-access-token', 'auth-error'];

export const authContextBridge = {
  apiKey: 'auth',
  api: {
    getAccessToken: (authRequestDto) => {
      ipcRenderer.send('get-access-token', {...authRequestDto});
    },
    once: (channel, func) => {
      if (!authChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid auth channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.once(channel, (event, ...args) => func(...args));
    },
    on: (channel, func) => {
      if (!authChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid auth channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    },
  }
}