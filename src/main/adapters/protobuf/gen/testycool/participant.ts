/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import { Empty } from "../google/protobuf/empty";

export const protobufPackage = "testycool.v1";

export interface GetParticipantRequest {
  id: number;
}

export interface GetParticipantResponse {
  participant: Participant | undefined;
}

export interface ListParticipantsRequest {
  size: number;
  page: number;
  filter: ListParticipantsRequest_Filter | undefined;
}

export interface ListParticipantsRequest_Filter {
  examId?: number | undefined;
}

export interface ListParticipantsResponse {
  participants: Participant[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateParticipantRequest {
  examId: number;
  code: string;
  name: string;
}

export interface CreateParticipantResponse {
  participant: Participant | undefined;
}

export interface UpdateParticipantRequest {
  participant: Participant | undefined;
}

export interface UpdateParticipantResponse {
  participant: Participant | undefined;
}

export interface DeleteParticipantRequest {
  id: number;
}

export interface Participant {
  id: number;
  examId: number;
  code: string;
  name: string;
}

function createBaseGetParticipantRequest(): GetParticipantRequest {
  return { id: 0 };
}

export const GetParticipantRequest = {
  encode(
    message: GetParticipantRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetParticipantRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetParticipantRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetParticipantRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: GetParticipantRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetParticipantRequest>, I>>(
    object: I
  ): GetParticipantRequest {
    const message = createBaseGetParticipantRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseGetParticipantResponse(): GetParticipantResponse {
  return { participant: undefined };
}

export const GetParticipantResponse = {
  encode(
    message: GetParticipantResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participant !== undefined) {
      Participant.encode(
        message.participant,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetParticipantResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetParticipantResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participant = Participant.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetParticipantResponse {
    return {
      participant: isSet(object.participant)
        ? Participant.fromJSON(object.participant)
        : undefined,
    };
  },

  toJSON(message: GetParticipantResponse): unknown {
    const obj: any = {};
    message.participant !== undefined &&
      (obj.participant = message.participant
        ? Participant.toJSON(message.participant)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetParticipantResponse>, I>>(
    object: I
  ): GetParticipantResponse {
    const message = createBaseGetParticipantResponse();
    message.participant =
      object.participant !== undefined && object.participant !== null
        ? Participant.fromPartial(object.participant)
        : undefined;
    return message;
  },
};

function createBaseListParticipantsRequest(): ListParticipantsRequest {
  return { size: 0, page: 0, filter: undefined };
}

export const ListParticipantsRequest = {
  encode(
    message: ListParticipantsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListParticipantsRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListParticipantsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListParticipantsRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListParticipantsRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListParticipantsRequest {
    return {
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      filter: isSet(object.filter)
        ? ListParticipantsRequest_Filter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: ListParticipantsRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListParticipantsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListParticipantsRequest>, I>>(
    object: I
  ): ListParticipantsRequest {
    const message = createBaseListParticipantsRequest();
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? ListParticipantsRequest_Filter.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

function createBaseListParticipantsRequest_Filter(): ListParticipantsRequest_Filter {
  return { examId: undefined };
}

export const ListParticipantsRequest_Filter = {
  encode(
    message: ListParticipantsRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== undefined) {
      writer.uint32(8).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListParticipantsRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListParticipantsRequest_Filter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListParticipantsRequest_Filter {
    return {
      examId: isSet(object.examId) ? Number(object.examId) : undefined,
    };
  },

  toJSON(message: ListParticipantsRequest_Filter): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListParticipantsRequest_Filter>, I>>(
    object: I
  ): ListParticipantsRequest_Filter {
    const message = createBaseListParticipantsRequest_Filter();
    message.examId = object.examId ?? undefined;
    return message;
  },
};

function createBaseListParticipantsResponse(): ListParticipantsResponse {
  return { participants: [], size: 0, page: 0, totalSize: 0 };
}

export const ListParticipantsResponse = {
  encode(
    message: ListParticipantsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.participants) {
      Participant.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListParticipantsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListParticipantsResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participants.push(
            Participant.decode(reader, reader.uint32())
          );
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListParticipantsResponse {
    return {
      participants: Array.isArray(object?.participants)
        ? object.participants.map((e: any) => Participant.fromJSON(e))
        : [],
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      totalSize: isSet(object.totalSize) ? Number(object.totalSize) : 0,
    };
  },

  toJSON(message: ListParticipantsResponse): unknown {
    const obj: any = {};
    if (message.participants) {
      obj.participants = message.participants.map((e) =>
        e ? Participant.toJSON(e) : undefined
      );
    } else {
      obj.participants = [];
    }
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.totalSize !== undefined &&
      (obj.totalSize = Math.round(message.totalSize));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListParticipantsResponse>, I>>(
    object: I
  ): ListParticipantsResponse {
    const message = createBaseListParticipantsResponse();
    message.participants =
      object.participants?.map((e) => Participant.fromPartial(e)) || [];
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.totalSize = object.totalSize ?? 0;
    return message;
  },
};

function createBaseCreateParticipantRequest(): CreateParticipantRequest {
  return { examId: 0, code: "", name: "" };
}

export const CreateParticipantRequest = {
  encode(
    message: CreateParticipantRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== 0) {
      writer.uint32(8).int32(message.examId);
    }
    if (message.code !== "") {
      writer.uint32(18).string(message.code);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateParticipantRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateParticipantRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examId = reader.int32();
          break;
        case 2:
          message.code = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateParticipantRequest {
    return {
      examId: isSet(object.examId) ? Number(object.examId) : 0,
      code: isSet(object.code) ? String(object.code) : "",
      name: isSet(object.name) ? String(object.name) : "",
    };
  },

  toJSON(message: CreateParticipantRequest): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    message.code !== undefined && (obj.code = message.code);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateParticipantRequest>, I>>(
    object: I
  ): CreateParticipantRequest {
    const message = createBaseCreateParticipantRequest();
    message.examId = object.examId ?? 0;
    message.code = object.code ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseCreateParticipantResponse(): CreateParticipantResponse {
  return { participant: undefined };
}

export const CreateParticipantResponse = {
  encode(
    message: CreateParticipantResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participant !== undefined) {
      Participant.encode(
        message.participant,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateParticipantResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateParticipantResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participant = Participant.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateParticipantResponse {
    return {
      participant: isSet(object.participant)
        ? Participant.fromJSON(object.participant)
        : undefined,
    };
  },

  toJSON(message: CreateParticipantResponse): unknown {
    const obj: any = {};
    message.participant !== undefined &&
      (obj.participant = message.participant
        ? Participant.toJSON(message.participant)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateParticipantResponse>, I>>(
    object: I
  ): CreateParticipantResponse {
    const message = createBaseCreateParticipantResponse();
    message.participant =
      object.participant !== undefined && object.participant !== null
        ? Participant.fromPartial(object.participant)
        : undefined;
    return message;
  },
};

function createBaseUpdateParticipantRequest(): UpdateParticipantRequest {
  return { participant: undefined };
}

export const UpdateParticipantRequest = {
  encode(
    message: UpdateParticipantRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participant !== undefined) {
      Participant.encode(
        message.participant,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateParticipantRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateParticipantRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participant = Participant.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateParticipantRequest {
    return {
      participant: isSet(object.participant)
        ? Participant.fromJSON(object.participant)
        : undefined,
    };
  },

  toJSON(message: UpdateParticipantRequest): unknown {
    const obj: any = {};
    message.participant !== undefined &&
      (obj.participant = message.participant
        ? Participant.toJSON(message.participant)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateParticipantRequest>, I>>(
    object: I
  ): UpdateParticipantRequest {
    const message = createBaseUpdateParticipantRequest();
    message.participant =
      object.participant !== undefined && object.participant !== null
        ? Participant.fromPartial(object.participant)
        : undefined;
    return message;
  },
};

function createBaseUpdateParticipantResponse(): UpdateParticipantResponse {
  return { participant: undefined };
}

export const UpdateParticipantResponse = {
  encode(
    message: UpdateParticipantResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participant !== undefined) {
      Participant.encode(
        message.participant,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateParticipantResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateParticipantResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participant = Participant.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateParticipantResponse {
    return {
      participant: isSet(object.participant)
        ? Participant.fromJSON(object.participant)
        : undefined,
    };
  },

  toJSON(message: UpdateParticipantResponse): unknown {
    const obj: any = {};
    message.participant !== undefined &&
      (obj.participant = message.participant
        ? Participant.toJSON(message.participant)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateParticipantResponse>, I>>(
    object: I
  ): UpdateParticipantResponse {
    const message = createBaseUpdateParticipantResponse();
    message.participant =
      object.participant !== undefined && object.participant !== null
        ? Participant.fromPartial(object.participant)
        : undefined;
    return message;
  },
};

function createBaseDeleteParticipantRequest(): DeleteParticipantRequest {
  return { id: 0 };
}

export const DeleteParticipantRequest = {
  encode(
    message: DeleteParticipantRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): DeleteParticipantRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDeleteParticipantRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteParticipantRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: DeleteParticipantRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<DeleteParticipantRequest>, I>>(
    object: I
  ): DeleteParticipantRequest {
    const message = createBaseDeleteParticipantRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseParticipant(): Participant {
  return { id: 0, examId: 0, code: "", name: "" };
}

export const Participant = {
  encode(
    message: Participant,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.examId !== 0) {
      writer.uint32(16).int32(message.examId);
    }
    if (message.code !== "") {
      writer.uint32(26).string(message.code);
    }
    if (message.name !== "") {
      writer.uint32(34).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Participant {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseParticipant();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.examId = reader.int32();
          break;
        case 3:
          message.code = reader.string();
          break;
        case 4:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Participant {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      examId: isSet(object.examId) ? Number(object.examId) : 0,
      code: isSet(object.code) ? String(object.code) : "",
      name: isSet(object.name) ? String(object.name) : "",
    };
  },

  toJSON(message: Participant): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    message.code !== undefined && (obj.code = message.code);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Participant>, I>>(
    object: I
  ): Participant {
    const message = createBaseParticipant();
    message.id = object.id ?? 0;
    message.examId = object.examId ?? 0;
    message.code = object.code ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

export const ParticipantServiceService = {
  getParticipant: {
    path: "/testycool.v1.ParticipantService/GetParticipant",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetParticipantRequest) =>
      Buffer.from(GetParticipantRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetParticipantRequest.decode(value),
    responseSerialize: (value: GetParticipantResponse) =>
      Buffer.from(GetParticipantResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      GetParticipantResponse.decode(value),
  },
  listParticipants: {
    path: "/testycool.v1.ParticipantService/ListParticipants",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListParticipantsRequest) =>
      Buffer.from(ListParticipantsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      ListParticipantsRequest.decode(value),
    responseSerialize: (value: ListParticipantsResponse) =>
      Buffer.from(ListParticipantsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      ListParticipantsResponse.decode(value),
  },
  createParticipant: {
    path: "/testycool.v1.ParticipantService/CreateParticipant",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateParticipantRequest) =>
      Buffer.from(CreateParticipantRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      CreateParticipantRequest.decode(value),
    responseSerialize: (value: CreateParticipantResponse) =>
      Buffer.from(CreateParticipantResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      CreateParticipantResponse.decode(value),
  },
  updateParticipant: {
    path: "/testycool.v1.ParticipantService/UpdateParticipant",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateParticipantRequest) =>
      Buffer.from(UpdateParticipantRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      UpdateParticipantRequest.decode(value),
    responseSerialize: (value: UpdateParticipantResponse) =>
      Buffer.from(UpdateParticipantResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      UpdateParticipantResponse.decode(value),
  },
  deleteParticipant: {
    path: "/testycool.v1.ParticipantService/DeleteParticipant",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteParticipantRequest) =>
      Buffer.from(DeleteParticipantRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      DeleteParticipantRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface ParticipantServiceServer extends UntypedServiceImplementation {
  getParticipant: handleUnaryCall<
    GetParticipantRequest,
    GetParticipantResponse
  >;
  listParticipants: handleUnaryCall<
    ListParticipantsRequest,
    ListParticipantsResponse
  >;
  createParticipant: handleUnaryCall<
    CreateParticipantRequest,
    CreateParticipantResponse
  >;
  updateParticipant: handleUnaryCall<
    UpdateParticipantRequest,
    UpdateParticipantResponse
  >;
  deleteParticipant: handleUnaryCall<DeleteParticipantRequest, Empty>;
}

export interface ParticipantServiceClient extends Client {
  getParticipant(
    request: GetParticipantRequest,
    callback: (
      error: ServiceError | null,
      response: GetParticipantResponse
    ) => void
  ): ClientUnaryCall;
  getParticipant(
    request: GetParticipantRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: GetParticipantResponse
    ) => void
  ): ClientUnaryCall;
  getParticipant(
    request: GetParticipantRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: GetParticipantResponse
    ) => void
  ): ClientUnaryCall;
  listParticipants(
    request: ListParticipantsRequest,
    callback: (
      error: ServiceError | null,
      response: ListParticipantsResponse
    ) => void
  ): ClientUnaryCall;
  listParticipants(
    request: ListParticipantsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListParticipantsResponse
    ) => void
  ): ClientUnaryCall;
  listParticipants(
    request: ListParticipantsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListParticipantsResponse
    ) => void
  ): ClientUnaryCall;
  createParticipant(
    request: CreateParticipantRequest,
    callback: (
      error: ServiceError | null,
      response: CreateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  createParticipant(
    request: CreateParticipantRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  createParticipant(
    request: CreateParticipantRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  updateParticipant(
    request: UpdateParticipantRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  updateParticipant(
    request: UpdateParticipantRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  updateParticipant(
    request: UpdateParticipantRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateParticipantResponse
    ) => void
  ): ClientUnaryCall;
  deleteParticipant(
    request: DeleteParticipantRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteParticipant(
    request: DeleteParticipantRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteParticipant(
    request: DeleteParticipantRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const ParticipantServiceClient = makeGenericClientConstructor(
  ParticipantServiceService,
  "testycool.v1.ParticipantService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): ParticipantServiceClient;
  service: typeof ParticipantServiceService;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
