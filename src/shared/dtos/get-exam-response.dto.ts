import {Exam} from "../models";

export class GetExamResponseDto {
  exam: Exam
}