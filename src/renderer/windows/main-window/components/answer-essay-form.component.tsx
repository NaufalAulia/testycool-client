import React, {useEffect, useState} from "react";
import {Answer, Choice, LoadingType, Question, TextFormat} from "../../../../shared/models";
import ReactQuill from "react-quill";
import {answerSender} from "../../../shared/ipcs";
import {Participant} from "../../../../shared/models/participant.model";

export default function AnswerEssayForm(
  props: {
    participant?: Participant,
    currentQuestion?: Question,
    choices?: Choice[],
    currentAnswer?: Answer,
    questionLoading: LoadingType,
    answerLoading: LoadingType
  }): JSX.Element {
  const {participant, currentQuestion, choices, currentAnswer, questionLoading, answerLoading} = props;

  const [answerForm, setAnswerForm] = useState<{ content?: string }>({
      content: currentAnswer?.essay?.content || " ",
    }
  );

  const handleChange = (content?: string) => {
    if (content) {
      setAnswerForm({...answerForm, content: content});
    }
    console.debug(answerForm);
  }

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (
        questionLoading === LoadingType.IDLE &&
        answerLoading === LoadingType.IDLE &&
        currentQuestion &&
        participant &&
        (answerForm.content && answerForm.content !== " ")
      ) {
        if (!currentAnswer) {
          answerSender.createAnswer({
            questionId: currentQuestion.id,
            participantId: participant.id,
            choiceId: undefined,
            essay: {
              content: answerForm.content,
              format: TextFormat.TEXT_FORMAT_HTML,
            },
          });
          console.log("create answer");
        } else {
          if (answerForm.content !== "" && currentAnswer.essay.content !== answerForm.content) {
            answerSender.updateAnswer({
              answer: {
                ...currentAnswer,
                essay: {
                  ...currentAnswer.essay,
                  content: answerForm.content
                }
              }
            });
          }
          console.log("update answer");
        }
      }
    }, 3000);
    return () => clearTimeout(timeout);
  }, [answerForm, participant, currentQuestion, choices, currentAnswer, questionLoading, answerLoading]);

  return (
    <form>
      <div className="mb-5 flex">
        <ReactQuill
          theme="snow"
          placeholder="type your answer here..."
          value={answerForm.content}
          onChange={(content) =>
            handleChange(content)}
        />
      </div>
    </form>
  );
}