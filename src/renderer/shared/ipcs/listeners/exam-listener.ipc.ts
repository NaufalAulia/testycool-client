import {ErrorResponseDto, GetExamResponseDto} from "../../../../shared/dtos";
import {examAction} from "../../../../shared/redux/actions";
import store from "../../../store";

const examListener = () => {
  window.exam.on('get-exam', (args: GetExamResponseDto) => {
    store.dispatch(examAction.getExam.fulfilled(args));
  });
  window.exam.on('exam-error', (args: ErrorResponseDto) => {
    store.dispatch(examAction.setError(args));
  });
}

export default examListener;
