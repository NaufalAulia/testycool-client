import {
  CreateAnswerResponseDto,
  ErrorResponseDto,
  GetAnswerResponseDto,
  ListAnswersResponseDto,
  UpdateAnswerResponseDto
} from "../../../../shared/dtos";
import {answerAction} from "../../../../shared/redux/actions";
import store from "../../../store";

const answerListener = () => {
  window.answer.on('list-answers', (args: ListAnswersResponseDto) => {
    store.dispatch(answerAction.listAnswers.fulfilled(args));
  });
  window.answer.on('get-answer', (args: GetAnswerResponseDto) => {
    store.dispatch(answerAction.getAnswer.fulfilled(args));
  });
  window.answer.on('create-answer', (args: CreateAnswerResponseDto) => {
    store.dispatch(answerAction.createAnswer.fulfilled(args));
  });
  window.answer.on('update-answer', (args: UpdateAnswerResponseDto) => {
    store.dispatch(answerAction.updateAnswer.fulfilled(args));
  });
  window.answer.on('answer-error', (args: ErrorResponseDto) => {
    store.dispatch(answerAction.setError(args));
  });
}

export default answerListener;
