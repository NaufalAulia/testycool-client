import {
  answerAction,
  attemptAction,
  authAction,
  examAction,
  participantAction,
  questionAction
} from "../../../../shared/redux/actions";
import store from "../../../store";

const windowManagerSender = {
  switchToMain: () => {
    window.windowManager.switchToMain();
  },
  switchToLogin: () => {
    store.dispatch(authAction.resetState());
    store.dispatch(examAction.resetState());
    store.dispatch(participantAction.resetState());
    store.dispatch(attemptAction.resetState());
    store.dispatch(questionAction.resetState());
    store.dispatch(answerAction.resetState());

    window.windowManager.switchToLogin();
  },
}

export default windowManagerSender;
