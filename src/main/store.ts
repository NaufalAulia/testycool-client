import {configureStore} from "@reduxjs/toolkit";
import {AttemptReducer, AuthReducer, ExamReducer, ParticipantReducer} from "../shared/redux/reducers";

const store = configureStore(
  {
    reducer: {
      auth: AuthReducer,
      exam: ExamReducer,
      participant: ParticipantReducer,
      attempt: AttemptReducer,
    },
  }
)


export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
