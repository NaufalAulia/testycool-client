import {choiceAction} from "../../../../shared/redux/actions";
import {GetChoiceRequestDto, ListChoicesRequestDto} from "../../../../shared/dtos";
import store from "../../../store";

const choiceSender = {
  listChoices: (dto: ListChoicesRequestDto) => {
    store.dispatch(choiceAction.listChoices.pending());
    window.choice.listChoices(dto);
  },
  getChoice: (dto: GetChoiceRequestDto) => {
    store.dispatch(choiceAction.getChoice.pending());
    window.choice.getChoice(dto);
  }
}

export default choiceSender;