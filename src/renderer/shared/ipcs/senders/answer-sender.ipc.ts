import {answerAction} from "../../../../shared/redux/actions";
import {
  CreateAnswerRequestDto,
  GetAnswerRequestDto,
  ListAnswersRequestDto,
  UpdateAnswerRequestDto
} from "../../../../shared/dtos";
import store from "../../../store";

const answerSender = {
  listAnswers: (dto: ListAnswersRequestDto) => {
    store.dispatch(answerAction.listAnswers.pending());
    window.answer.listAnswers(dto);
  },
  getAnswer: (dto: GetAnswerRequestDto) => {
    store.dispatch(answerAction.getAnswer.pending());
    window.answer.getAnswer(dto);
  },
  createAnswer: (dto: CreateAnswerRequestDto) => {
    store.dispatch(answerAction.createAnswer.pending());
    window.answer.createAnswer(dto);
  },
  updateAnswer: (dto: UpdateAnswerRequestDto) => {
    store.dispatch(answerAction.updateAnswer.pending());
    window.answer.updateAnswer(dto);
  },
}

export default answerSender;