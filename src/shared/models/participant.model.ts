export interface Participant {
  id: number;
  examId: number;
  code: string;
  name: string;
}
