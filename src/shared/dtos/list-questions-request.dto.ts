export class ListQuestionsRequestDto {
  public filter: {
    examId: number;
  }
  public page: number;
  public size: number;
}