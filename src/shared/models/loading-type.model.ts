export enum LoadingType {
  IDLE = "Idle",
  LOADING = "Loading",
  CREATE = "Create",
  GETLIST = "GetList",
  GETENTRY = "GetEntry",
  UPDATE = "Update",
  DELETE = "Delete",
}
