export class ListChoicesRequestDto {
  public filter: {
    questionId: number;
  }
  public page: number;
  public size: number;
}