import React from "react";
import ReactDOM from "react-dom";
import App from './app';
import {Provider} from "react-redux";
import store from "../../store";
import {
  analyticsListener,
  answerListener,
  attemptListener,
  authListener,
  choiceListener,
  examListener,
  participantListener,
  questionListener
} from "../../shared/ipcs";

declare global {
  interface Window {
    windowManager: any;
    auth: any;
    exam: any;
    participant: any;
    question: any;
    choice: any;
    answer: any;
    attempt: any;
    analytics: any;
  }
}

authListener();
examListener();
participantListener();
questionListener();
choiceListener();
answerListener();
attemptListener();
analyticsListener();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App/>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
