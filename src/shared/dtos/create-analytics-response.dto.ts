import {Analytics} from "../models";

export interface CreateAnalyticsResponseDto {
  analytics: Analytics | undefined;
}
