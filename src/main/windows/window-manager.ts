import {BrowserWindow, Display, nativeImage, screen} from "electron";
import {WindowRecipe} from "./recipes/interfaces/window-recipe";
import * as path from "path";
import screenFillerWindow from "./recipes/screen-filler.window";
import ScreenFillerWindow from "./recipes/screen-filler.window";

interface BrowserWindows {
  loginWindow: BrowserWindow | undefined;
  mainWindow: BrowserWindow | undefined;
}


export const browserWindows: BrowserWindows = {
  loginWindow: undefined,
  mainWindow: undefined,
}

const iconFile = path.join(process.resourcesPath, 'assets', 'images', 'icon', 'icon.png');
const getIcon = () => {
  let icon = nativeImage.createFromPath(iconFile);

  if (!icon) {
    icon = nativeImage.createEmpty();
  }

  return icon;
}

export const createNewWindow = (recipe: WindowRecipe): BrowserWindow => {

  const newWindow = new BrowserWindow({
      ...recipe.options,
      icon: getIcon(),
    },
  );

  if (recipe.extraActions) {
    recipe.extraActions(newWindow);
  }

  newWindow.loadURL(recipe.entry);

  return newWindow;
}

const createExtraWindow = (display: Display, mainWindow: BrowserWindow): BrowserWindow => {
  return createNewWindow({
      ...screenFillerWindow, options: {
        ...ScreenFillerWindow.options,
        x: display.bounds.x,
        y: display.bounds.y,
        height: display.size.height,
        width: display.size.width,
        parent: mainWindow,
      }
    }
  );
}

export const setupExtraWindows = (mainWindow?: BrowserWindow): void => {
  const currentMainWindow = mainWindow ? mainWindow : browserWindows.mainWindow;
  if (!currentMainWindow) {
    return;
  }

  currentMainWindow.getChildWindows().forEach((cw) => cw.close());

  const mainDisplay = screen.getPrimaryDisplay();
  const mainWindowPos = currentMainWindow.getPosition();
  if (mainDisplay.bounds.x !== mainWindowPos[0] && mainDisplay.bounds.y === mainWindowPos[1]) {
    currentMainWindow.setPosition(mainDisplay.bounds.x, mainDisplay.bounds.y, false);
    currentMainWindow.setSize(mainDisplay.size.width, mainDisplay.size.height, false);
  }

  const displays = screen.getAllDisplays();
  displays.forEach((display) => {
    if (display.bounds.x !== 0 || display.bounds.y !== 0) {
      createExtraWindow(display, currentMainWindow);
    }
  });

  currentMainWindow.moveTop();
  currentMainWindow.restore();
  currentMainWindow.focus();
}