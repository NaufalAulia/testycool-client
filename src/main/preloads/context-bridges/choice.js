import {ipcRenderer} from "electron";

export const choiceChannels = ['list-choices', 'get-choice', 'choice-error'];

export const choiceContextBridge = {
  apiKey: 'choice',
  api: {
    listChoices: (listChoicesRequestDto) => {
      ipcRenderer.send('list-choices', {...listChoicesRequestDto});
    },
    getChoice: (getChoiceRequestDto) => {
      ipcRenderer.send('get-choice', {...getChoiceRequestDto});
    },
    once: (channel, func) => {
      if (!choiceChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid choice channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.once(channel, (event, ...args) => func(...args));
    },
    on: (channel, func) => {
      if (!choiceChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid choice channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    },
  }
}