import {createReducer} from '@reduxjs/toolkit';
import {Analytics, LoadingType} from "../../models";
import {analyticsAction} from "../actions";
import {ErrorResponseDto} from "../../dtos";

export interface AnalyticsState {
  newAnalytics?: Analytics;
  analytics: Analytics[];
  loading: LoadingType;
  error?: ErrorResponseDto;
}

const initialState: AnalyticsState = {
  newAnalytics: undefined,
  analytics: [],
  loading: LoadingType.IDLE,
  error: undefined
};

const AnalyticsReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(analyticsAction.createAnalytics.pending, (state) => {
      return {
        ...state,
        loading: LoadingType.CREATE,
      }
    })
    .addCase(analyticsAction.createAnalytics.fulfilled, (state, action) => {
      state.newAnalytics = action.payload.analytics;
      state.analytics.push(action.payload.analytics);
      state.loading = LoadingType.IDLE;
      state.error = undefined;
    })
    .addCase(analyticsAction.createAnalytics.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(analyticsAction.setError, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      }
    })
    .addCase(analyticsAction.resetState, () => {
      return {
        ...initialState
      }
    })
);

export default AnalyticsReducer;
