export interface CreateAnalyticsRequestDto {
  participantId: number;
  examId: number;
  message: string;
  createdAt: Date;
}
