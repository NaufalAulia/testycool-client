/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import { Timestamp } from "../google/protobuf/timestamp";
import { Empty } from "../google/protobuf/empty";

export const protobufPackage = "testycool.v1";

export interface GetAttemptRequest {
  id: number | undefined;
  participantId: number | undefined;
}

export interface GetAttemptResponse {
  attempt: Attempt | undefined;
}

export interface ListAttemptsRequest {
  size: number;
  page: number;
  filter: ListAttemptsRequest_Filter | undefined;
}

export interface ListAttemptsRequest_Filter {
  examId?: number | undefined;
}

export interface ListAttemptsResponse {
  attempts: Attempt[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateAttemptRequest {
  participantId: number;
  examId: number;
  createdAt: Date | undefined;
}

export interface CreateAttemptResponse {
  attempt: Attempt | undefined;
}

export interface UpdateAttemptRequest {
  attempt: Attempt | undefined;
}

export interface UpdateAttemptResponse {
  attempt: Attempt | undefined;
}

export interface DeleteAttemptRequest {
  id: number;
}

export interface Attempt {
  id: number;
  participantId: number;
  examId: number;
  finished: boolean;
  corrects: number;
  wrongs: number;
  unanswered: number;
  createdAt: Date | undefined;
  updatedAt: Date | undefined;
}

function createBaseGetAttemptRequest(): GetAttemptRequest {
  return { id: undefined, participantId: undefined };
}

export const GetAttemptRequest = {
  encode(
    message: GetAttemptRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== undefined) {
      writer.uint32(8).int32(message.id);
    }
    if (message.participantId !== undefined) {
      writer.uint32(16).int32(message.participantId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAttemptRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAttemptRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.participantId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAttemptRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : undefined,
      participantId: isSet(object.participantId)
        ? Number(object.participantId)
        : undefined,
    };
  },

  toJSON(message: GetAttemptRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.participantId !== undefined &&
      (obj.participantId = Math.round(message.participantId));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAttemptRequest>, I>>(
    object: I
  ): GetAttemptRequest {
    const message = createBaseGetAttemptRequest();
    message.id = object.id ?? undefined;
    message.participantId = object.participantId ?? undefined;
    return message;
  },
};

function createBaseGetAttemptResponse(): GetAttemptResponse {
  return { attempt: undefined };
}

export const GetAttemptResponse = {
  encode(
    message: GetAttemptResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.attempt !== undefined) {
      Attempt.encode(message.attempt, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAttemptResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAttemptResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempt = Attempt.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAttemptResponse {
    return {
      attempt: isSet(object.attempt)
        ? Attempt.fromJSON(object.attempt)
        : undefined,
    };
  },

  toJSON(message: GetAttemptResponse): unknown {
    const obj: any = {};
    message.attempt !== undefined &&
      (obj.attempt = message.attempt
        ? Attempt.toJSON(message.attempt)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAttemptResponse>, I>>(
    object: I
  ): GetAttemptResponse {
    const message = createBaseGetAttemptResponse();
    message.attempt =
      object.attempt !== undefined && object.attempt !== null
        ? Attempt.fromPartial(object.attempt)
        : undefined;
    return message;
  },
};

function createBaseListAttemptsRequest(): ListAttemptsRequest {
  return { size: 0, page: 0, filter: undefined };
}

export const ListAttemptsRequest = {
  encode(
    message: ListAttemptsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListAttemptsRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListAttemptsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListAttemptsRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListAttemptsRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAttemptsRequest {
    return {
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      filter: isSet(object.filter)
        ? ListAttemptsRequest_Filter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: ListAttemptsRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListAttemptsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListAttemptsRequest>, I>>(
    object: I
  ): ListAttemptsRequest {
    const message = createBaseListAttemptsRequest();
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? ListAttemptsRequest_Filter.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

function createBaseListAttemptsRequest_Filter(): ListAttemptsRequest_Filter {
  return { examId: undefined };
}

export const ListAttemptsRequest_Filter = {
  encode(
    message: ListAttemptsRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== undefined) {
      writer.uint32(16).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAttemptsRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListAttemptsRequest_Filter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAttemptsRequest_Filter {
    return {
      examId: isSet(object.examId) ? Number(object.examId) : undefined,
    };
  },

  toJSON(message: ListAttemptsRequest_Filter): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListAttemptsRequest_Filter>, I>>(
    object: I
  ): ListAttemptsRequest_Filter {
    const message = createBaseListAttemptsRequest_Filter();
    message.examId = object.examId ?? undefined;
    return message;
  },
};

function createBaseListAttemptsResponse(): ListAttemptsResponse {
  return { attempts: [], size: 0, page: 0, totalSize: 0 };
}

export const ListAttemptsResponse = {
  encode(
    message: ListAttemptsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.attempts) {
      Attempt.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAttemptsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListAttemptsResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempts.push(Attempt.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAttemptsResponse {
    return {
      attempts: Array.isArray(object?.attempts)
        ? object.attempts.map((e: any) => Attempt.fromJSON(e))
        : [],
      size: isSet(object.size) ? Number(object.size) : 0,
      page: isSet(object.page) ? Number(object.page) : 0,
      totalSize: isSet(object.totalSize) ? Number(object.totalSize) : 0,
    };
  },

  toJSON(message: ListAttemptsResponse): unknown {
    const obj: any = {};
    if (message.attempts) {
      obj.attempts = message.attempts.map((e) =>
        e ? Attempt.toJSON(e) : undefined
      );
    } else {
      obj.attempts = [];
    }
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.totalSize !== undefined &&
      (obj.totalSize = Math.round(message.totalSize));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListAttemptsResponse>, I>>(
    object: I
  ): ListAttemptsResponse {
    const message = createBaseListAttemptsResponse();
    message.attempts =
      object.attempts?.map((e) => Attempt.fromPartial(e)) || [];
    message.size = object.size ?? 0;
    message.page = object.page ?? 0;
    message.totalSize = object.totalSize ?? 0;
    return message;
  },
};

function createBaseCreateAttemptRequest(): CreateAttemptRequest {
  return { participantId: 0, examId: 0, createdAt: undefined };
}

export const CreateAttemptRequest = {
  encode(
    message: CreateAttemptRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participantId !== 0) {
      writer.uint32(8).int32(message.participantId);
    }
    if (message.examId !== 0) {
      writer.uint32(16).int32(message.examId);
    }
    if (message.createdAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.createdAt),
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAttemptRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateAttemptRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participantId = reader.int32();
          break;
        case 2:
          message.examId = reader.int32();
          break;
        case 3:
          message.createdAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAttemptRequest {
    return {
      participantId: isSet(object.participantId)
        ? Number(object.participantId)
        : 0,
      examId: isSet(object.examId) ? Number(object.examId) : 0,
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
    };
  },

  toJSON(message: CreateAttemptRequest): unknown {
    const obj: any = {};
    message.participantId !== undefined &&
      (obj.participantId = Math.round(message.participantId));
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    message.createdAt !== undefined &&
      (obj.createdAt = message.createdAt.toISOString());
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateAttemptRequest>, I>>(
    object: I
  ): CreateAttemptRequest {
    const message = createBaseCreateAttemptRequest();
    message.participantId = object.participantId ?? 0;
    message.examId = object.examId ?? 0;
    message.createdAt = object.createdAt ?? undefined;
    return message;
  },
};

function createBaseCreateAttemptResponse(): CreateAttemptResponse {
  return { attempt: undefined };
}

export const CreateAttemptResponse = {
  encode(
    message: CreateAttemptResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.attempt !== undefined) {
      Attempt.encode(message.attempt, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAttemptResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateAttemptResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempt = Attempt.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAttemptResponse {
    return {
      attempt: isSet(object.attempt)
        ? Attempt.fromJSON(object.attempt)
        : undefined,
    };
  },

  toJSON(message: CreateAttemptResponse): unknown {
    const obj: any = {};
    message.attempt !== undefined &&
      (obj.attempt = message.attempt
        ? Attempt.toJSON(message.attempt)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CreateAttemptResponse>, I>>(
    object: I
  ): CreateAttemptResponse {
    const message = createBaseCreateAttemptResponse();
    message.attempt =
      object.attempt !== undefined && object.attempt !== null
        ? Attempt.fromPartial(object.attempt)
        : undefined;
    return message;
  },
};

function createBaseUpdateAttemptRequest(): UpdateAttemptRequest {
  return { attempt: undefined };
}

export const UpdateAttemptRequest = {
  encode(
    message: UpdateAttemptRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.attempt !== undefined) {
      Attempt.encode(message.attempt, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAttemptRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateAttemptRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempt = Attempt.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAttemptRequest {
    return {
      attempt: isSet(object.attempt)
        ? Attempt.fromJSON(object.attempt)
        : undefined,
    };
  },

  toJSON(message: UpdateAttemptRequest): unknown {
    const obj: any = {};
    message.attempt !== undefined &&
      (obj.attempt = message.attempt
        ? Attempt.toJSON(message.attempt)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateAttemptRequest>, I>>(
    object: I
  ): UpdateAttemptRequest {
    const message = createBaseUpdateAttemptRequest();
    message.attempt =
      object.attempt !== undefined && object.attempt !== null
        ? Attempt.fromPartial(object.attempt)
        : undefined;
    return message;
  },
};

function createBaseUpdateAttemptResponse(): UpdateAttemptResponse {
  return { attempt: undefined };
}

export const UpdateAttemptResponse = {
  encode(
    message: UpdateAttemptResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.attempt !== undefined) {
      Attempt.encode(message.attempt, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAttemptResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateAttemptResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempt = Attempt.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAttemptResponse {
    return {
      attempt: isSet(object.attempt)
        ? Attempt.fromJSON(object.attempt)
        : undefined,
    };
  },

  toJSON(message: UpdateAttemptResponse): unknown {
    const obj: any = {};
    message.attempt !== undefined &&
      (obj.attempt = message.attempt
        ? Attempt.toJSON(message.attempt)
        : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UpdateAttemptResponse>, I>>(
    object: I
  ): UpdateAttemptResponse {
    const message = createBaseUpdateAttemptResponse();
    message.attempt =
      object.attempt !== undefined && object.attempt !== null
        ? Attempt.fromPartial(object.attempt)
        : undefined;
    return message;
  },
};

function createBaseDeleteAttemptRequest(): DeleteAttemptRequest {
  return { id: 0 };
}

export const DeleteAttemptRequest = {
  encode(
    message: DeleteAttemptRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): DeleteAttemptRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDeleteAttemptRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteAttemptRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: DeleteAttemptRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<DeleteAttemptRequest>, I>>(
    object: I
  ): DeleteAttemptRequest {
    const message = createBaseDeleteAttemptRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseAttempt(): Attempt {
  return {
    id: 0,
    participantId: 0,
    examId: 0,
    finished: false,
    corrects: 0,
    wrongs: 0,
    unanswered: 0,
    createdAt: undefined,
    updatedAt: undefined,
  };
}

export const Attempt = {
  encode(
    message: Attempt,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.participantId !== 0) {
      writer.uint32(16).int32(message.participantId);
    }
    if (message.examId !== 0) {
      writer.uint32(24).int32(message.examId);
    }
    if (message.finished === true) {
      writer.uint32(32).bool(message.finished);
    }
    if (message.corrects !== 0) {
      writer.uint32(40).int32(message.corrects);
    }
    if (message.wrongs !== 0) {
      writer.uint32(48).int32(message.wrongs);
    }
    if (message.unanswered !== 0) {
      writer.uint32(56).int32(message.unanswered);
    }
    if (message.createdAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.createdAt),
        writer.uint32(66).fork()
      ).ldelim();
    }
    if (message.updatedAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.updatedAt),
        writer.uint32(74).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Attempt {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAttempt();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.participantId = reader.int32();
          break;
        case 3:
          message.examId = reader.int32();
          break;
        case 4:
          message.finished = reader.bool();
          break;
        case 5:
          message.corrects = reader.int32();
          break;
        case 6:
          message.wrongs = reader.int32();
          break;
        case 7:
          message.unanswered = reader.int32();
          break;
        case 8:
          message.createdAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        case 9:
          message.updatedAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Attempt {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      participantId: isSet(object.participantId)
        ? Number(object.participantId)
        : 0,
      examId: isSet(object.examId) ? Number(object.examId) : 0,
      finished: isSet(object.finished) ? Boolean(object.finished) : false,
      corrects: isSet(object.corrects) ? Number(object.corrects) : 0,
      wrongs: isSet(object.wrongs) ? Number(object.wrongs) : 0,
      unanswered: isSet(object.unanswered) ? Number(object.unanswered) : 0,
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
      updatedAt: isSet(object.updatedAt)
        ? fromJsonTimestamp(object.updatedAt)
        : undefined,
    };
  },

  toJSON(message: Attempt): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.participantId !== undefined &&
      (obj.participantId = Math.round(message.participantId));
    message.examId !== undefined && (obj.examId = Math.round(message.examId));
    message.finished !== undefined && (obj.finished = message.finished);
    message.corrects !== undefined &&
      (obj.corrects = Math.round(message.corrects));
    message.wrongs !== undefined && (obj.wrongs = Math.round(message.wrongs));
    message.unanswered !== undefined &&
      (obj.unanswered = Math.round(message.unanswered));
    message.createdAt !== undefined &&
      (obj.createdAt = message.createdAt.toISOString());
    message.updatedAt !== undefined &&
      (obj.updatedAt = message.updatedAt.toISOString());
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Attempt>, I>>(object: I): Attempt {
    const message = createBaseAttempt();
    message.id = object.id ?? 0;
    message.participantId = object.participantId ?? 0;
    message.examId = object.examId ?? 0;
    message.finished = object.finished ?? false;
    message.corrects = object.corrects ?? 0;
    message.wrongs = object.wrongs ?? 0;
    message.unanswered = object.unanswered ?? 0;
    message.createdAt = object.createdAt ?? undefined;
    message.updatedAt = object.updatedAt ?? undefined;
    return message;
  },
};

export const AttemptServiceService = {
  getAttempt: {
    path: "/testycool.v1.AttemptService/GetAttempt",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetAttemptRequest) =>
      Buffer.from(GetAttemptRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetAttemptRequest.decode(value),
    responseSerialize: (value: GetAttemptResponse) =>
      Buffer.from(GetAttemptResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetAttemptResponse.decode(value),
  },
  listAttempts: {
    path: "/testycool.v1.AttemptService/ListAttempts",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListAttemptsRequest) =>
      Buffer.from(ListAttemptsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListAttemptsRequest.decode(value),
    responseSerialize: (value: ListAttemptsResponse) =>
      Buffer.from(ListAttemptsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListAttemptsResponse.decode(value),
  },
  createAttempt: {
    path: "/testycool.v1.AttemptService/CreateAttempt",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateAttemptRequest) =>
      Buffer.from(CreateAttemptRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateAttemptRequest.decode(value),
    responseSerialize: (value: CreateAttemptResponse) =>
      Buffer.from(CreateAttemptResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => CreateAttemptResponse.decode(value),
  },
  updateAttempt: {
    path: "/testycool.v1.AttemptService/UpdateAttempt",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateAttemptRequest) =>
      Buffer.from(UpdateAttemptRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateAttemptRequest.decode(value),
    responseSerialize: (value: UpdateAttemptResponse) =>
      Buffer.from(UpdateAttemptResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => UpdateAttemptResponse.decode(value),
  },
  deleteAttempt: {
    path: "/testycool.v1.AttemptService/DeleteAttempt",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteAttemptRequest) =>
      Buffer.from(DeleteAttemptRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteAttemptRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface AttemptServiceServer extends UntypedServiceImplementation {
  getAttempt: handleUnaryCall<GetAttemptRequest, GetAttemptResponse>;
  listAttempts: handleUnaryCall<ListAttemptsRequest, ListAttemptsResponse>;
  createAttempt: handleUnaryCall<CreateAttemptRequest, CreateAttemptResponse>;
  updateAttempt: handleUnaryCall<UpdateAttemptRequest, UpdateAttemptResponse>;
  deleteAttempt: handleUnaryCall<DeleteAttemptRequest, Empty>;
}

export interface AttemptServiceClient extends Client {
  getAttempt(
    request: GetAttemptRequest,
    callback: (error: ServiceError | null, response: GetAttemptResponse) => void
  ): ClientUnaryCall;
  getAttempt(
    request: GetAttemptRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: GetAttemptResponse) => void
  ): ClientUnaryCall;
  getAttempt(
    request: GetAttemptRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: GetAttemptResponse) => void
  ): ClientUnaryCall;
  listAttempts(
    request: ListAttemptsRequest,
    callback: (
      error: ServiceError | null,
      response: ListAttemptsResponse
    ) => void
  ): ClientUnaryCall;
  listAttempts(
    request: ListAttemptsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListAttemptsResponse
    ) => void
  ): ClientUnaryCall;
  listAttempts(
    request: ListAttemptsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListAttemptsResponse
    ) => void
  ): ClientUnaryCall;
  createAttempt(
    request: CreateAttemptRequest,
    callback: (
      error: ServiceError | null,
      response: CreateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  createAttempt(
    request: CreateAttemptRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  createAttempt(
    request: CreateAttemptRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  updateAttempt(
    request: UpdateAttemptRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  updateAttempt(
    request: UpdateAttemptRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  updateAttempt(
    request: UpdateAttemptRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  deleteAttempt(
    request: DeleteAttemptRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAttempt(
    request: DeleteAttemptRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAttempt(
    request: DeleteAttemptRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const AttemptServiceClient = makeGenericClientConstructor(
  AttemptServiceService,
  "testycool.v1.AttemptService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): AttemptServiceClient;
  service: typeof AttemptServiceService;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = t.seconds * 1_000;
  millis += t.nanos / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Date {
  if (o instanceof Date) {
    return o;
  } else if (typeof o === "string") {
    return new Date(o);
  } else {
    return fromTimestamp(Timestamp.fromJSON(o));
  }
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
