export interface Attempt {
  id: number;
  participantId: number;
  examId: number;
  finished: boolean;
  corrects: number;
  wrongs: number;
  unanswered: number;
  createdAt: number;
  updatedAt: number;
}
