import {ipcRenderer} from "electron";

export const questionChannels = ['list-questions', 'get-question', 'question-error'];

export const questionContextBridge = {
  apiKey: 'question',
  api: {
    listQuestions: (listQuestionsRequestDto) => {
      ipcRenderer.send('list-questions', {...listQuestionsRequestDto});
    },
    getQuestion: (getQuestionRequestDto) => {
      ipcRenderer.send('get-question', {...getQuestionRequestDto});
    },
    once: (channel, func) => {
      if (!questionChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid question channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.once(channel, (event, ...args) => func(...args));
    },
    on: (channel, func) => {
      if (!questionChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid question channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    },
  }
}