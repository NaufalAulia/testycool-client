import {Choice} from "../models";

export interface GetChoiceResponseDto {
  choice: Choice | undefined;
}
