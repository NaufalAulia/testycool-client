import {ipcMain} from 'electron';
import store from "../store";
import Services, {GRPCCredential} from "../adapters/services";
import {Metadata} from "@grpc/grpc-js";
import {ParticipantServiceClient} from "../adapters/protobuf/gen/testycool/participant";
import {GetParticipantRequestDto} from "../../shared/dtos";
import {participantAction} from "../../shared/redux/actions";

export const ParticipantIpc = () => {
  ipcMain.on('get-participant', (event, args: GetParticipantRequestDto) => {
    const address = store.getState().auth.address;
    if (!Services.participant) {
      Services.participant = new ParticipantServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }

    const participantId = store.getState().auth.credentials.participantId;

    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.participant.getParticipant(
      {id: args.id || participantId},
      metadata,
      (error, response) => {
        if (error) {
          event.sender.send('participant-error', {code: error.code, details: error.details});
        }
        if (response) {
          store.dispatch(participantAction.getParticipant.fulfilled(response));
          event.reply('get-participant', response);
        }
      })
  });
}