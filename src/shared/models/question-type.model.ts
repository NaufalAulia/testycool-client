export enum QuestionType {
  UNKNOWN = 0,
  MULTIPLE_CHOICE = 1,
  ESSAY = 2,
}
