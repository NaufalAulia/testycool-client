import React, {useCallback, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../../store";
import ReactQuill from "react-quill";
import QuestionList from "./components/question-list.component";
import QuestionControl from "./components/question-control.component";
import AnswerForm from "./components/answer-form.component";
import CountdownTimer from "./components/countdown-timer.component";
import {answerSender, attemptSender, questionSender} from "../../shared/ipcs";
import {answerAction, attemptAction} from "../../../shared/redux/actions";
import {FinishStatus, LoadingType} from "../../../shared/models";
import {LoadingTypeToString} from "../../../shared/utils/string-helper";
import {useNavigate} from "react-router";
import Modal from 'react-modal';

export default function Attempt(): JSX.Element {
  const exam = useSelector((state: RootState) => state.exam.exam);
  const participant = useSelector((state: RootState) => state.participant.participant);
  const attempt = useSelector((state: RootState) => state.attempt.attempt);
  const attemptFinishStatus = useSelector((state: RootState) => state.attempt.finishStatus);
  const questions = useSelector((state: RootState) => state.question.questions);
  const questionsSize = useSelector((state: RootState) => state.question.size);
  const currentQuestionIdx = useSelector((state: RootState) => state.question.currentQuestionIdx);
  const currentQuestion = useSelector((state: RootState) => state.question.currentQuestion);
  const choices = useSelector((state: RootState) => state.choice.choices);
  const answers = useSelector((state: RootState) => state.answer.answers);
  const currentAnswer = useSelector((state: RootState) => state.answer.currentAnswer);
  const questionFlags = useSelector((state: RootState) => state.question.flags);
  const currentFlag = useSelector((state: RootState) => state.question.currentFlag);

  const examLoading = useSelector((state: RootState) => state.exam.loading);
  const questionLoading = useSelector((state: RootState) => state.question.loading);
  const choiceLoading = useSelector((state: RootState) => state.choice.loading);
  const answerLoading = useSelector((state: RootState) => state.answer.loading);
  const attemptLoading = useSelector((state: RootState) => state.attempt.loading);
  const answerSaved = useSelector((state: RootState) => state.answer.answerSaved);

  const finishAttempt = useCallback(() => {
    attemptSender.updateAttempt({
      attempt: {
        ...attempt,
        finished: true,
        updatedAt: Date.now(),
      }
    });
  }, [attempt]);

  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();

  const confirmFinish = useCallback((finishStatus: FinishStatus) => {
    dispatch(attemptAction.updateFinishStatus(finishStatus));
  }, [dispatch]);

  useEffect(() => {
    if (
      attemptFinishStatus === FinishStatus.FINISHED ||
      attemptFinishStatus === FinishStatus.FORFEITED
    ) {
      return navigate('/result');
    }
  }, [attemptFinishStatus, navigate]);

  useEffect(() => {
    if (!questions && exam) {
      questionSender.listQuestions({filter: {examId: exam.id}, page: 1, size: -1})
    }
    if (questions?.length > 0 && (!currentQuestion || currentQuestion.id !== questions[currentQuestionIdx].id)) {
      questionSender.getQuestion({id: questions[currentQuestionIdx].id});
    }
  }, [exam, questions, currentQuestionIdx, currentQuestion]);

  useEffect(() => {
    if (!answers && exam) {
      answerSender.listAnswers({filter: {participantId: participant.id, examId: exam.id}, page: 1, size: -1})
    }
    if (
      questions?.length > 0 && answers?.length > 0 &&
      (!currentAnswer || currentQuestion?.id !== questions[currentQuestionIdx].id)
    ) {
      dispatch(answerAction.changeAnswer({questionId: questions[currentQuestionIdx].id}));
    }
  }, [exam, participant, currentQuestionIdx, currentQuestion, questions, answers]);

  Modal.setAppElement("#root");

  return (
    <div className="h-screen max-h-screen px-8 py-10 flex flex-col justify-center items-stretch bg-gray-100">
      <h1 className="text-5xl font-bold text-center mb-10">{exam.title || "Exam Title"}</h1>
      <div
        className="
        flex-grow
        grid grid-flow-col grid-cols-5 gap-2
        justify-center items-stretch overflow-hidden
        "
      >
        <div
          className="col-span-1 card drop-shadow-md flex flex-col items-center content-start min-w-fit">
          {
            exam && examLoading === LoadingType.IDLE ?
              <CountdownTimer exam={exam} onTimerEnd={finishAttempt}/>
              :
              <h2 className="text-xl font-bold text-center">
                {LoadingTypeToString(examLoading, 'Exam')}
              </h2>
          }
          <QuestionList
            questions={questions}
            questionFlags={questionFlags}
            questionIdx={currentQuestionIdx}
            questionLoading={questionLoading}
            answerLoading={answerLoading}
            answerSaved={answerSaved}
          />
          <button
            onClick={() => confirmFinish(FinishStatus.REQUESTED)}
            className="button button-md button-info justify-self-center w-1/2"
          >
            FINISH
          </button>
        </div>
        <div className="col-span-4 card drop-shadow-md flex flex-col overflow-y-hidden">
          <QuestionControl
            questionIdx={currentQuestionIdx}
            questionsSize={questionsSize}
            questionLoading={questionLoading}
            answerLoading={answerLoading}
            answerSaved={answerSaved}
            currentAnswer={currentAnswer}
            currentFlag={currentFlag}
            className="mb-5"
          />
          <div className="flex-grow overflow-y-auto">
            {currentQuestion && questionLoading !== LoadingType.GETENTRY ? (
                <ReactQuill
                  className="flex text-md select-none"
                  value={currentQuestion.content}
                  theme="bubble"
                  readOnly
                />
              )
              : (
                <h2 className="flex-grow my-8 text-xl font-bold text-center">
                  {LoadingTypeToString(questionLoading, 'Question')}
                </h2>
              )
            }
            <hr className="mx-2 my-8"/>
            {
              (currentQuestion && currentQuestion.id === questions[currentQuestionIdx].id) ?
                <AnswerForm
                  participant={participant}
                  currentQuestion={currentQuestion}
                  choices={choices}
                  currentAnswer={currentAnswer}
                  questionLoading={questionLoading}
                  choiceLoading={choiceLoading}
                  answerLoading={answerLoading}
                />
                : ""
            }
          </div>
        </div>
      </div>
      <Modal
        isOpen={attemptFinishStatus === FinishStatus.REQUESTED}
        onRequestClose={() => dispatch(attemptAction.updateFinishStatus(FinishStatus.UNFINISHED))}
        className="place-self-center w-1/4 card drop-shadow-md p-8"
        overlayClassName="fixed top-0 bottom-0 left-0 right-0 w-screen h-screen bg-black bg-opacity-50 grid"
      >
        {attemptLoading === LoadingType.IDLE ? (
          <div className="flex flex-col items-center content-center text-center">
            <h1 className="text-lg font-bold text-primary">
              Do want to finish and submit your attempt?
            </h1>
            <h3 className="text-md my-4">
              Please review your work carefully before submitting
            </h3>
            <button
              onClick={() => finishAttempt()}
              className="button button-md button-success justify-self-center w-1/2"
            >
              YES
            </button>
          </div>
        ) : (
          <h1 className="text-lg font-bold text-primary text-center">
            Calculating your result
          </h1>
        )}
      </Modal>
    </div>
  );
}
