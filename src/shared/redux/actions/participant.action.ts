import {createAction} from '@reduxjs/toolkit';
import {ErrorResponseDto, GetParticipantResponseDto} from "../../dtos";

const participantAction = {
  getParticipant: {
    pending: createAction('[Participant] Get Participant/pending'),
    fulfilled: createAction<GetParticipantResponseDto>('[Participant] Get Participant/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Participant] Get Participant/rejected'),
  },
  setError: createAction<ErrorResponseDto>(
    '[Participant] set exam error'
  ),
  resetState: createAction('[Participant] reset state')
}

export default participantAction;
