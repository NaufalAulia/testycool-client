import {EssayAnswer} from "../models";

export interface CreateAnswerRequestDto {
  participantId: number;
  questionId: number;
  choiceId: number | undefined;
  essay: EssayAnswer | undefined;
}
