import {ipcRenderer} from "electron";

export const participantChannels = ['get-participant', 'participant-error'];

export const participantContextBridge = {
  apiKey: 'participant',
  api: {
    getParticipant: (getParticipantRequestDto) => {
      ipcRenderer.send('get-participant', {...getParticipantRequestDto});
    },
    once: (channel, func) => {
      if (!participantChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid participant channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.once(channel, (event, ...args) => func(...args));
    },
    on: (channel, func) => {
      if (!participantChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid participant channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    },
  }
}