import {createReducer} from '@reduxjs/toolkit';
import {LoadingType, Question, QuestionFlag} from "../../models";
import {questionAction} from "../actions";
import {ErrorResponseDto} from "../../dtos";

export interface QuestionState {
  questions?: Question[];
  size: number;
  page: number;
  totalSize: number;
  flags?: QuestionFlag[];
  currentFlag?: QuestionFlag;
  currentQuestionIdx: number;
  currentQuestion?: Question;
  loading: LoadingType;
  error?: ErrorResponseDto;
}

const initialState: QuestionState = {
  questions: undefined,
  size: -1,
  page: 1,
  totalSize: 0,
  flags: undefined,
  currentFlag: undefined,
  currentQuestionIdx: 0,
  currentQuestion: undefined,
  loading: LoadingType.IDLE,
  error: undefined,
};

const QuestionReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(questionAction.listQuestions.pending, (state) => ({
      ...state,
      loading: LoadingType.GETLIST,
    }))
    .addCase(questionAction.listQuestions.fulfilled, (state, action) => {
      const flags = action.payload.questions.map((q) => ({
        isActive: false,
        note: "",
        questionId: q.id
      }));

      return {
        ...state,
        questions: action.payload.questions,
        size: action.payload.size,
        page: action.payload.page,
        totalSize: action.payload.totalSize,
        flags: flags,
        currentFlag: flags[0],
        loading: LoadingType.IDLE,
      };
    })
    .addCase(questionAction.listQuestions.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(questionAction.getQuestion.pending, (state) => ({
      ...state,
      loading: LoadingType.GETENTRY,
    }))
    .addCase(questionAction.getQuestion.fulfilled, (state, action) => {
      return {
        ...state,
        currentQuestion: action.payload.question,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(questionAction.getQuestion.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(questionAction.changeQuestion, (state, action) => ({
      ...state,
      currentQuestionIdx: action.payload.index,
      currentFlag: state.flags[action.payload.index],
      loading: LoadingType.IDLE,
    }))
    .addCase(questionAction.setFlag, (state, action) => {
      state.currentFlag = action.payload;
      state.flags[state.currentQuestionIdx] = action.payload;
      state.loading = LoadingType.IDLE;
    })
    .addCase(questionAction.setError, (state, action) => ({
      ...state,
      error: action.payload,
      loading: LoadingType.IDLE,
    }))
    .addCase(questionAction.resetState, () => {
      return {
        ...initialState
      }
    })
);

export default QuestionReducer;
