import {LoadingType} from "../../../shared/models";
import React, {useState} from "react";
import {useSelector} from "react-redux";
import {RootState} from "../../store";
import {AuthRequestDto} from "../../../shared/dtos";
import {authSender} from "../../shared/ipcs";

export default function LoginForm() {
  const {error, loading} = useSelector((state: RootState) => state.auth);

  const [authRequestForm, setAuthRequestForm] = useState<AuthRequestDto>({
    address: '',
    examPassword: '',
    participantCode: ''
  });

  const handleSubmit = (event: any) => {
    event.preventDefault();

    authSender.getAccessToken({
      address: authRequestForm.address,
      examPassword: authRequestForm.examPassword,
      participantCode: authRequestForm.participantCode
    });
  }

  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    const {currentTarget} = event;
    switch (currentTarget.name) {
      case 'address':
        setAuthRequestForm({...authRequestForm, address: currentTarget.value})
        break;
      case 'examPassword':
        setAuthRequestForm({...authRequestForm, examPassword: currentTarget.value})
        break;
      case 'participantCode':
        setAuthRequestForm({...authRequestForm, participantCode: currentTarget.value})
        break;
    }
  }

  return (
    <div className="h-screen border-2 grid justify-items-stretch content-center p-8 gap-5">
      <h1 className="text-center text-4xl font-bold text-primary">TestyCool</h1>
      <h4 className="text-center text-lg font-bold">INPUT CREDENTIALS</h4>
      {error ?
        <div className="p-2 rounded-md bg-danger text-center text-white">
          {`${error.code}: ${error.details}`}
        </div>
        : ""
      }
      <form
        className="grid justify-stretch items-start gap-4"
        onSubmit={handleSubmit}
      >
        <div className="form-control">
          <label className="text-center" htmlFor="address">
            SERVER ADDRESS
          </label>
          <input
            type="text"
            placeholder="192.2.2.2:1234 / example.com"
            id="address"
            name="address"
            value={authRequestForm.address}
            required
            disabled={loading !== LoadingType.IDLE}
            onChange={handleChange}
          />
        </div>
        <div className="form-control">
          <label className="text-center" htmlFor="examPassword">
            EXAM PASSWORD
          </label>
          <input
            type="text"
            id="examPassword"
            name="examPassword"
            value={authRequestForm.examPassword}
            required
            disabled={loading !== LoadingType.IDLE}
            onChange={handleChange}/>
        </div>
        <div className="form-control">
          <label className="text-center" htmlFor="participantCode">
            PARTICIPANT ID
          </label>
          <input
            type="text"
            id="participantCode"
            name="participantCode"
            value={authRequestForm.participantCode}
            required
            disabled={loading !== LoadingType.IDLE}
            onChange={handleChange}/>
        </div>
        <button
          disabled={loading !== LoadingType.IDLE}
          className="w-40 button button-md button-info justify-self-center"
        >
          {loading !== LoadingType.IDLE ? "LOGGING IN..." : "LOGIN"}
        </button>
      </form>
    </div>
  );
}
