import {contextBridge} from "electron";
import {authContextBridge, windowManagerContextBridge} from "./context-bridges";

contextBridge.exposeInMainWorld(windowManagerContextBridge.apiKey, windowManagerContextBridge.api);
contextBridge.exposeInMainWorld(authContextBridge.apiKey, authContextBridge.api);
