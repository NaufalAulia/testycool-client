import {questionAction} from "../../../../shared/redux/actions";
import {GetQuestionRequestDto, ListQuestionsRequestDto} from "../../../../shared/dtos";
import store from "../../../store";

const questionSender = {
  listQuestions: (dto: ListQuestionsRequestDto) => {
    store.dispatch(questionAction.listQuestions.pending());
    window.question.listQuestions(dto);
  },
  getQuestion: (dto: GetQuestionRequestDto) => {
    store.dispatch(questionAction.getQuestion.pending());
    window.question.getQuestion(dto);
  }
}

export default questionSender;