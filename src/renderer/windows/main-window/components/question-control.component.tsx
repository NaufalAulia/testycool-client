import React, {useCallback, useEffect, useState} from "react";
import {questionAction} from "../../../../shared/redux/actions";
import {
  CheckIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  CloudDownloadIcon,
  CloudUploadIcon,
  ExclamationIcon,
  FlagIcon,
  PencilAltIcon,
  RefreshIcon
} from "@heroicons/react/solid";
import {useDispatch} from "react-redux";
import {AppDispatch} from "../../../store";
import {Answer, LoadingType, QuestionFlag} from "../../../../shared/models";

export default function QuestionControl(
  props: {
    questionIdx: number,
    questionsSize: number,
    questionLoading: LoadingType,
    answerLoading: LoadingType,
    answerSaved: boolean,
    currentAnswer?: Answer,
    currentFlag?: QuestionFlag,
    className?: string,
  }): JSX.Element {
  const {
    questionIdx,
    questionsSize,
    questionLoading,
    answerLoading,
    answerSaved,
    currentAnswer,
    currentFlag,
    className
  } = props;

  const dispatch = useDispatch<AppDispatch>();

  const saveStatus = useCallback((): { color: string, icon: JSX.Element } => {
    switch (answerLoading) {
      case LoadingType.IDLE:
        if (!currentAnswer) {
          return {
            color: 'button-danger',
            icon: <ExclamationIcon/>
          }
        }
        if (!answerSaved) {
          return {
            color: 'button-warning',
            icon: <PencilAltIcon/>
          }
        }
        return {
          color: 'button-success',
          icon: <CheckIcon/>
        }
      case LoadingType.CREATE:
      case LoadingType.UPDATE:
        return {
          color: 'button-info',
          icon: <CloudUploadIcon/>
        }
      case LoadingType.GETLIST:
      case LoadingType.GETENTRY:
        return {
          color: 'button-info',
          icon: <CloudDownloadIcon/>
        }
      case LoadingType.LOADING:
        return {
          color: 'button-info',
          icon: <RefreshIcon/>
        }
    }
  }, [answerLoading, answerSaved, currentAnswer]);

  const [flagNote, setFlagNote] = useState("");

  useEffect(() => {
    setFlagNote(currentFlag?.note || "");
  }, [currentFlag]);

  const updateFlagNote = (e: any) => {
    setFlagNote(e.currentTarget.value);
  };

  return (
    <div className={`flex flex-row justify-between content-center ${className}`}>
      <div className="flex gap-2 items-stretch">
        <button
          onClick={() => dispatch(questionAction.changeQuestion({index: questionIdx - 1}))}
          disabled={
            questionIdx === 0 || !answerSaved ||
            questionLoading !== LoadingType.IDLE || answerLoading !== LoadingType.IDLE
          }
          className="button button-md button-info w-12"
        >
          <ChevronLeftIcon/>
        </button>
        <button
          onClick={() => dispatch(questionAction.changeQuestion({index: questionIdx + 1}))}
          disabled={
            questionIdx === questionsSize - 1 || !answerSaved ||
            questionLoading !== LoadingType.IDLE || answerLoading !== LoadingType.IDLE
          }
          className="button button-md button-info w-12"
        >
          <ChevronRightIcon/>
        </button>
        <button
          onClick={() => dispatch(questionAction.setFlag({...currentFlag, isActive: !currentFlag.isActive}))}
          className={`button button-md w-12 ${currentFlag?.isActive ? "button-danger" : "button-danger-outline"}`}
        >
          <FlagIcon/>
        </button>
        {currentFlag?.isActive ? (
            <div className="form-control flex flex-row">
              <input
                type="text" name="note" id="note"
                value={flagNote}
                onChange={updateFlagNote}
                onBlur={() => dispatch(questionAction.setFlag({...currentFlag, note: flagNote}))}
                className="flex-grow"
              />
            </div>
          )
          : (
            ""
          )
        }
      </div>
      <h1 className="flex-grow text-center text-2xl font-bold">
        Number {questionIdx + 1}
      </h1>
      <div className={`button button-md w-12 ${saveStatus().color} rounded-full`}>
        {saveStatus().icon}
      </div>
    </div>
  );
}