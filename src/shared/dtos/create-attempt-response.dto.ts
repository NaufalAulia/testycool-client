import {Attempt} from "../models";

export interface CreateAttemptResponseDto {
  attempt: Attempt | undefined;
}
