import {ipcMain} from "electron";
import {AuthRequestDto} from "../../shared/dtos";
import {AuthServiceClient} from "../adapters/protobuf/gen/testycool/auth";
import Services, {GRPCCredential} from "../adapters/services";
import store from "../store";
import {authAction} from "../../shared/redux/actions";

export const AuthIpc = () => {
  ipcMain.on('get-access-token', (event, args: AuthRequestDto) => {
    const {address, examPassword, participantCode} = args;

    if (!Services.auth || store.getState().auth.address !== address) {
      store.dispatch(authAction.setAddress({address}));
      Services.auth = new AuthServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }

    Services.auth.getParticipantToken(
      {examPassword, participantCode},
      (err, response) => {
        if (err) {
          event.sender.send('auth-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          const token = response.accessToken;
          const payload = token.split('.')[1];
          const participantId = JSON.parse(global.atob(payload)).user_id;

          store.dispatch(authAction.getAccessToken.fulfilled({accessToken: response.accessToken}));
          store.dispatch(authAction.setCredentials({
            examPassword: args.examPassword,
            participantCode: args.participantCode,
            participantId
          }));
          event.reply('get-access-token', {
            ...response,
            examPassword: args.examPassword,
            participantCode: args.participantCode,
            participantId
          });
        }
      }
    );
  });
}
