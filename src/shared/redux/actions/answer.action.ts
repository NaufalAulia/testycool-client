import {createAction} from '@reduxjs/toolkit';
import {
  CreateAnswerResponseDto,
  ErrorResponseDto,
  GetAnswerResponseDto,
  ListAnswersResponseDto,
  UpdateAnswerResponseDto
} from "../../dtos";

const answerAction = {
  listAnswers: {
    pending: createAction('[Answer] List Answers/pending'),
    fulfilled: createAction<ListAnswersResponseDto>('[Answer] List Answers/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Answer] List Answers/rejected'),
  },
  getAnswer: {
    pending: createAction('[Answer] Get Answer/pending'),
    fulfilled: createAction<GetAnswerResponseDto>('[Answer] Get Answer/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Answer] Get Answer/rejected'),
  },
  createAnswer: {
    pending: createAction('[Answer] Create Answer/pending'),
    fulfilled: createAction<CreateAnswerResponseDto>('[Answer] Create Answer/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Answer] Create Answer/rejected'),
  },
  updateAnswer: {
    pending: createAction('[Answer] Update Answer/pending'),
    fulfilled: createAction<UpdateAnswerResponseDto>('[Answer] Update Answer/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Answer] Update Answer/rejected'),
  },
  changeAnswer: createAction<{ questionId: number }>('[Answer] Change current Answer'),
  setAnswerSaved: createAction<boolean>('[Answer] set answer saved status'),
  setError: createAction<ErrorResponseDto>('[Answer] set answer error'),
  resetState: createAction('[Answer] reset state')
}

export default answerAction;
