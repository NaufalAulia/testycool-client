import {createAction} from '@reduxjs/toolkit';
import {AuthResponseDto, ErrorResponseDto} from "../../dtos";

const authAction = {
  getAccessToken: {
    pending: createAction('[Auth] Request Authentication/pending'),
    fulfilled: createAction<AuthResponseDto>('[Auth] Request Authentication/fulfilled'),
    rejected: createAction<ErrorResponseDto>('[Auth] Request Authentication/rejected'),
  },
  setAddress: createAction<{ address: string }>(
    '[Auth] set backend server address'
  ),
  setAccessToken: createAction<AuthResponseDto>(
    '[Auth] set backend access token'
  ),
  setCredentials: createAction<{ participantCode: string, participantId: number, examPassword: string }>(
    '[Auth] set auth credentials'),
  setError: createAction<ErrorResponseDto>(
    '[Auth] set authentication error'
  ),
  resetState: createAction('[Auth] reset state')
}

export default authAction;
