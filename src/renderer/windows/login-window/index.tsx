import React from "react";
import ReactDOM from "react-dom";
import App from './app';
import {authListener} from "../../shared/ipcs";
import {Provider} from "react-redux";
import store from "../../store";

declare global {
  interface Window {
    windowManager: any;
    auth: any;
  }
}

authListener();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App/>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
