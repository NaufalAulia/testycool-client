import {ipcMain} from "electron";
import {GetExamRequestDto} from "../../shared/dtos";
import store from "../store";
import {Metadata} from "@grpc/grpc-js";
import Services, {GRPCCredential} from "../adapters/services";
import {ExamServiceClient} from "../adapters/protobuf/gen/testycool/exam";
import {examAction} from "../../shared/redux/actions";
import {examStatusToLocalExamStatus} from "../../shared/utils/type-translate-helper";

export const ExamIpc = () => {
  ipcMain.on('get-exam', ((event, args: GetExamRequestDto) => {
    const address = store.getState().auth.address;
    if (!Services.exam) {
      Services.exam = new ExamServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }

    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    const credentials = store.getState().auth.credentials;
    Services.exam.getExam({
        id: args.id,
        password: args.examPassword || credentials.examPassword,
      }, metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('exam-error', {code: error.code, details: error.details});
        }
        if (response) {
          store.dispatch(examAction.getExam.fulfilled({
            exam: {
              ...response.exam,
              status: examStatusToLocalExamStatus(response.exam.status),
              startAt: new Date(response.exam.startAt).getTime(),
            }
          }));
          event.reply('get-exam', response);
        }
      }));
  }))
}
