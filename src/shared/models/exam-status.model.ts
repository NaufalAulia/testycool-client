export enum ExamStatus {
  UNKNOWN = 0,
  WAITING = 1,
  STARTED = 2,
  DONE = 3,
}
