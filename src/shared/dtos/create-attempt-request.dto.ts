export interface CreateAttemptRequestDto {
  participantId: number;
  examId: number;
  createdAt: number;
}
