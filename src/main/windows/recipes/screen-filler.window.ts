import {WindowRecipe} from "./interfaces/window-recipe";

declare const SCREEN_FILLER_WINDOW_WEBPACK_ENTRY: string;
declare const SCREEN_FILLER_WINDOW_PRELOAD_WEBPACK_ENTRY: string;

const ScreenFillerWindow: WindowRecipe = {
  entry: SCREEN_FILLER_WINDOW_WEBPACK_ENTRY,
  options: {
    fullscreen: true,
    focusable: false,
    autoHideMenuBar: true,
  },
  extraActions: (window) => {
    window.setVisibleOnAllWorkspaces(true, {
      visibleOnFullScreen: true
    })
  }
}

export default ScreenFillerWindow;