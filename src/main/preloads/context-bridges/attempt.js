import {ipcRenderer} from "electron";

export const attemptChannels = [
  'get-attempt',
  'create-attempt',
  'update-attempt',
  'finish-attempt',
  'forfeit-attempt',
  'attempt-error'
];

export const attemptContextBridge = {
  apiKey: 'attempt',
  api: {
    getAttempt: (getAttemptRequestDto) => {
      ipcRenderer.send('get-attempt', {...getAttemptRequestDto});
    },
    createAttempt: (createAttemptRequestDto) => {
      ipcRenderer.send('create-attempt', {...createAttemptRequestDto});
    },
    updateAttempt: (updateAttemptRequestDto) => {
      ipcRenderer.send('update-attempt', {...updateAttemptRequestDto});
    },
    once: (channel, func) => {
      if (!attemptChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid attempt channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.once(channel, (event, ...args) => func(...args));
    },
    on: (channel, func) => {
      if (!attemptChannels.includes(channel)) {
        throw new Error(`${channel} is not a valid attempt channel`);
      }

      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    },
  }
}