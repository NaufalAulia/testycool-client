import {Answer} from "../models";

export interface CreateAnswerResponseDto {
  answer: Answer | undefined;
}
