import {app, BrowserWindow, dialog} from 'electron';
import {createNewWindow} from './windows/window-manager';
import {
  AnalyticsIpc,
  AnswerIpc,
  AttemptIpc,
  AuthIpc,
  ChoiceIpc,
  ExamIpc,
  ParticipantIpc,
  QuestionIpc,
  WindowManagerIpc
} from "./ipc";
import * as si from 'systeminformation';
import {LoginWindow} from "./windows/recipes";

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  // eslint-disable-line global-require
  app.quit();
}

AuthIpc();
ExamIpc();
ParticipantIpc();
QuestionIpc();
ChoiceIpc();
AnswerIpc();
AttemptIpc();
AnalyticsIpc();
WindowManagerIpc();

// Set App User Model ID for Windows Notification
app.setAppUserModelId(process.execPath);

const createWindow = async (): Promise<void> => {
  // Create the browser window.
  const window = createNewWindow(LoginWindow);
  const system = await si.system();
  if (system.virtual) {
    const response = dialog.showMessageBoxSync(window, {
      type: "error",
      title: "Virtual Environment Detected!",
      message: "You are not allowed to run this app on virtual environment"
    });

    if (response === 0) {
      app.quit();
    }
  }
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.