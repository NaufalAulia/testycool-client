import {ipcMain} from 'electron';
import store from "../store";
import {Metadata} from "@grpc/grpc-js";
import {CreateAnalyticsRequestDto} from "../../shared/dtos";
import Services, {GRPCCredential} from "../adapters/services";
import {AnalyticsServiceClient} from "../adapters/protobuf/gen/testycool/analytics";

export const AnalyticsIpc = () => {
  ipcMain.on('create-analytics', ((event, args: CreateAnalyticsRequestDto) => {
    const address = store.getState().auth.address;
    if (!Services.analytics) {
      Services.analytics = new AnalyticsServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }

    const accessToken = store.getState().auth.accessToken;
    const metadata = new Metadata;
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.analytics.createAnalytics(
      args,
      metadata,
      ((error, response) => {
        if (error) {
          event.sender.send('analytics-error', {code: error.code, details: error.details});
        }
        if (response) {
          event.reply('create-analytics', response);
        }
      }));
  }))
}
