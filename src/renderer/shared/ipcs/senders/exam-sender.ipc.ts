import {GetExamRequestDto} from "../../../../shared/dtos";
import {examAction} from "../../../../shared/redux/actions";
import store from "../../../store";

const examSender = {
  getExam: (dto: GetExamRequestDto) => {
    store.dispatch(examAction.getExam.pending());
    window.exam.getExam(dto);
  }
}

export default examSender;