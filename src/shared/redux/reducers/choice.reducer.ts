import {createReducer} from '@reduxjs/toolkit';
import {Choice, LoadingType} from "../../models";
import {choiceAction} from "../actions";
import {ErrorResponseDto} from "../../dtos";

export interface ChoiceState {
  choices?: Choice[];
  size: number;
  page: number;
  totalSize: number;
  loading: LoadingType;
  error?: ErrorResponseDto;
}

const initialState: ChoiceState = {
  choices: undefined,
  size: -1,
  page: 1,
  totalSize: 0,
  loading: LoadingType.IDLE,
  error: undefined,
};

const ChoiceReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(choiceAction.listChoices.pending, (state) => ({
      ...state,
      loading: LoadingType.GETLIST,
    }))
    .addCase(choiceAction.listChoices.fulfilled, (state, action) => {
      return {
        ...state,
        choices: action.payload.choices,
        size: action.payload.size,
        page: action.payload.page,
        totalSize: action.payload.totalSize,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(choiceAction.listChoices.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(choiceAction.getChoice.pending, (state) => ({
      ...state,
      loading: LoadingType.GETENTRY,
    }))
    .addCase(choiceAction.getChoice.fulfilled, (state, action) => {
      return {
        ...state,
        currentChoice: action.payload.choice,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(choiceAction.getChoice.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.IDLE,
      };
    })
    .addCase(choiceAction.setError, (state, action) => ({
      ...state,
      error: action.payload,
      loading: LoadingType.IDLE,
    }))
    .addCase(choiceAction.resetState, () => {
      return {
        ...initialState
      }
    })
);

export default ChoiceReducer;
