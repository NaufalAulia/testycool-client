export class ListAnswersRequestDto {
  public filter: {
    participantId?: number | undefined;
    examId?: number | undefined;
  }
  public page: number;
  public size: number;
}